��          |       �       �      �   \   �   �   T  �  �  
   �     �  >   �         /  �  C  f  �  �  K
       A      �   b  �  �     �  	   �  8   �     �  �  �  �  �  �  }   أهداف المشروع إعادة العدالة الانتقالية إلى جدول الأعمال السياسي تعزيز قدرة منظمات المجتمع المدني على الدعوة بشكل مشترك لاعتماد ضمانات عدم التكرار تهدف الومضة إلى توعية المواطن بأهمية الانتخابات، وبوجود رابط وثيق بين
                            انتهاكات و شوائب ما قبل الثورة وتكرارها في حياته اليومية طالما لم تبذل الدولة مزيد من الجهود
                            لاستكمال مسار العدالة الانتقالية وتعزيز الانتقال الديمقراطي. توثيق رؤية المزيد رصد نقدي لمسار العدالة الانتقالية سبر أراء من التوانسة يشوفوا أنو أهداف العدالة الانتقالية إلّي هوما كشف الحقيقة عن مختلف الانتهاكات مساءلة
                                ومحاسبة المسؤولين عنها وجبر الضرر ورد الاعتبار للضحايا لتحقيق المصالحة الوطنية لازم كونوا
                                موجودين في برامج السياسيين الي باش يكونوا في الحكم بعد انتخابات 2019 من التوانسة يعتبروا أنو المصالحة الوطنية لازم تكون بتحقيق كل الشروط اللازمة لمسار العدالة الإنتقالية بداية بكشف الحقيقة، مرورا بمحاسبة مرتكبي الجرائم، وردّ الإعتبار للضحايا ووضع الضمانات اللازمة باش ما يعاودش يصير إلي صار قبل هو مشروع ثلاثي يضم كل من منظمة البوصلة، محامون بلا حدود، والمنتدى التونسي
                    للحقوق الاقتصادية والاجتماعية يقوم على متابعة مسار العدالة الإنتقالية ودعم جهود الفاعلين فيه بغاية
                    القطع مع أنّات الماضي وضمان عدم تكرار الانتهاكات الجسيمة لحقوق الإنسان والجرائم الاقتصادية التي وقعت
                    في عهد الدكتاتورية في تونس Project-Id-Version: Justice Transitionnelle
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-20 14:36+0000
PO-Revision-Date: 2020-05-20 14:41+0000
Last-Translator: root <elmanaahoussem@gmail.com>
Language-Team: French (France)
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.1; wp-5.4 les objectifs du projet
 Remettre la justice transitionnelle à l'ordre du jour politique
 Renforcer la capacité des organisations de la société civile à plaider conjointement pour l'adoption de garanties de non-récurrence
 
Le flash a pour but d'éduquer le citoyen sur l'importance des élections, et qu'il existe un lien étroit entre eux
                            Violations et impuretés antérieures à la révolution et leur récurrence dans sa vie quotidienne tant que l'État ne fait pas plus d'efforts
                            Terminer le cours sur la justice transitionnelle et promouvoir la transition démocratique. 789 Voir plus Suivi critique de la voie de la justice transitionnelle
 Sondage 
De la part des Tunisiens, ils voient que les objectifs de la justice transitionnelle à Homma révèlent la vérité sur les différents abus responsables
                                Et en tenant les responsables pour responsables, les réparations et la réhabilitation des victimes pour parvenir à la réconciliation nationale, vous devez être
                                Ils sont présents dans les programmes des politiciens au pouvoir après les élections de 2019 
De la part des Tunisiens, ils estiment que la réconciliation nationale doit être réalisée en réunissant toutes les conditions nécessaires à la voie de la justice transitionnelle, en commençant par la détection de la vérité, par la responsabilisation des auteurs des crimes, en redonnant de la considération aux victimes et en mettant les garanties nécessaires de manière à ce qu'elles ne reviennent pas à ce qui s'est produit auparavant. Il s'agit d'un projet tripartite qui comprend l'Organisation Compass, Avocats sans frontières et le Forum tunisien Les droits économiques et sociaux reposent sur le cheminement de la justice transitionnelle et le soutien actif aux efforts de ses acteurs Rompre avec les gémissements du passé et veiller à ce que les violations flagrantes des droits de l'homme et les crimes économiques qui se sont produits ne se répètent pas A l'ère de la dictature en Tunisie 