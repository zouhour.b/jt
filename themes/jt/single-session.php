<?php
/*
Template Name: session
*/
get_header();
$proces = get_field('proces');
$date = get_field('date');
$ar_months = ["جانفي", "فيفري", "مارس", "آفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
 while (have_posts()):the_post()          
?>
<section data-single=true class="single">
<div class="page-header-doc" style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/bg-3.png' ?>');" >
        <div class="page-header-doc" style="background-image: url('<?= get_field('image', $proces->ID) ?>');" >
        <div class="black-layer-2 ">
            <a href="<?= the_permalink($proces->ID) ?>">
                
                <h5 class="mx-5 px-5 white float-left" style="padding-top: 8rem;"> العودة إلى القضية <img style="width: 1.5rem; height: auto;" src="<?= get_template_directory_uri() . '/assets/icons/arrowWhite.svg' ?>"/></h5>
            </a>
                <div class="container page-header-content">
                    <h1 class="title-ar-1 white mb-4 text-center"><?= $proces->post_title ?></h1>
                    <div class="bigger offset-3 mt-5 px-4" style="color: #fefefe; border-right: 1px solid #fefefe;">

                        <span class="title-ar-3 d-block"><?= get_field('sub_title') ?></span>
                        <span><?php
                        $val = get_field_object('date');
                        $time = strtotime($val['value']);
                        $day = date('d',$time);
                        $year = date('Y',$time);
                        $month = $ar_months[ date('n',$time) - 1 ];
                        ?>
                        <img src="<?= get_template_directory_uri() . '/assets/icons/pin.svg' ?>" class="proces-icon mx-1"/>
                        <?php
                            $address = get_the_terms($proces->ID,'location');
                            $state = get_field('state', $proces->ID);
                            $select_state= empty( $state['value'])?null:$state['value'];
                            $place = empty($address[0]->name)?"لم يحدد بعد":$address[0]->name.' ب'.$state['label'];
                            echo $place;
                        ?>
                        <br><img src="<?= get_template_directory_uri() . '/assets/icons/clock-white.svg' ?>" class="proces-icon mx-1"/>
                        <?= $day.' '.$month.' '.$year.' ' ; ?>
                        <?= empty(get_field('time', $proces->ID))?" ":get_field('time', $proces->ID); ?></span>
                    </div>

            </div>
             <div class="share-btn my-4 float-right">
                    <span><?= __('مشاركة','jt') ?></span><br>
                    <div style="border-top: 1px solid white;" class="mt-2 pt-3 px-1"><?= get_template_part('template', 'sharing-box'); ?></div>
                    </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container my-5">
        <?php echo the_content() ?>
        <div class="d-none">
            <a class="prev"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
                                          class="slide-video-icon"/></a>
            <a class="next"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
                                          class="slide-video-icon"/></a>
        </div>
        </div>
    </section>


    <section id="vids">
    </section>
<?php endwhile ?>


<?php get_footer(); ?>
