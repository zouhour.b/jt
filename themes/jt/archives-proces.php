<?php
/*
Template Name: Archives proces
*/
$url = get_post_permalink();
get_header();
include 'states.php';
if ( pll_current_language() == "fr") {
    $liaison = ' à ';
} else
    $liaison = ' ب';
?>



<section >
    <div class="page-header-sondage"  style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/archives-cover.jpg' ?>');" >
        <div class="black-layer-2 ">
            <div class="container page-header-content">
                    <h1 class="title-ar-1 white mb-4 text-center"><?= __('مراقبة القضايا','jt') ?></h1>
                    <p class="px-5 pt-3 bigger text-right"><?= __('تعالج الدوائر الجنائية المختصة في العدالة الانتقالية أكثر من 200 ملف، ونظرا لاستحالة ملاحظة جميع القضايا، يلاحظ ائتلاف "لارجوع" عينة هامة من الملفات المنشورة أمام هذه الدوائر وذلك وفق معايير دقيقة وموضوعية تمكننا من استخلاص الاستنتاجات والملاحظات التي من شأنها أن تدعم المسار القضائي وأن تقدم للجهات الحكومية الحلول المناسبة.
                ','jt') ?> <br> <?= __('وتتمثل هذه المعايير في:','jt') ?> <br>
                <?= __('-التمثيلية الجغرافية للدوائر: نسعى أن تكون ملفاتنا مختارة من بين ال13  دائرة مختصة المنتشرة في كامل تراب الجمهورية.

               ','jt') ?> <br>
               <span id="dots">...</span><span id="more" class="d-none">
                <?= __('-التمثيلية النوعية: نسعى أن تكون ملفاتنا ممثلة لجميع التيارات النضالية، وجميع التوجهات السياسية من الاتجاه الاسلامي والحركات اليسارية والقومية والطلابية ، وأن تشمل ملفات فريدة لضحايا النساء والمواطنين العزل و ملفات الفساد المالي والاعتداء على المال العام ','jt') ?> <br>

                <?= __('-تمثيلية نوعية الجرائم والانتهاكات : نسعى أن تكون ملفاتنا ممثلة لجميع الانتهاكات الممكنة والتي تعالجها الدوائر القضائية المتخصصة على السبيل الذكر ولا الحصر: الاختفاء القسري، التعذيب، القتل العمد، الايقاف التعسفي، الاعتداء الجنسي، الاعدام دون ضمانات محاكمة عادلة، الاعتداء على المال العام...
                ','jt') ?> <br>
                <?= __('- رمزية الملفات: نسعى أن تكون ملفاتنا قد شملت كل الأحداث ذات الرمزية العالية في بلادنا وأن تغطي الفترة المشمولة بالعدالة الانتقالية كاملة أي من 1955 إلى 2013.','jt') ?> </span> </p>
       <button class="btn w-100" onclick="seeMore()" id="seeMore"> <img src="<?= get_template_directory_uri() . '/assets/icons/white-arrow.png' ?>"/></button>
            </div>
        </div>
 
    </div>
</section>

<section>
<div class="container mt-5">
    <?php 
     if ( pll_current_language() == "ar") {
        $dir = 'rtl';
    }
    else $dir = 'ltr';
    $calendar = new PostTypeCalendar([
        'baseurl'=>get_the_permalink( get_the_ID() ),
        'post_type'=>'session', 'dir'=>$dir]); ?>
    <?= $calendar->renderMonthFromRequest($_GET) ?>
<div id="eventCalendarHumanDate" class="row"></div>
</div>
</section>

<section >
<?php
$page = get_query_var('page', 1); 
    $wp_query = new WP_Query( array(
    'post_type' => 'proces',
    'posts_per_page'   => 6,

    'paged' => $page,
    'meta_key' => 'category',              
    'meta_value' => 'Highlighted',
   
    
    ) );
    
 ?>
 <div class="container mt-5">
         <h3><?= __('تصفية','jt') ?>:</h3>        
    <div class="row">
      <form class="form-inline pr-0 pt-0" method="get" id="searchform" style="padding: 2rem;
       ">
                    <input type="hidden" name="current-lang" id="current-lang" value="<?= pll_current_language() ?>" />
                    <select name="tpi" id="tpi" class="form-control mr-lg-3 mb-2 i-code" onchange="fetchProces()";>
                    <option value=" " selected="selected"><?= __('إسم المحكمة','jt') ?></option>
                    <?php 
                        $tpi = get_terms( array(
                            'taxonomy' => 'location',
                            'hide_empty' => false,
                        ) );
                        $accusationlist = get_terms( array(
                            'taxonomy' => 'accusation',
                            'hide_empty' => false,
                        ) );
                        
                        foreach( $tpi as $location ) {
                          ?>
                        <option value="<?= $location->term_id ?> "><?= $location->name ?></option>
                    <?php } ?>
                    </select>
                    <select name="court" id="court" class="form-control mr-lg-3 mb-2 i-year" onchange="fetchProces()">
                        <option  value="" selected="selected"><?= __('الدائرة','jt') ?></option>
                         <?php foreach( $state as $obj ) {
                         if (pll_current_language() == "ar") {
                            ?>
                              <option value="<?= $obj['val'] ?> "><?= $obj['namear'] ?></option>
                          
                        
                        <?php } else{ ?>
                              <option value="<?= $obj['val'] ?> "><?= $obj['namefr'] ?></option>
                        <?php
                        } } ?>
                    </select>
                    <select name="accusation" id="accusation" class="form-control mr-lg-3 mb-2 i-year" onchange="fetchProces()">
                        <option  value="" selected="selected"><?= __('التهمة','jt') ?></option>
                         <?php foreach( $accusationlist as $obj ) {
                         
                            ?>
                              <option value="<?= $obj->term_id ?> "><?= $obj->name ?></option>
                        <?php
                        }  ?>
                    </select>
                    

       </form>
    </div>
   </div> 

    <div class="container" id="infinite-scroll">
    <?php 
         $i=0;

        $args = array(
            'numberposts' => -1,
            'post_type'   => 'session',
        );
        $all_sessions = get_posts( $args );
         while ($wp_query->have_posts()): $wp_query->the_post();$i++;
            $nSessions = 0;
              foreach ($all_sessions as $session) {
                $proces_Attached = get_field("proces", $session->ID);
                if ( $proces_Attached->ID == get_the_ID() ){
                    $nSessions += 1;
                }
                
              }
              
    ?>
        <div class="row mb-5 py-5 desktop-display">
            <?php 
            
                    if($wp_query->current_post % 2 == 0){ 
            ?>
            
                        <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                        <?php if (get_the_post_thumbnail_url() == false) { ?>
                            <a href="<?= the_permalink() ?>">
                            <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                            </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                            <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"></div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    <?php
                                    $terms = get_the_terms($post,'location');

                                    $stateofthecase = get_field('state');
                                    $val= empty( $stateofthecase['value'])?null:$stateofthecase['value'];
                                    $label = empty( $stateofthecase['label'])?"تونس":$stateofthecase['label'];
                                    $labelfr = null;
                                    foreach($state as $obj) {
                                            if ($val == $obj['val']) {
                                                $labelfr = $obj['namefr'];
                                                break;
                                            }
                                        }
                                    $label = pll_current_language() == "fr"?$labelfr:$label;


                                        if(!empty($terms)){
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= $terms[0]->name.$liaison.$label; ?></h5>
                                    <?php
                                    } else { 
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= __('لم يحدد بعد','jt') ?></h5>
                                    <?php
                                    } 
                                    ?>
                                </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    
                                    <h5 class="d-inline-block" id="num"> <?= __('عدد الجلسات','jt') ?> : <?= $nSessions ?> </h5>
                                </div>
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt') ?></button>
                                </a>
                            </div>
                        </div> 
           
            <?php 
                    } 
                    else { 
            ?>      <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    <?php
                                    $terms = get_the_terms($post,'location');
                                    
                                    $stateofthecase = get_field('state');
                                    $val= empty( $stateofthecase['value'])?null:$stateofthecase['value'];
                                    $label = empty( $stateofthecase['label'])?"تونس":$stateofthecase['label'];
                                    $labelfr = null;
                                    foreach($state as $obj) {
                                            if ($val == $obj['val']) {
                                                $labelfr = $obj['namefr'];
                                                break;
                                            }
                                        }
                                    $label = pll_current_language() == "fr"?$labelfr:$label;

                                    if(!empty($terms)){
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= $terms[0]->name.$liaison.$label; ?></h5>
                                    <?php
                                    } else {
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= __('لم يحدد بعد','jt') ?></h5>
                                    <?php
                                    } 
                                    ?>
                                </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                       
                                    <h5 class="d-inline-block" id="num"> <?= __('عدد الجلسات','jt') ?> : <?= $nSessions ?> </h5>
                                </div>
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt') ?></button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                            <?php if (get_the_post_thumbnail_url() == false) { ?>
                                <a href="<?= the_permalink() ?>">
                                <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                                </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                            <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"> </div>
                            </a><?php } ?>
                        </div> 
            <?php 
                    } 
            ?>
           

            
        </div>
        <div class="row mobile-display">
                    <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                        <?php if (get_the_post_thumbnail_url() == false) { ?>
                            <a href="<?= the_permalink() ?>">
                            <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                            </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                            <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"></div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    <?php
                                    $terms = get_the_terms($post,'location');

                                    $stateofthecase = get_field('state');
                                    $val= empty( $stateofthecase['value'])?null:$stateofthecase['value'];
                                    $label = empty( $stateofthecase['label'])?"تونس":$stateofthecase['label'];
                                    $labelfr = null;
                                    foreach($state as $obj) {
                                            if ($val == $obj['val']) {
                                                $labelfr = $obj['namefr'];
                                                break;
                                            }
                                        }
                                    $label = pll_current_language() == "fr"?$labelfr:$label;


                                        if(!empty($terms)){
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= $terms[0]->name.$liaison.$label; ?></h5>
                                    <?php
                                    } else { 
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= __('لم يحدد بعد','jt') ?></h5>
                                    <?php
                                    } 
                                    ?>
                                </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    
                                    <h5 class="d-inline-block" id="num"> <?= __('عدد الجلسات','jt') ?> : <?= $nSessions ?> </h5>
                                </div>
                                <div style="height: 5rem">
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mb-5 mt-3"><?= __('رؤية المزيد','jt') ?></button>
                                </a>
                                </div>
                            </div>
                        </div> 
           
        </div>
    <?php 
        endwhile;  ?>
       
    </div>
    <?php if ($wp_query->max_num_pages > 1) {
     ?>  <div class="w-100 text-center btn-mobile">
                <button class="load-more my-5" id="load-more1" data-per-page="6" data-max-page="<?= $wp_query->max_num_pages ?>" data-target="#infinite-scroll"
                data-page="<?= $page ?>" data-url="<?= $url ?>">
                <i class="d-none loader fa fa-spin fa-spinner"></i>
                <?= __('عرض المزيد من المحتوي','jt') ?></button> 
        </div>
    <?php }  ?>
  
</section>
<section>
<?php 
$page = get_query_var('paged', 1); 

$wp_query = new WP_Query( array(
    'post_type' => 'proces',
    'posts_per_page'   => 6,
    'paged' => $page,
    'meta_key' => 'category',              
    'meta_value' => 'Not highlighted',
    
));

 ?>
    <div class="container" id="other-posts">
        <?php if ($wp_query->have_posts()) { ?>
            <hr class="seperator">
            <button class="default-btn black no-click py-1 px-3" style="background-image: unset;"><?= __('قضايا أخرى','jt') ?></button>
       <?php } ?>
    
        <div class="row" id="infinite-scroll2">        
        <?php 
        
        while ($wp_query->have_posts()): $wp_query->the_post();
        //$content = get_post_field('post_content', (get_the_ID()))
        ?>
            <div class="col-md-4 px-4 my-3">

            <svg width="35" height="5">
                <rect width="35" height="5" style="fill:#be0a26" />
            </svg>
            <h1 class="card-title my-4"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
            <p class="card-text" style="min-height: 15vh" id="the_excerpt"><?=  wp_trim_words( get_the_content(), 30, ".." ); ?></p>
            <a href="<?= the_permalink() ?>">
                <button class="default-btn black float-left py-1 mt-5"><?= __('رؤية المزيد','jt') ?></button>
            </a>
            </div>
            <?php 
            endwhile; 
        ?>
        </div>
        <?php if ($wp_query->max_num_pages > 1) {  ?>
        <div class="w-100 text-center mb-5">
                <button class="load-more" id="load-more2" data-per-page="2" data-target="#infinite-scroll2"
                data-page="<?= $page ?>" data-url="<?= $url ?>">
                <i class="d-none loader fa fa-spin fa-spinner"></i>
                <?= __('عرض المزيد من المحتوي','jt') ?></button> 
        </div>
        <?php } ?>
    </div>
</section>


<?php get_footer(); ?>
