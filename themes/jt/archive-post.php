<?php
/*
Template Name: archive post
*/
get_header();
?>


<?php
$page = get_query_var('page', 1);
$wp_query = new WP_Query( array(
    'post_type' => 'post',
    'posts_per_page'   => 9,
    'paged' => $page,   
)); ?>
<div class="container-fluid px-5 pt-5" id="infinite-scroll">
	<div class="row mt-5 mx-5">
		<h1 class="title-ar-2 my-5"><?= __('منشورات','jt') ?></h1>
	</div>
	<div class="row mx-5">
	<?php
	while ($wp_query->have_posts()): $wp_query->the_post(); 
		if ($wp_query->current_post == 0) { ?>
			<div class="col-md-9 row smaller-screen-display mb-5">
				<div class="card-img-top image-content col-md-6 " style="height: 33.75rem; width: 33.75rem; background-image: url(<?= get_the_post_thumbnail_url() ?>);">
				</div>
				<div class="col-md-6 side-card">
					<h1 class="card-title my-4 mx-2"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
					<span class="mx-2 pink-bold"><?php echo get_the_date('d F Y'); ?></span>
		            <p class="card-text mx-2" id="the_excerpt"><?=  get_the_excerpt() ?></p>
			    </div>
			</div>
		<?php } elseif (($wp_query->current_post == 1) or ($wp_query->current_post == 2)) { ?>
			<div class="col-12 s-cards">
				<div class="card-img-top image-content col-6 card-thumbnail my-2 px-0" style="background-image: url(<?= get_the_post_thumbnail_url() ?>);">
					<div class=" black-layer-2 card-content">
					<div class="m-4"><h3 class="card-title"><a href="<?= the_permalink() ?>"><?= the_title() ?>		</a></h3>
						<span class="mx-2"><?php echo get_the_date('d F Y'); ?></span>
					</div>
					</div>
			</div>
				
	    	</div>
	    <?php
		} else {
		?> 
		<div class="col-md-4 mt-5 pt-5 mb-5 h-100">
				<div class="card-img-top image-content" style="height: 35vh; width: 100%; background-image: url(<?= get_the_post_thumbnail_url() ?>);">
				</div>
				<div class="card-body">
					<h1 class="card-title my-4"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
		            <p class="card-text" id="the_excerpt"><?=   get_the_excerpt() ?></p>
	            </div>
	    </div>

	<?php }
	endwhile;

	?>
	</div>
</div>
<?php if ($wp_query->max_num_pages -1 != get_query_var('paged')) {
     ?>  <div class="w-100 text-center btn-mobile">
                <button class="load-more" id="load-more1" data-per-page="2" data-target="#infinite-scroll"
                data-page="<?= $page ?>" data-url="<?= $url ?>">
                <i class="d-none loader fa fa-spin fa-spinner"></i>
                <?= __('عرض المزيد من المحتوي','jt') ?></button> 
        </div>
    <?php }  ?>

<?php get_footer(); ?>