
$(document).ready(function() {
    $('.responsiveCarousel').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        adaptiveHeight: true,
        rtl: !($('body').hasClass("lang-fr")),
        nextArrow: $('.carousel-nav-btn.--next'),
        prevArrow: $('.carousel-nav-btn.--prev'),
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
    });
    $( '.carousel-nav-btn.slick-disabled' ).attr('style', 'display: none !important');

  $( '.carousel-nav-btn.slick-arrow' ).on('click',function(e){   
    $( '.carousel-nav-btn.slick-arrow' ).attr('style', 'display: inline-block !important;');
    $( '.carousel-nav-btn.slick-disabled' ).attr('style', 'display: none !important');
  })

  $('#video-slides').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    infinite: true,
    autoplay: true,
    rtl: !($('body').hasClass("lang-fr")),
    nextArrow: $('a.next'),
    prevArrow: $('a.prev'),
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
});
});
