 
$ = jQuery

function fetchProces () {

     data = { 
        action: 'proces_fetch', 
        tpi:  $('#tpi').val(), 
        court:  $('#court').val(), 
        accusation:  $('#accusation').val(),
        lang: $('#current-lang').val() 
         

    }

    $.post( '/wp-json/apirest/getproces/', data ) 
      .always(function(data) {
    
            $('#infinite-scroll').html( data.responseText );      
        });
     $('#load-more1').hide();
     $('#other-posts').hide();
  
}

function fetchDocs() {

     data = { 
        action: 'data_fetch', 
        keyword: $('#keyword').val(), 
        cat: $('#cat').val(), 
        format: $('#format').val(), 
        year: $('#year').val() ,
        lang: $('#current-lang').val() 
    }

    $.post( '/wp-json/apirest/getposts/', data ) 
      .always(function(data) { 
            $('#liveSearch').html( data.responseText );      
    });
 
} 

