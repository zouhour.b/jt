let $ = jQuery;
$.fn.mediaField = function(){
		$(this).after('<div class="file-selection --js-add-selection" style="margin-top:10px"></div>');
		let $input = $(this).hide();
		let $selection = $(this).next();
		let frame = wp.media({
			multiple: 'add', 
			title: 'Select files',
			library: {}
		})

		let refreshView = ()=>{
			$selection.html('')
			if ($input.val() == ''){
			let value = $input.val().split(',').map(x=>{
					let attachment = wp.media.attachment(x);
					attachment.fetch().then(r=>{
							$div = createFileView(r);
							$div.appendTo($selection);
					});
			});
		}
		}

		const createFileView = (r)=>{
				let $div = $('<div>', {style:"background: #EEE; display:flex; align-items:center;margin-bottom:5px:border:1px solid #666;"});
				$div.html('<i class="dashicons dashicons-text-page"></i> <span>'+r.title+'</span>')
				
				return $div;
		}
		
		frame.on('select', function(){
				let selection = frame.state().get('selection').map(x=>x.toJSON().id).join(',');
				$input.val(selection);
				refreshView();
		})

		frame.on('open', function(){
			selection = frame.state().get('selection')
				let value = $input.val().split(",");

			value.map(v=>{
				let attachment = wp.media.attachment(v);
				attachment.fetch();
				selection.add(attachment)
			})
		})
		$(this).before('<a class="button-primary --js-add-selection">Ajouter des fichiers</a>');


		$('.--js-add-selection').click(function(){
			frame.open()
		})
		refreshView();
		

}


$(document).ready(function($){
	$('#acf-field_5ddd23e5a63f3').mediaField();
});

