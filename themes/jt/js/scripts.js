function play_video(url) {
  var video = $('<video />', {
    id: 'video',
    class: 'video-player',
    src: url,
    type: 'video/mp4',
    autoplay: true,
    controls: true,
  });
  $('#video-slides').css('height', 'unset');
  $('#video-slides').html(video);
}
function seeMore() {
  if($('#dots').hasClass('d-none')){
    $('#more').addClass('d-none')
    $('#dots').removeClass('d-none')
    $('#seeMore img').css('transform', 'rotate(-90deg)')
  } else {
    $('#more').removeClass('d-none')
    $('#dots').addClass('d-none')
    $('#seeMore img').css('transform', 'rotate(90deg)')
  }
}
function slider (target, next, prev, autoplay, arrows){
  $(target).slick({
    infinite: true,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    autoplay: autoplay,
    rtl: !($('body').hasClass("lang-fr")),
    nextArrow: next,
    prevArrow: prev,
    arrows: arrows,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: autoplay,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: autoplay,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: autoplay,
        }
      }
    ]
  });
}


function toggleMenu() {
  $('#mobile-menu').toggleClass('open');
  if ($('#mobile-menu').hasClass('open')) {
  $('html').css('overflow-y', 'hidden')
  } else {
  $('html').css('overflow-y', 'auto')
  }
}

$(document).ready(function () {
  
    /*if (document.URL.includes("actualite")){
      
      $('.card-img-top.image-content').each( function() { 
        content = $(this).prev().find('.wp-block-image img');
        console.log($(this).prev().find('.wp-block-image img')[0]);
          img_url = $(content).first().attr('src');
        $(this).css( "background", "url('"+ img_url +"')" )
        //$('.components-responsive-wrapper__content').attr('src') = img_url;
      }) 
    }*/
  $('.col-12.s-cards').wrapAll('<div class="col-md-3 smaller-screen-display px-0"></div>')
  $('.wp-block-embed-facebook .wp-block-embed__wrapper').each( function() {
    $(this).next('figcaption').addBack().wrapAll("<div id='one-vid' class='position-relative' />")
  })
  //$('.wp-block-embed__wrapper .fb-video, .wp-block-embed-facebook figcaption').wrap("<div id='one-vid' class='position-relative' />")
  //> .wp-block-embed__wrapper
  $(".wp-block-embed-youtube , #one-vid").wrapAll("<div id='videos' />");
  if ($('.single').data('single') == true) {
    $('#vids').append($('#videos'))
    slider($('#videos'), $('.d-none a.next:first'), $('.d-none a.prev:first'), false)
    $('#videos .slick-list').append($('.d-none a.next:first'))
    $('#videos .slick-list').append($('.d-none a.prev:first'))

    $('.slick-slide').each( function() {
    $(this).find('.wp-block-embed__wrapper').append($(this).find('.fb-video')) 
        console.log($(this).find('blockquote > p')[0])
      let title = $(this).find('figcaption').html() === ""? $(this).find('iframe').attr("title") : $(this).find('figcaption').html();
      $(this).find('.wp-block-embed__wrapper').after(function() {
        return "<div class='vid-t title-ar-3 col-md-5'>" + title + "</div>";
      });
      $(this).find('.vid-t').wrap("<div id='top-layer' />")
      if ($(this).find('figcaption').html() !== "") {
             $(this).find('blockquote > a:first').html($(this).find('figcaption').html())
             console.log($(this).find('blockquote > a:first'))
       } 
      $(this).find('blockquote > p').addClass('vid-title col-md-5').wrap("<div id='top-layer' />")
      
      $(this).find('#top-layer').append('<div class="black-layer-2" id="vid-layer" style="display: unset; top: 0%; position: absolute; width: 97%; right: 2%; z-index: 99;" />');
      $(this).find('#top-layer').append($(this).find('blockquote > a:first').addClass('vid-t title-ar-3 col-md-5'))
      $(this).find('a').removeAttr("href");
      //$(this).append($('#top-layer'))
    })
$( "div#top-layer" ).on("click", function(e) {
  $(this).css('display', 'none');
});
}
  let initial_text = $('.wp-block-quote').text();
  $('.wp-block-quote').html("<img src='" + base_url + "/wp-content/themes/jt/assets/icons/quotation-mark.svg' style='width: 1rem;'/> <br>" + initial_text)
  $('body').hasClass("lang-fr")?$('.wp-block-file > a').html(' télécharger un fichier'):$('.wp-block-file > a').html(' تحميل الملف')

  if (document.URL.includes("site-search")) {
    $('div.collapse').addClass("in");
  }
  
  slider($('.header-slider'), false, false, true, false);
  slider($('.sondage-slides'), $('.left.arrow'),  $('.right.arrow'), true, true)
  $('ul.blocks-gallery-grid img').each(function(){
   let bg = $(this).attr('src');
    $(this).wrap('<div class="img-container"></div>')
    $('figure.wp-block-gallery').append($('.img-container'))
    $(this).parent().css({'background-image': 'url("'+ bg +'")', 'background-position': 'center', 'background-size': 'cover'})
    $(this).remove()
  })
  $('ul.blocks-gallery-grid').remove();
  slider($('figure.wp-block-gallery'), $('.d-none a.next').first(), $('.d-none a.prev').first(), false, true)
  $('.wp-block-gallery.slick-slider').css({'width': '100%'})
  $('.img-container img').css({'height': '100%', 'object-position': 'center', 'object-fit': 'cover'})
  $('.img-container').css({ 'height': '80vh'});
  let arrowprev = '/wp-content/themes/jt/assets/icons/down-arrow.svg';
      //$('.slick-list').append('<a class="prev"><img src="'+ base_url + arrowprev + '" class="slide-video-icon"/></a>')
      $('figure.wp-block-gallery .slick-list').append($('.d-none a.next').last())
      $('figure.wp-block-gallery .slick-list').append($('.d-none a.prev').last())
  if ($( window ).width()< 500) {
    $('.mobile-card').find('rect').attr("x",4).attr("y",4);
    $('.Vtimeline').find('svg').attr("width", '100%').find('rect').attr("x", "48.5%");
 
  }

 
});

$('.modal').on('shown.bs.modal', function () {
  $('html').css('overflow-y', 'hidden')
});
$('.modal').on('hidden.bs.modal', function () {
  $('html').css('overflow-y', 'auto')
});