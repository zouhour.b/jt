(function($){

	$.fn.accusedField = function(box_name, input_type){
		let input = $(this).find('textarea').hide();
		let value  = input.val();
		input.after('<table class="table accused-field widefat"><thead></thead><tbody></tbody><tfoot><tr><td colspan="3"><a role="add" class="button-secondary button">Ajouter</a></td></tr></tfoot><table>');
		let list = $(this).find('table.accused-field tbody');
		let head = $(this).find('table.accused-field thead');
		value = value?JSON.parse(value):[];
		let options = [];
		let persons = [];
		function fetchPersons(){
			$.get(api_url+'/wp-json/wp/v2/person?per_page=100', function(response){
				persons = [...persons, response];
				response.map(p=>{
					options.push('<option value="'+p.id+'">'+p.name+'</option>')
				})
				render();
			})
		}


		fetchPersons();

		function createLine(line, index){
			let tr = $('<tr>');
			let person = line.person==null?' ':line.person
			tr.html('<td class="person" ><'+input_type+' class="select2" data-name="person" value="'+ person +'"></'+input_type+'></td><td class="charge"><input style="height:30px" class="widefat" data-name="charge" value="'+line.charge+'"/></td><td style="width:20px;"><a class="button button-primary" style=""background:#F33;" role="delete">&times;</a></td>')
			if (input_type == 'select') {	
				tr.find(input_type).html(options.join('')).val(line.person);
			}
			tr.appendTo(list);
		}
		function render(){
			list.html('');
			if (value.length) {
				head.html('<tr><th style="width:200px" class="person">Person</th><th class="charge">'+box_name+'</th><th style="width:20px;"></th></tr>')
			}
			else{
				head.html('');
			}
			value.map(createLine);
		}
		$(this).on('click', '[role="add"]', function(){
			value.push({person:null, charge:''});
			input.val(JSON.stringify(value));
			render();
		});
		$(this).on('click', '[role="delete"]', function(){
			let index = $(this).closest('tr').index();
			value = value.filter((v,i)=>i!==index);
			input.val(JSON.stringify(value));
			render();
		});
		list.on('change','[data-name]',function(e){

			let index = $(this).closest('tr').index();
			let name = $(this).data('name');
			value[index][name] = e.target.value;
			input.val(JSON.stringify(value));
		});

	}
	$(document).ready(function(){
		$('#accused-field').each(function(){
			$(this).accusedField('Charge', 'select');
		});
		$('#comment-field').each(function(){
			$(this).accusedField('Comment', 'input');
		});
	});


})(jQuery)