
function morePosts(btn, appended) {
$(document).on('click', btn,function () {
    let page = $(this).data('page') 
    let target=$(this).data('target')
    let url= $(this).data('url')
    let perPage = $(this).data('perPage')
    let maxPage = $(this).data('maxPage')
    page++;
    if (url.includes('?')){
        url = url+'&page='+page
    }
    else {
        url = url+'?page='+page;
    }
    let $tbody=$('<div>')

    $btn = $(this)
    $btn.find('.loader').removeClass('d-none')
    
    let path = url+' '+ target
    $tbody.load(path, function(){
        $btn.find('.loader').addClass('d-none')
        $btn.data('page', page ).attr('data-page', page)
        $(target).append($tbody.find(target).html())
        if (page >= maxPage){
            $btn.hide();
        }
console.log($tbody.find(appended).length,perPage)
    })
    
console.log(page)
})
}

function loadPersons(target) {
    let index = 0;
        let data = JSON.parse($(target).find('textarea').text())
        let max = (data.length <= 9)?data.length:9;
        let out = '';
        
        for (let i = index; i < max; i++) {
        out += '<div class="col-md-4 px-4 my-3"><svg width="35" height="5"><rect width="35" height="5" style="fill:#be0a26" /></svg><h1 class="card-title my-4"><a href="'+data[i].link+'">'+ data[i].name +'</a></h1><p class="card-text">'+data[i].description+'</p><h6 class= "pink-bold d-inline-block mb-0">'+ translateAccusation +' : <span style="color: black; font-family: Cairo-Regular;">'+ data[i].accusations+'</span></h6><h6 class="mb-4"><img src="'+base_url+'/wp-content/themes/jt/assets/icons/law.svg" class="tax-icon mx-1 mt-2" style="width: 1rem"/>'+ translationNbrProces +': '+data[i].sum+'</h6><a href="'+data[i].link+'"><button class="default-btn black float-left py-1 mt-3">'+ translationLoadMore +'</button></a></div>' 
            
        }
        $(target).append(out);
}

function morePerson (btn) {
    $(document).on('click', btn,function () {
        let index = $(this).data('index')
        let target= $(this).data('target')
        $(this).data('index', index).attr('data-index', index + 9 )
        let data = JSON.parse($(target).find('textarea').text())
        let out = ''
        for (let i = index; i < data.length; i++) {
        out += '<div class="col-md-4 px-4 my-3"><svg width="35" height="5"><rect width="35" height="5" style="fill:#be0a26" /></svg><h1 class="card-title my-4"><a href="'+data[i].link+'">'+ data[i].name +'</a></h1><p class="card-text">'+data[i].description+'</p><h6 class= "pink-bold d-inline-block mb-0">'+ translateAccusation +' : <span style="color: black; font-family: Cairo-Regular;">'+ data[i].accusations+'</span></h6><h6 class="mb-4"><img src="'+base_url+'/wp-content/themes/jt/assets/icons/law.svg" class="tax-icon mx-1 mt-2" style="width: 1rem"/>'+ translationNbrProces +': '+data[i].sum+'</h6><a href="'+data[i].link+'"><button class="default-btn black float-left py-1 mt-3">'+ translationLoadMore +'</button></a></div>'      
        }
        $(target).append(out);
        $(this).css('display', 'none');
    });
}


//morePosts('#load-more1', '.row.mobile-display')
morePosts('#load-more1', '.row.desktop-display')
morePosts('#load-more2', '.col-md-4')
morePosts('.--js-load-more', 'tr')

morePerson('#load-person')
$(document).ready(function () {

if (document.URL.includes("fr")) {
    $('body').css({ "text-align": "left" })
    $('body').addClass('lang-fr')
    $('html').attr("dir", "ltr")
  }
  if($('body').hasClass("lang-fr")){
    translateAccusation = "Les charges";
    translationNbrProces = "Nombres des procés";
    translationLoadMore = "Voir plus";
} else {
    translateAccusation = "التهم الموجهة";
    translationNbrProces = "عدد القضايا";
    translationLoadMore = "رؤية المزيد"
   
}
loadPersons('#morePerson')

})
