
(function($){
    function switchField(){
        if($('#proces-category').find('.acf-input').find('label.selected').text() == "Highlighted"){
            $('.secondary-field').hide();
            $('.primary-field').show()
            } else {
                $('.secondary-field').show();
                $('.primary-field').hide()
            }
    }
$(document).ready(function(){
    $('#proces-category').find('.acf-input').change(function(){
       switchField();
    })
    switchField();
    $('#tagsdiv-location').hide();
    $('#tagsdiv-accusation').hide();
})
})(jQuery)