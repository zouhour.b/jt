function hoverdiv(e,divid){

if ($( window ).width()>= 500) {
    var left  = e.clientX +15  + "px";
    var top  = e.clientY + "px";

    var div = $("#"+divid);

    div.css('left' , left);
    div.css('top' , top);

    $("#"+divid).toggle();
    return false;
  }
}
function diplayCard(e){
    e.preventDefault();
    $( '.mapCarousel' ).css( "height", "0" ).css("overflow-y", "hidden");
    $('div#'+e.target.id).parents("div.mapCarousel").css( "height", "inherit" ).css("overflow-y", "inherit");
    $( '.mapCarousel' ).find('.card').css( "display", "block" );
    $( 'rect' ).removeClass('rect-active');
    $(e.target).addClass('rect-active');
}



$( document ).ready(function() {
  
  $.each( $( '#tunisiamap' ).find('.hoverCard'), function( i, l ){
    if ($('span#' + $(this)[0].id ).length > 1) {
      $('span#' + $(this)[0].id +'> .list').wrapAll('<div>')
      $(this).find('#countProces').html($('span#' + $(this)[0].id ).length);
   
    }
    //$('span#' + $(this)[0].id + '> h3').wrapAll('span#' + $(this)[0].id)
  })

        $.each( $( '#tunisiamap' ).find('.toggle-card'), function( i, l ){
          
          if ($( window ).width()>= 500) {
            $( '.mapCarousel:first').css( "height", "inherit" ).css("overflow-y", "inherit");
            }
            firstRectId = $('#tunisiamap rect:first' )[0].id;
            $( 'rect#'+firstRectId ).addClass('rect-active').attr("z-index",99);
            if($( this ).has( ".card" ).length)  {
                  let mainCard = $( '.'+ $( this ).children('.card')[0].id );
                  mainCard.wrapAll("<div class='mapCarousel'></div>" );               
                  let carousel_card = $( this ).find('.mapCarousel');
                  let modal = $( this ).find('.map-modal');


            if ($( window ).width()< 500) {
              console.log(modal.find('.mapCarousel')[0])
              modal.on('shown.bs.modal', function (e) {
              $( this ).find("#card-modal-body").append($(this).parents().find("div.mapCarousel"))

              carousel_card.resize();    
                e.preventDefault();
                $( '.mapCarousel' ).css( "height", "0" ).css("overflow-y", "hidden");
                let target = e.relatedTarget
                $('div#'+target.id).parents("div.mapCarousel").css( "height", "inherit" ).css("overflow-y", "inherit");
              console.log($('div#'+target.id)[0])
              })
              $('rect').attr('data-toggle', 'modal');
              $('rect').attr('data-target', '#mapModal');
            }
 

          }

          
          });

$('.mapCarousel').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    rtl: !($('body').hasClass("lang-fr")),
    adaptiveHeight: true,
    centerMode: true,
    nextArrow: "<img src="+base_url+"/wp-content/themes/jt/assets/icons/arrow-right-red.png class='map-btn --next'/>",
    prevArrow: "<img src="+base_url+"/wp-content/themes/jt/assets/icons/arrow-left-red.png class='map-btn --prev'/>",
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
});
  $( 'img.slick-disabled' ).attr('style', 'display: none !important');
    $( 'img.slick-arrow' ).on('click', function(e){
      $( 'img.slick-arrow' ).attr('style', 'display: inline-block !important;');
      $( 'img.slick-disabled' ).attr('style', 'display: none !important');
    })

  $('#tunisiamap').find('rect').hover(function(){
    if($( this ).width() != 15){
      $( this ).animate({"width": "15px", "height": "15px"})
    }
  }, function() { 
      $( this ).animate({"width": "10px", "height": "10px"})
      //$(this).unbind('mouseenter mouseleave')

  })




})
