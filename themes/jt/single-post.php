<?php
/*
Template Name: feed
*/

get_header(); 
the_post() ?>
<section data-single=true class="single">
<div class="container py-5">
	<div class="mx-5 px-3 mt-5">
		<h1 class="title-ar-2 my-4 pt-5"><?= the_title() ?></h1>
		<h6 class="pink-bold d-block"><?= get_the_date('d F Y'); ?></h6>
		<p><?= get_the_excerpt(); ?></p>
	</div>
<div class="row mb-3">
	<img src="<?= get_the_post_thumbnail_url() ?>)" class="w-100 px-3">
</div>
<div class="row mx-1">
	<?php the_content() ?>
	 <div class="d-none">
               
     </div>
</div>

		
</div>
</section>
<section id="vids">
	<div class="d-none">
		<a class="prev"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
	                                          class="slide-video-icon"/></a>
	    <a class="next"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
	                                          class="slide-video-icon"/></a>
	    <a class="prev"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
	                                          class="slide-video-icon"/></a>
	    <a class="next"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
	                                          class="slide-video-icon"/></a>
    </div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="share-btn my-4 float-right" style="position: relative;">
		    <span style="color: black"><?= __('مشاركة','jt') ?></span><br>
		    <div style="border-top: 1px solid black;" class="mt-2 pt-3 px-1"><?= get_template_part('template', 'sharing-box'); ?></div>
		    </div>
		</div>
	</div>
</section>

<?php get_footer(); ?>