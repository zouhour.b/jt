    <?php
    include 'states.php';
    global $post;
    if ( pll_current_language() == "fr") {
        $liaison = ' à ';
    } else
        $liaison = ' ب';
      $i=0;
     

    while( $the_query->have_posts() ): $the_query->the_post(); 
        if (get_field('category') == 'Highlighted') {
           

        $args = array(
            'numberposts' => -1,
            'post_type'   => 'session',
        );
        $all_sessions = get_posts( $args );
            $nSessions = 0;
              foreach ($all_sessions as $session) {
                $proces_Attached = get_field("proces", $session->ID);
                if ( $proces_Attached->ID == get_the_ID() ){
                    $nSessions += 1;
                }
                
              }
              
    ?>
        <div class="row my-5 py-5 desktop-display">
            
            <?php 
            
                    if($the_query->current_post % 2 == 0){ 
            ?>
            
                        <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                        <?php if (get_the_post_thumbnail_url() == false) { ?>
                            <a href="<?= the_permalink() ?>">
                            <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                            </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                            <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"></div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    <?php
                                    
                                    $terms = get_the_terms($post,'location');

                                    $stateofthecase = get_field('state');
                                    $val= empty( $stateofthecase['value'])?null:$stateofthecase['value'];
                                    $label = empty( $stateofthecase['label'])?"تونس":$stateofthecase['label'];
                                    $labelfr = null;
                                    foreach($state as $obj) {
                                            if ($val == $obj['val']) {
                                                $labelfr = $obj['namefr'];
                                                break;
                                            }
                                        }
                                    $label = pll_current_language() == "fr"?$labelfr:$label;


                                        if(!empty($terms)){
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= $terms[0]->name.$liaison.$label; ?></h5>
                                    <?php
                                    } else { 
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= __('لم يحدد بعد','jt') ?></h5>
                                    <?php
                                    } 
                                    ?>
                                </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    
                                    <h5 class="d-inline-block" id="num"> <?= __('عدد الجلسات','jt') ?> : <?= $nSessions ?> </h5>
                                </div>
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt') ?></button>
                                </a>
                            </div>
                        </div> 
           
            <?php 
                    } 
                    else { 
            ?>      <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    <?php
                                    $terms = get_the_terms($post,'location');
                                    
                                    $stateofthecase = get_field('state');
                                    $val= empty( $stateofthecase['value'])?null:$stateofthecase['value'];
                                    $label = empty( $stateofthecase['label'])?"تونس":$stateofthecase['label'];
                                    $labelfr = null;
                                    foreach($state as $obj) {
                                            if ($val == $obj['val']) {
                                                $labelfr = $obj['namefr'];
                                                break;
                                            }
                                        }
                                    $label = pll_current_language() == "fr"?$labelfr:$label;

                                    if(!empty($terms)){
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= $terms[0]->name.$liaison.$label; ?></h5>
                                    <?php
                                    } else {
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= __('لم يحدد بعد','jt') ?></h5>
                                    <?php
                                    } 
                                    ?>
                                </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                       
                                    <h5 class="d-inline-block" id="num"> <?= __('عدد الجلسات','jt') ?> : <?= $nSessions ?> </h5>
                                </div>
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt') ?></button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                            <?php if (get_the_post_thumbnail_url() == false) { ?>
                                <a href="<?= the_permalink() ?>">
                                <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                                </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                            <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"> </div>
                            </a><?php } ?>
                        </div> 
            <?php 
                    } 
            ?>
           

            
        </div>
        <?php
        } 
         endwhile; 


          ?>
            
                <?php 
                
                while ($the_query->have_posts()): $the_query->the_post();
                    if (get_field('category') == 'Not highlighted') {
                if ($the_query->current_post == 0) { ?>
                    <hr class="seperator">
                    <button class="default-btn black no-click py-1 px-3" style="background-image: unset;"><?= __('قضايا أخرى','jt') ?></button>
                <div class="row" id="infinite-scroll2">        
                <?php } ?>
                
                    <div class="col-md-4 px-4 my-3">

                    <svg width="35" height="5">
                        <rect width="35" height="5" style="fill:#be0a26" />
                    </svg>
                    <h1 class="card-title my-4"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                    <p class="card-text" style="min-height: 15vh" id="the_excerpt"><?=  wp_trim_words( get_the_content(), 30, ".." ); ?></p>
                    <a href="<?= the_permalink() ?>">
                        <button class="default-btn black float-left py-1 mt-5"><?= __('رؤية المزيد','jt') ?></button>
                    </a>
                    </div>

                    <?php 
                }
                    endwhile; 
                ?>
                
