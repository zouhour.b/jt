<?php
/*
Template Name: Accusation
*/
$url = get_term_link(get_queried_object()->ID);
get_header();
?>


<?php 

 $index_query = new WP_Query( array(
    'post_type' => 'proces',
    'posts_per_page'   => -1,
    
));
            $taxonomy_name = get_queried_object()->name;
            $field = get_field('category');
            $proces_sum = 0;
        while ($index_query->have_posts()): $index_query->the_post();
         $accusations = get_the_terms(get_the_ID(),'accusation');
         if ($accusations) {
            foreach( $accusations as $accusation ):
            if((!empty($accusation->name)) and ($taxonomy_name === $accusation->name)) { 
            $proces_sum += 1;
            }
            endforeach;
                }
        endwhile;
 ?>
    <div class="container" style="padding-top: 6.5rem">

    <div style="background: url(<?= get_template_directory_uri() . '/assets/backgrounds/salesla.svg' ?>) no-repeat top center">
            <h1 class="tax-title mt-5 offset-2" >
            <svg width="35" height="5" style="position: absolute; bottom: 109%;">
                <rect width="35" height="5" style="fill:#be0a26" />
            </svg><?= get_queried_object()->name ?>
            </h1>
        
            <p class="tax-desc offset-2"><?= empty(get_queried_object()->description)?null: get_queried_object()->description . '<br/>' ?>
            </p>
            <h4 class= "offset-2 mb-3">
            <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>" class="tax-icon mx-1 mt-2"/>
            <?= __('عدد القضايا','jt'); ?>: <?= $proces_sum ?>
            </h4>
        </div>
        <?php $index_query = new WP_Query( array(
            'post_type' => 'proces',
            'posts_per_page'   => -1,
            'meta_key' => 'category',              
            'meta_value' => 'Highlighted',
            'tax_query'         => array(
                array(
                    'taxonomy'  => 'accusation',
                    'field'     => 'name',
                    'terms'     => get_queried_object()->name
                )
            )
        )); 
        if ($index_query->have_posts()) { ?>
             <hr class="seperator">
            <button class="default-btn black no-click py-1 px-2" style="background-image: unset;"><?= __('القضايا ذات صلة','jt'); ?></button>
       <?php  } ?>
        
        <?php 
        
            $args = array(
                'numberposts' => -1,
                'post_type'   => 'session'
            );
            $all_sessions = get_posts( $args );
            $post_index = 0;
            while ($index_query->have_posts()): $index_query->the_post();
            $accusations = get_the_terms(get_the_ID(),'accusation');
            if ($accusations) {
                 ?>
                <?php 
                    $nSessions = 0;
                    foreach ($all_sessions as $session) {
                        $proces_Attached = get_field("proces", $session->ID);
                        if ( $proces_Attached->ID == get_the_ID() ){
                            $nSessions += 1;
                        }
                        
                    }
                
        ?>
        <section>
                <div class="row desktop-display my-5 py-5">
                    <?php 
                    $post_index += 1;
                            if($post_index % 2 == 0){ 
                    ?>
                                <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                                <?php if (get_the_post_thumbnail_url() == false) { ?>
                                    <a href="<?= the_permalink() ?>"><img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                                    </a>
                                    <?php } else { ?>
                                    
                                        <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"> </div>                                    </a>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                                    <div class="card-body">
                                        <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                                    
                                        <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                        <div>
                                            <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                                class="proces-icon mx-1"/>
                                            <?php
                                            $terms = get_the_terms($post,'location');

                                            $state = get_field('state');
                                            $val= empty( $state['value'])?null:$state['value'];
                                            $label = $state['label'];

                                                if(!empty($terms[0]->name)){
                                            ?>
                                            <h5 class="card-text mb-0 d-inline-block" id="location"><?= $terms[0]->name.' , '.$label; ?></h5>
                                            <?php
                                            } else { 
                                            ?>
                                            <h5 class="card-text mb-0 d-inline-block" id="location"><?= __('لم يحدد بعد','jt'); ?></h5>
                                            <?php
                                            } 
                                            ?>
                                        </div>
                                        <div>
                                            <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                                class="proces-icon mx-1"/>
                                            
                                            <h5 class="card-text d-inline-block" id="num"><?= __('عدد الجلسات','jt'); ?> : <?= $nSessions ?> </p>
                                        </div>
                                        <a href="<?= the_permalink() ?>">
                                                <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt'); ?></button>
                                        </a>
                                    </div>
                                </div> 
                    <?php 
                            } 
                            else { 
                    ?>      <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                                    <div class="card-body">
                                        <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                                    
                                        <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                        <div>
                                            <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                                class="proces-icon mx-1"/>
                                            <?php
                                            $terms = get_the_terms($post,'location');
                                            
                                            $state = get_field('state');
                                            $val= empty( $state['value'])?null:$state['value'];
                                            $label = $state['label'];

                                            if(!empty($terms[0]->name)){
                                            ?>
                                            <h5 class="card-text mb-0 d-inline-block" id="location"><?= $terms[0]->name.' , '.$label; ?></h5>
                                            <?php
                                            } else {
                                            ?>
                                            <h5 class="card-text mb-0 d-inline-block" id="location"><?= __('لم يحدد بعد','jt'); ?></h5>
                                            <?php
                                            } 
                                            ?>
                                        </div>
                                        <div>
                                            <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                                class="proces-icon mx-1"/>
                                            
                                            <h5 class="card-text d-inline-block" id="num"><?= __('عدد الجلسات','jt'); ?> : <?= $nSessions ?> </h5>
                                        </div>
                                        <a href="<?= the_permalink() ?>">
                                                <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt'); ?></button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                                    <?php if (get_the_post_thumbnail_url() == false) { ?>
                                        <a href="<?= the_permalink() ?>"><img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                                        </a>
                                    <?php } else { ?>
                                    <a href="<?= the_permalink() ?>">
                                    <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"> </div>                                    </a><?php } ?>
                                </div> 
                    <?php 
                            } 
                        
                    ?>
                

                    
                </div>
                <div class="row py-4 mobile-display">
                    <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                        <?php if (get_the_post_thumbnail_url() == false) { ?>
                            <a href="<?= the_permalink() ?>">
                            <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                            </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                            <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"></div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    <?php
                                    $terms = get_the_terms($post,'location');

                                    $state = get_field('state');
                                    $val= empty( $state['value'])?null:$state['value'];
                                    $label = $state['label'];

                                        if(!empty($terms[0]->name)){
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= $terms[0]->name.' ب'.$label; ?></h5>
                                    <?php
                                    } else { 
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= __('لم يحدد بعد','jt'); ?></h5>
                                    <?php
                                    } 
                                    ?>
                                </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    
                                    <h5 class="d-inline-block" id="num"> عدد الجلسات: <?= $nSessions ?> </h5>
                                </div>
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mb-5"><?= __('رؤية المزيد','jt'); ?></button>
                                </a>
                            </div>
                        </div> 
           
                </div>
        </section>
        <?php  }
        endwhile; 
        ?>
        
        </div>

<section>
<?php 
$index_query = new WP_Query( array(
    'post_type' => 'proces',
    'posts_per_page'   => -1,
    'meta_key' => 'category',              
    'meta_value' => 'Not highlighted',
    'tax_query'         => array(
                array(
                    'taxonomy'  => 'accusation',
                    'field'     => 'name',
                    'terms'     => get_queried_object()->name
                )
            )
    
));

 ?>
    <div class="container">
    <?php 
    $arrayAccusation = array();
         while ($index_query->have_posts()): $index_query->the_post();
         
                    $obj = new \stdClass() ;
                    $obj->title = get_the_title();
                    $obj->content = wp_trim_words( get_the_content(), 30, ".." );
                    $obj->link = get_the_permalink();
                    array_push($arrayAccusation, $obj); 
        ?>
    
        <?php 
            endwhile;
            if (!empty($arrayAccusation)) {
        ?>
        <hr class="seperator mt-3">
        <button class="default-btn black no-click py-1 px-3" style="background-image: unset;"><?= __('قضايا أخرى','jt'); ?></button>
        <div class="row mb-5">
        <?php
        foreach ($arrayAccusation as $data) : ?>
            <div class="col-md-4 px-4 my-3">

            <svg width="35" height="5">
                <rect width="35" height="5" style="fill:#be0a26" />
            </svg>
            <h1 class="card-title my-4"><a href="<?= $data->link ?>"><?= $data->title ?></a></h1>
            <p class="card-text" id="the_excerpt"><?=  $data->content; ?></p>
            <a href="<?= $data->link ?>">
                <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt'); ?></button>
            </a>
            </div>
               
            <?php
        endforeach;
         } ?>
        </div>

    </div>
        </section>






<section>
<div class="container">
    <div id="labelAcc">
    
    </div>
            <?php 
            $page = get_query_var('page', 1); 

            $index_query = new WP_Query( array(
                'post_type' => 'proces',
                'posts_per_page'   => -1,
                
            ));
            ?>
    <div class="row" id="morePerson">
    <?php
    $arrayPerson = array(); 
    $acc_sum='';
    $stack = array();
    while ($index_query->have_posts()): $index_query->the_post();
    $accusations = get_the_terms(get_the_ID(),'accusation');
    if ($accusations) {
        foreach( $accusations as $accusation ):
        if((!empty($accusation->name)) and ($taxonomy_name === $accusation->name)) {
       
        $accused = get_field('accused_data', get_the_ID());
        $accused = $accused?json_decode($accused, true):[];
        if ($accused) {
                foreach( $accused as $data ):
                    $person = get_term_by('id', $data['person'], 'person');
                    if (!$person) continue;
                    if ((!in_array($person, $stack)) and (!empty(get_field('public', $person)))){
                        $secondary_query = new WP_Query( array(
                            'post_type' => 'proces',
                            'posts_per_page'   => -1,
                            'meta_key' => 'category',              
                            'meta_value' => 'Highlighted',
                        ));
                        
                        $proces_sum = 0;
                        while ($secondary_query->have_posts()): $secondary_query->the_post();
                        $acc = get_field('accused_data', get_the_ID());
                        $acc = $acc?json_decode($acc, true):[];
                        if ($acc) {
                            $p='';
                            foreach( $acc as $data ):
                                $person_tag = get_term_by('id', $data['person'], 'person');
                                if (!$person_tag) continue;
                                $p .= strval($person_tag->term_id) .',';
                            endforeach;
                            if(strpos($p, strval($person->term_id)) !== false) { 
                                $proces_sum++;
                        $acc_list = get_the_terms(get_the_ID(),'accusation');
                        if ($acc_list) {
                            $a='';
                            foreach( $acc_list as $data ):
                                if(strpos($a, strval($data->name)) === false) { 
                                    $a .= strval($data->name) .' - ';          
                                }
                            endforeach;
                            }
                                    
                        }}
                        
                        endwhile;


                    array_push($stack, $person); 
                    $obj = new \stdClass() ; 
                    $obj->name = $person->name;            
                    $obj->description = wp_trim_words( $person->description, 20, ".." );            
                    $obj->sum = $proces_sum;            
                    $obj->accusations = substr($a, 0, -2);            
                    $obj->link = get_term_link($person, 'person');            

                    array_push($arrayPerson, $obj); 
                    ?>
                   
                    <?php }   
                endforeach;
            }
        }
        endforeach;
    } 
    
    ?>
   <?php endwhile; ?>
   
<textarea class="d-none">

<?=  json_encode($arrayPerson, true) ?>

</textarea>
           
<div class="d-none">
   <?php if (!empty($arrayPerson)){ ?>
       <div id="thisL">
        <hr class="seperator">
        <button class="default-btn no-click black py-1 px-2" style="background-image: unset;"><?= __('المتهمين','jt'); ?></button>
       </div>
   <?php } ?>
    </div>
    
    </div>
    <?php if (count($arrayPerson) > 9) {?>
    <div class="w-100 text-center mb-5">
                <button class="load-more" id="load-person" data-index="9" data-target="#morePerson"
                data-url="<?= $url ?>">
                <i class="d-none loader fa fa-spin fa-spinner"></i>
                <?= __('عرض المزيد من المحتوي','jt'); ?></button> 
    </div>
    <?php } ?>
</div>
</section>

<?php get_footer(); ?>