<?php
$state = array(
    (0) => Array
        (
            'val' => 'tun',
            'namefr' => 'Tunis',
            'namear' => 'تونس'
        ),

    (1) => Array
        (
            'val' => 'ari',
            'namefr' => 'Ariana',
            'namear' => 'اريانة'
        ),

    (2) => Array
        (
            'val' => 'bej',
            'namefr' => 'Beja',
            'namear' => 'باجة'
        ),
    (3) => Array
        (
            'val' => 'bar',
            'namefr' => 'Ben arous',
            'namear' => 'بن عروس'
        ),
    (4) => Array
        (
            'val' => 'biz',
            'namefr' => 'Bizerte',
            'namear' => 'بنزرت'
        ),
    (5) => Array
        (
            'val' => 'gab',
            'namefr' => 'Gabes',
            'namear' => 'قابس'
        ),
    (6) => Array
        (
            'val' => 'gaf',
            'namefr' => 'Gafsa',
            'namear' => 'قفصة'
        ),
    (7) => Array
        (
            'val' => 'jen',
            'namefr' => 'Jendouba',
            'namear' => 'جندوبة'
        ),
    (8) => Array
        (
            'val' => 'kai',
            'namefr' => 'Kairouan',
            'namear' => 'قيروان'
        ),
    (9) => Array
        (
            'val' => 'kas',
            'namefr' => 'Kasserine',
            'namear' => 'قصرين'
        ),
    (10) => Array
        (
            'val' => 'keb',
            'namefr' => 'Kebili',
            'namear' => 'قبلي'
        ),
    (11) => Array
        (
            'val' => 'kef',
            'namefr' => 'Le Kef',
            'namear' => 'الكاف'
        ), 
    (12) => Array
        (
            'val' => 'meh',
            'namefr' => 'Mahdia',
            'namear' => 'المهدية'
        ),
    (13) => Array
        (
            'val' => 'man',
            'namefr' => 'Le Manouba',
            'namear' => 'منوبة'
        ),
    (14) => Array
        (
            'val' => 'med',
            'namefr' => 'Medenine',
            'namear' => 'مدنين'
        ),
    (15) => Array
        (
            'val' => 'mes',
            'namefr' => 'Mestir',
            'namear' => 'المنستير'
        ),
    (16) => Array
        (
            'val' => 'nab',
            'namefr' => 'Nabeul',
            'namear' => 'نابل'
        ),
    (17) => Array
        (
            'val' => 'sfa',
            'namefr' => 'Sfax',
            'namear' => 'صفاقس'
        ),
    (18) => Array
        (
            'val' => 'sil',
            'namefr' => 'Siliana',
            'namear' => 'سليانة'
        ),
    (19) => Array
        (
            'val' => 'sib',
            'namefr' => 'Sidi bouzid',
            'namear' => 'سيدي بوزيد'
        ),
    (20) => Array
        (
            'val' => 'sou',
            'namefr' => 'Sousse',
            'namear' => 'سوسة'
        ),  
    (21) => Array
        (
            'val' => 'tat',
            'namefr' => 'Tattouine',
            'namear' => 'تطاوين'
        ),
    (22) => Array
        (
            'val' => 'toz',
            'namefr' => 'Tozeur',
            'namear' => 'توزر'
        ),
    (23) => Array
        (
            'val' => 'zag',
            'namefr' => 'Zagouane',
            'namear' => 'زغوان'
        ) 
    
);
/*
$arrayAR = array(
    "tun" => "تونس",
    "ari" => "اريانة",
    "bej" => "باجة",
    "bar" => "بن عروس",
    "biz" => "بنزرت",
    "gab" => "قابس",
    "gaf" => "قفصة",
    "jen" => "جندوبة",
    "kai" => "قيروان",
    "kas" => "قصرين",
    "keb " => "قبلي",
    "kef" => "الكاف",
    "meh" => "المهدية",
    "man" => "منوبة",
    "med" => "مدنين",
    "mes" => "المنستير",
    "nab" => "نابل",
    "sfa" => "صفاقس",
    "sil" => "سليانة",
    "sib" => "سيدي بوزيد",
    "sou" => "سوسة",
    "tat" => "تطاوين",
    "toz" => "توزر",
    "zag" => "زغوان",
    "tun" => "Tunis",
    "ari" => "Ariana",
    "bej" => "Beja",
    "bar" => "Ben arous",
    "biz" => "Bizerte",
    "gab" => "Gabes",
    "gaf" => "Gafsa",
    "jen" => "Jendouba",
    "kai" => "Kairouan",
    "kas" => "Kasserine",
    "keb " => "Kebili",
    "kef" => "Le Kef",
    "meh" => "Mahdia",
    "man" => "Le Manouba",
    "med" => "Medenine",
    "mes" => "Mestir",
    "nab" => "Nabeul",
    "sfa" => "Sfax",
    "sil" => "Siliana",
    "sib" => "Sidi bouzid",
    "sou" => "Sousse",
    "tat" => "Tattouine",
    "toz" => "Tozeur",
    "zag" => "Zagouane",
);
*/
?>