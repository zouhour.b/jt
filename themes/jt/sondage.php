<?php
/*
Template Name: Sondage Page
*/
get_header();

?>


<section >
    <div class="page-header-sondage"  >
    <div class="black-layer-2 ">
        <div class="container page-header-content">
            <h1 class="title-ar-1 white mb-4 text-center"><?= __('سبر أراء','jt') ?></h1>
            <div class="px-5 pt-3">
                <p ><?= __('قام فريق المشروع بالاشتراك مع شركة مختصّة في سبر الآراء في شهر أوت 2019، بإجراء سبر للآراء حول رؤية المواطنين والمواطنات للعدالة الإنتقالية. تعلّق سبر الآراء بمواضيع عديدة, منها رؤية التونسيّين والتونسيّات لوضعيّة حقوق الإنسان, للإفلات من العقاب, من كشف الحقيقة ومحاسبة المسؤولين عن الانتهاكات, عن مدى ثقتهم في مؤسّسات الدولة.','jt') ?></p>
                <p ><?= __('إرتكز العمل على منهجيّة علميّة وعيّنة تمثيليّة ل1000 شخص،  شملت مختلف الشرائح العمريّة على كامل التراب التونسي.','jt') ?> </p>
            </div>
        </div>
    </div>
    </div>
</section>

<section>
    <div class="position-relative">
        <div class="sondage-item container">

            <h4 class="text-bg"><?= __('نتائج سبر الأراء','jt') ?> </h4>

                <div class="sondage-slides">
                  
                    <div class="row mx-0 sondage-section mb-md-5">
                        <div class="col-12 sondage-icon mb-4">
                            <img src="<?= get_template_directory_uri() . '/assets/icons/rights.svg' ?>" class="mx-auto mb-4" style="height: 5rem">
                            <h1 class="title-ar-2 text-center"><?= __('تطور وضعية حقوق الانسان في تونس','jt') ?></h1>
                        </div>
                        <div class="col-md-6 row mx-0 px-4 pt-5">
                            <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">82.7%</p>
                                <p class="text-right"><?= __('ملتوانسة يشوفوا إلي التجاوزات في ما يخص حقوق الانسان تحت النظام السابق لازم يتم الاعلان عليهم','jt') ?></p>
                                
                            </div>
                        </div>
                        <div class="col-md-6 px-4 pt-5 right-border-red" id="sondage-desc">
                             <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">70.8%</p>
                                <p class="text-right"><?= __('ملتوانسة يشوفوا إلي لليوم مازال فما إستعمال للعنف في مراكز الشرطة','jt') ?></p>
                            </div>
                        </div>

                    </div>

                    <div class="row mx-0 sondage-section mb-md-5">
                        <div class="col-12 sondage-icon mb-4">
                            <img src="<?= get_template_directory_uri() . '/assets/icons/confiance.svg' ?>" class="mx-auto mb-4" style="height: 5rem">
                            <h1 class="title-ar-2 text-center"><?= __('الثقة في المؤسسات','jt') ?></h1>
                        </div>
                        <div class="col-md-6 row mx-0 px-4 pt-5">
                            <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">58.7%</p>
                                <p class="text-right"><?= __('من التوانسة عندهم ثقة في المؤسسة الأمنية','jt') ?></p>
                                
                            </div>
                        </div>
                        <div class="col-md-6 px-4 pt-5 right-border-red" id="sondage-desc">
                             <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">76%</p>
                                <p class="text-right"><?= __('من التوانسة ماعندهمش ثقة في نواب الشعب','jt') ?></p>
                            </div>
                        </div>

                    </div>

                    <div class="row mx-0 sondage-section mb-md-5">
                        <div class="col-12 sondage-icon mb-4">
                            <img src="<?= get_template_directory_uri() . '/assets/icons/corruption.svg' ?>" class="mx-auto mb-4" style="height: 5rem">
                            <h1 class="title-ar-2 text-center"><?= __('الفساد','jt') ?></h1>
                        </div>
                        <div class="col-md-6 row mx-0 px-4 pt-5">
                            <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">93.9%</p>
                                <p class="text-right"><?= __('من التوانسة يشوفوا إلي الفساد له تأثير سلبي علي الاقتصاد','jt') ?></p>
                                
                            </div>
                        </div>
                        <div class="col-md-6 px-4 pt-5 right-border-red" id="sondage-desc">
                             <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">60.2%</p>
                                <p class="text-right"><?= __('من التوانسة يشوفوا إلي الادارات العمومية بصفة عامة أكثر قطاع فيه فساد','jt') ?></p>
                            </div>
                        </div>

                    </div>

                    <div class="row mx-0 sondage-section mb-md-5">
                        <div class="col-12 sondage-icon mb-4">
                            <img src="<?= get_template_directory_uri() . '/assets/icons/justice.svg' ?>" class="mx-auto mb-4" style="height: 5rem">
                            <h1 class="title-ar-2 text-center"><?= __('العدالة الانتقالية','jt') ?></h1>
                        </div>
                        <div class="col-md-6 row mx-0 px-4 pt-5">
                            <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">83.8%</p>
                                <p class="text-right"><?= __('من التوانسة يشوفوا إلي لازم إصلاح مؤسسات الدولة باش متعاودش تصير خروقات لحقوق الانسان كيما في الأنظمة السابقة','jt') ?></p>
                                
                            </div>
                        </div>
                        <div class="col-md-6 px-4 pt-5 right-border-red" id="sondage-desc">
                             <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">70.1%</p>
                                <p class="text-right"><?= __('من التوانسة يشوفوا إلي المصالحة الوطنية لازم تكون بتحقيق كل الشروط اللازمة لمسار العدالة الانتقالية كيما كشف الحقيقة، محاسبة مرتكبي الجرائم و وضع الضمانات اللازمة باش ما يعاودش يصير إلي صار قبل في الأنظمة السابقة','jt') ?></p>
                            </div>
                        </div>

                    </div>

                    <div class="row mx-0 sondage-section mb-md-5">
                        <div class="col-12 sondage-icon mb-4">
                            <img src="<?= get_template_directory_uri() . '/assets/icons/election.svg' ?>" class="mx-auto mb-4" style="height: 5rem">
                            <h1 class="title-ar-2 text-center"><?= __(' التوانسة و الإنتخابات / الاعلام','jt') ?></h1>
                        </div>
                        <div class="col-md-6 row mx-0 px-4 pt-5">
                            <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">57.7%</p>
                                <p class="text-right"><?= __('من التوانسة معيارهم لانتخاب رئيس الدولة هو النزاهة و الثقة و إحترام القانون','jt') ?></p>
                                
                            </div>
                        </div>
                        <div class="col-md-6 px-4 pt-5 right-border-red" id="sondage-desc">
                             <div class="stats">
                                <p class="percentage text-right" id="sondage-percentage">66.5%</p>
                                <p class="text-right"><?= __('مالتوانسة يشوفوا أن أهداف العدالة الانتقالية لازم يكونوا موجودين في برامج السياسيين إلي باش يكونوا  في برامج الحكم بعد إنتخابات 2019','jt') ?></p>
                            </div>
                        </div>

                    </div>
                </div>

 
      
            
            <div class="text-left py-5 ">
                <a href="<?= get_template_directory_uri() . '/assets/files/Rapport_%20JT_2019.pdf' ?>" download><button class="pdf-btn"><?= __('تحميل الملف','jt') ?></button></a>
            </div>
        </div>
<div class="sondage-slider-controls">
            <img src="<?= get_template_directory_uri() . '/assets/icons/arrow-left-red.png' ?>" class="left arrow"
                 />
            <img src="<?= get_template_directory_uri() . '/assets/icons/arrow-right-red.png' ?>" class="right arrow"
                />
        </div>
</div>


    </div>

</section>

<?php get_footer(); ?>
