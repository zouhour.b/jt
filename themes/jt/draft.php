



<?php 
 
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $argsP = array(
      'post_type' => 'proces',
      'posts_per_page' => 3,
      'paged' => $paged,
      'meta_key' => 'category',              
      'meta_value' => 'Highlighted',
    );
    $index_query = new WP_Query( $argsP );
    
    while($index_query->have_posts()) :
        $index_query->the_post(); ?>
        <h1><?= the_title() ?></h1>
        <?php  endwhile; ?>
    <?php 
     


    
    
    
        if (function_exists("pagination")) {
            pagination($index_query->max_num_pages);
        } 
    

    
function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;
 
    global $paged;
    if(empty($paged)) $paged = 1;
 
    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }
 
    if(1 != $pages)
    {
        echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }
 
        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
        echo "</div>\n";
    }
}?>





<a href="<?= the_permalink($post_ID) ?>">
                    <?php
                        if (get_field('image') == "") { 
                    ?>
                            <img style="width: 100%;" src ="https://i.picsum.photos/id/237/550/368.jpg"/>
                    <?php    
                    } else {
                    ?>
                        <img class="card-img-top" src="<?= get_field('image') ?>" alt="Card image cap">
                    <?php    
                    } 
                    ?>
                        <div class="card-body">
                            <h3 class="card-title" id="the_title"><?= the_title() ?></h3>
                            <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 20, ".." ); ?></p>
                            <div>
                                <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                    class="proces-icon mx-1"/>
                                    <?php $sessions = new WP_Query( array(
                                        'post_type' => 'session',
                                        'posts_per_page'   => -1,
                                    ));
                                    $nSessions = 0;
                                    while ($sessions->have_posts()): $sessions->the_post();
                                    $proces = get_field("proces");
                                    if ($proces->ID == $post_ID) {
                                        $nSessions += 1;
                                    } 
                                    endwhile; 
                                    $terms = get_field('service_municipal', $post_ID);
                                    if(!empty($terms->name)){
                                    ?>
                                    <h5 class="card-text mb-0 d-inline-block" style="font-family: Cairo-Regular;" id="location"><?= $terms->name.' ب'.$label; ?></h5>
                                    <?php
                                    } else {
                                    ?>
                                    <h5 class="card-text mb-0 d-inline-block" style="font-family: Cairo-Regular;" id="location">لم يحدد بعد</h5>
                                    <?php
                                    } 
                                    ?>
                                    </div>
                            <div>
                                <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                    class="proces-icon mx-1"/>
                                    <h5 class="card-text d-inline-block" style="font-family: Cairo-Regular;" id="num"><?= __('عدد الجلسات','jt') ?>: <?= $nSessions ?></h5>
                            </div>
                        
                        </div>
                        </a>
                    </div>
        <script>
        $( document ).ready(function() {

$.each( $( '#tunisiamap' ).find('.toggle-card'), function( i, l ){
    $( '.mapCarousel' ).css( "height", "0" ).css("overflow-y", "hidden");
    $( '.mapCarousel:first').css( "height", "inherit" ).css("overflow-y", "inherit");

    firstRectId = $('#tunisiamap rect:first' )[0].id;
    $( 'rect#'+firstRectId ).attr("width",15).attr("height",15).attr("z-index",99);
if($( this ).children( ".card" ))  {
      let mainCard = $( '.'+ $( this ).children( '.card' )[0].id );
      console.log(mainCard)
       mainCard.wrapAll("<div class='mapCarousel'></div>" );
    
       

if ($( window ).width()< 500) {
$('#mapModal').on('shown.bs.modal', function (e) {
console.log($( this ).parent().next('.mapCarousel'))
$( this ).find("#card-modal-body").append($( this ).parent().next('.mapCarousel'))
$('.mapCarousel').resize();

})
}


$( 'rect#'+ $( this ).children( ".card" ).id).click(function(e){
      console.log($(this))
      e.preventDefault();
    $( '.mapCarousel' ).css( "height", "0" ).css("overflow-y", "hidden");
    $('#'+e.target.id).parents("div.mapCarousel").css( "height", "inherit" ).css("overflow-y", "inherit");
      
    $( 'rect' ).attr("width",10).attr("height",10);
    e.target.setAttribute("width",15);
    e.target.setAttribute("height",15);
    

      });
    }
  });
$('.mapCarousel').slick({
infinite: false,
slidesToShow: 1,
slidesToScroll: 1,
adaptiveHeight: true,
centerMode: true,
nextArrow: "<img src="+base_url+"/wp-content/themes/jt/assets/icons/arrow-right-red.png' class='map-btn --next'/>",
prevArrow: "<img src="+base_url+"/wp-content/themes/jt/assets/icons/arrow-left-red.png' class='map-btn --prev'/>",
responsive: [
{
  breakpoint: 1024,
  settings: {
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    dots: false
  }
},
{
  breakpoint: 600,
  settings: {
    slidesToShow: 1,
    slidesToScroll: 1
  }
},
{
  breakpoint: 480,
  settings: {
    slidesToShow: 1,
    slidesToScroll: 1
  }
}
]
});
$( 'img.slick-disabled' ).attr('style', 'display: none !important');

$( 'img.slick-arrow' ).click(function(e){   
$( 'img.slick-arrow' ).attr('style', 'display: inline-block !important;');
$( 'img.slick-disabled' ).attr('style', 'display: none !important');
})

})
</script>