<?php get_header(); 
if ( pll_current_language() == "fr") {
    include 'states.php';
    $liaison = ' à ';
} else
    $liaison = ' ب';
    ?>


<section id="top">
   
    <div class="black-layer-2" style="position: absolute; z-index: 1;">

            <div class="col-6 mx-auto page-header-content home" style=" top:35% ">
                    <h1 class="title-ar-1 white mb-4 "><span class="red"><?= __('لا','jt') ?></span> <?= __('رجوع','jt') ?></h1>
                    <p class="white home pr-2 pt-3"><?= __('هو مشروع ثلاثي يضم كل من منظمة البوصلة، محامون بلا حدود، والمنتدى التونسي
                    للحقوق الاقتصادية والاجتماعية يقوم على متابعة مسار العدالة الإنتقالية ودعم جهود الفاعلين فيه بغاية
                    القطع مع أنّات الماضي وضمان عدم تكرار الانتهاكات الجسيمة لحقوق الإنسان والجرائم الاقتصادية التي وقعت
                    في عهد الدكتاتورية في تونس','jt') ?></p>
            </div>
            <a href="#projectgoals"> <img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
                                          class="slide-down-icon"/></a>
    </div>
    <div class="header-slider">
        <div class="page-header" style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/cover1.jpg' ?>');">
        </div>
        <div class="page-header" style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/cover2.jpg' ?>');">
        </div>
        <div class="page-header" style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/cover3.png' ?>');">
        </div>
        <div class="page-header" style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/cover4.png' ?>');">
        </div>
        <div class="page-header" style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/cover5.png' ?>');">
        </div>
    </div>
          
</section>

<section>
    <div class="container py-5" id="projectgoals">
        <h1 class="title-ar-2 text-center"><?= __('أهداف المشروع','jt') ?></h1>
        <div class="row mx-0 justice-goals mb-md-5">
            <div class="col-md-4 px-4 mt-md-0 mt-3">
                <div class="image-holder">

                    <img src="<?= get_template_directory_uri() . '/assets/icons/icon1.svg' ?>">
                </div>

                <p class="bigger"><?= __('دعم مجهودات المجتمع المدني لمواصلة مسار العدالة الانتقالية','jt') ?></p>
            </div>
            <div class="col-md-4 px-4 mt-md-0 mt-3">
                <div class="image-holder">
                    <img src="<?= get_template_directory_uri() . '/assets/icons/icon2.svg' ?>">
                </div>


                <p class="bigger"><?= __('إدراج العدالة الإنتقالية صلب الحياة السياسية','jt') ?></p>

            </div>
            <div class="col-md-4 px-4 mt-md-0 mt-3">
                <div class="image-holder">
                    <img src="<?= get_template_directory_uri() . '/assets/icons/icon3.svg' ?>">
                </div>

                <p class="bigger"><?= __('رصد قضايا العدالة الانتقالية أمام الدوائر الجنائية المختصة','jt') ?></p>
            </div>
        </div>
        <a href="<?= pll_current_language() == "ar"?get_page_link(14):get_page_link(532) ?>">
            <button class="default-btn"><?= __('رؤية المزيد','jt') ?></button>
        </a>
    </div>
</section>


<section>
    <div class="video-section px-5" id="video-section">
        <?php
        query_posts(array(
            'post_type' => 'videos',
        ));

        ?>
<div id="video-slides">
        <?php while (have_posts()): the_post(); ?>
            <div>
                <video id="video" width="100%" height="auto" poster="<?= get_field('video_capture') ?>">
                    <source src="<?= get_field('video') ?>" type="video/mp4">
                </video>
                    <div style="position: absolute;top: 42.5%;width: 100%; z-index: 99;">
                        
                            <img src="<?= get_template_directory_uri() . '/assets/icons/play-btn.png' ?>" class="mx-auto play-btn" style="width: 5rem;"
                            onclick="play_video('<?= get_field('video') ?>')"/>
                        
                    </div>
                    <div class="vid-desc">
                        <h3 class="title-ar-3 mx-5"><?php the_title() ?></h3>
                          
                    </div>
                    <div class="black-layer-2" style="top: 0%; position: absolute;">
                    </div>
            </div>
           
        <?php endwhile; ?>
    
</div>
<a class="prev"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
                                          class="slide-video-icon"/>
</a>
<a class="next"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
                                          class="slide-video-icon"/>
</a>
      

    </div>
</section>


<section>
    <div class="position-relative">
        <div class="container py-5 ">
            <h1 class="title-ar-2 text-center"><?= __('رؤية التوانسة للعدالة الانتقالية','jt') ?></h1>
                <div class="sondage-slides">
                    <div class="row mx-0 sondage-section mb-md-5">
                        <div class="col-md-6 row mx-0 px-4 pt-5">
                            <div class="col-4 px-0 sondage-icon">
                                <img src="<?= get_template_directory_uri() . '/assets/icons/013-analysis.svg' ?>">
                            </div>
                            <div class="col-8 stats">
                                <p class="percentage" id="sondage-percentage">66.5%</p>
                                <p id="sondage-info"></p>
                                <a href="<?= pll_current_language() == "ar"?get_page_link(17):get_page_link(492) ?>">
                                    <button class="default-btn mt-5"><?= __('رؤية المزيد','jt') ?></button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 px-4 pt-5 right-border-red" id="sondage-desc">
                            <p class="bigger text-right"><?= __('من التوانسة يشوفوا أنو أهداف العدالة الانتقالية إلّي هوما كشف الحقيقة عن مختلف الانتهاكات مساءلة
                                ومحاسبة المسؤولين عنها وجبر الضرر ورد الاعتبار للضحايا لتحقيق المصالحة الوطنية لازم كونوا
                                موجودين في برامج السياسيين الي باش يكونوا في الحكم بعد انتخابات 2019','jt') ?></p>
                        </div>

                    </div>
                    <div class="row mx-0 sondage-section mb-md-5">
                        <div class="col-md-6 row mx-0 px-4 pt-5">
                            <div class="col-4 px-0 sondage-icon">
                                <img src="<?= get_template_directory_uri() . '/assets/icons/013-analysis.svg' ?>">
                            </div>
                            <div class="col-8 stats">
                                <p class="percentage" id="sondage-percentage">70.1%</p>
                                <p id="sondage-info"></p>
                                <a href="<?= pll_current_language() == "ar"?get_page_link(17):get_page_link(492) ?>">
                                    <button class="default-btn mt-5"><?= __('رؤية المزيد','jt') ?></button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 px-4 pt-5 right-border-red" id="sondage-desc">
                            <p class="bigger text-right"><?= __('من التوانسة يعتبروا أنو المصالحة الوطنية لازم تكون بتحقيق كل الشروط اللازمة لمسار العدالة الإنتقالية بداية بكشف الحقيقة، مرورا بمحاسبة مرتكبي الجرائم، وردّ الإعتبار للضحايا ووضع الضمانات اللازمة باش ما يعاودش يصير إلي صار قبل','jt') ?></p>
                        </div>

                    </div>
                </div>
        </div>
        <div class="sondage-slider-controls" data-dataset="2">
            <img src="<?= get_template_directory_uri() . '/assets/icons/arrow-left-red.png' ?>" class="left arrow"
                 />
            <img src="<?= get_template_directory_uri() . '/assets/icons/arrow-right-red.png' ?>" class="right arrow"
                />
        </div>
    </div>
</section>

<section>
<?php 
 query_posts(array(
    'post_type' => 'proces',
    'posts_per_page'   => -1,
));
 ?>
<div class="bg-gray">
    <div class="container py-5">
        <h1 class="title-ar-2 text-center pt-5"><?= __('متابعة القضايا','jt') ?></h1>

    <div class="row" id="tunisiamap">        
    <div class="col-md-6 mb-2 pt-5" dir="ltr">   

             
        <?php 

            $arraydata = array();

        while (have_posts()): the_post(); 
            $field = get_field('category');
            $dir = pll_current_language() == "fr"?"ltr":"rtl";
            if ($field == 'Highlighted') {
                $state = get_field('state');
                $val= empty( $state['value'])?"tun":$state['value'];
                $label = empty( $state['label'])?"تونس":$state['label'];
                $label = pll_current_language() == "fr"?$array[$val]:$label;
                
                $obj1 = new \stdClass() ; 
                    $obj1->state = $val;            

                    array_push($arraydata, $obj1); 
                    $post_ID = get_the_ID();?>
                    <span class="hoverCard showOnHover bg-white" id="<?= $val ?>" dir=<?= $dir ?>>
                        <h5 class="pink-bold"><?= $label ?></h5>
                        <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>" class="tax-icon mx-1 mt-2"/>
                    <h5 class="d-inline-block"><?= __('عدد القضايا','jt'); ?>: <span id="countProces">1</span></h5>
                        <span class="list d-block">
                            <img src="<?= get_the_post_thumbnail_url() ?>" style="height:2rem; width:2rem; object-fit: cover;" class="img-thumbnail p-0 rounded-0"/>
                            <h6 class="d-inline-block"><?= the_title() ?></h6>
                        </span>   
                    </span>

                <div class="toggle-card">


                    <div class="modal modal-fullscreen force-fullscreen map-modal" id="mapModal" style="">
                        <div class="modal-dialog" style="max-width: 80%;">
                        <div class="modal-content px-3" style="border-radius: 0.8rem;">
                        
                            <!-- Modal Header -->
                            <div class="modal-header" style="border-bottom: none">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body" id="card-modal-body">
                            </div>
                            
                            
                            
                        </div>
                        </div>
                    </div>

                    <div class="card <?= $val ?>" id="<?= $val ?>" style="background-color: unset; border: none;  width:90%" id="one-card">
                        <a href="<?= the_permalink($post_ID) ?>">
                        <?php
                            if (get_the_post_thumbnail_url() == "") { 
                        ?>
                                <img style="width: 100%;" src ="https://i.picsum.photos/id/237/550/368.jpg"/>
                        <?php    
                        } else {
                        ?>
                            <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>'); height:20rem" alt="Card image cap"> </div>
                        <?php    
                        } 
                        ?>
                            <div class="card-body px-0">
                                <h3 class="card-title" id="the_title"><?= the_title() ?></h3>
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 20, ".." ); ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                        <?php
                                        $sessions = new WP_Query( array(
                                            'post_type' => 'session',
                                            'posts_per_page'   => -1,
                           
                                        ));
                                        $nSessions = 0;
                                        while ($sessions->have_posts()): $sessions->the_post();
                                            $val = get_field_object('date');
                                            $time = strtotime($val['value']);
                                        $proces = empty(get_field("proces"))?null:get_field("proces");
                                        $proces = ($proces==null)?0:$proces->ID;
                                        if (($proces == $post_ID) && (date("Y/m/d") >= date('Y/m/d',$time))) {
                                            $nSessions += 1;
                                        } 
                                        endwhile; 
                                        $terms = get_the_terms($post_ID,'location');
                                        if(!empty($terms)){
                                        ?>
                                        <h5 class="card-text mb-0 d-inline-block" style="font-family: Cairo-Regular;" id="location"><?= $terms[0]->name.$liaison.$label; ?></h5>
                                        <?php
                                        } else {
                                        ?>
                                        <h5 class="card-text mb-0 d-inline-block" style="font-family: Cairo-Regular;" id="location"><?= __('لم يحدد بعد','jt') ?></h5>
                                        <?php
                                        } 
                                        ?>
                                        </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                        <h5 class="card-text d-inline-block" style="font-family: Cairo-Regular;" id="num"><?= __('عدد الجلسات','jt') ?> : </h5><h5 class="d-inline-block">  <?= $nSessions ?></h5>
                                </div>
                            
                            </div>
                            </a>
                        </div>
                    </div>
                   
                                    
                   

                <?php
            } endwhile;
                ?>
    </div>
    <div class="col-md-6 text-center map-image">
        <svg x="0px" y="0px"
	        viewBox="0 0 349.2 703.5" style="height: inherit;">
        <?php 
        foreach ($arraydata as $data) {
            switch ($data->state) {  
                case "bar":
                    $x = 215.9;
                    $y = 97.7;
                break;
                case "tun":
                    $x = 197.4;
                    $y = 92.7;
                break;
                case "ari":
                    $x = 205.9;
                    $y = 67.1;
                break;
                case "zag":
                    $x = 197.4;
                    $y = 124.8;
                break; 
                case "sil":
                    $x = 148.7;
                    $y = 147.7;
                break; 
                case "kef":
                    $x = 96.4;
                    $y = 151.3;
                break; 
                case "nab":
                    $x = 254.3;
                    $y = 92.1;
                break;
                case "sou":
                    $x = 227.4;
                    $y = 167.9;
                break;
                case "mes":
                    $x = 257.8;
                    $y = 191.4;
                break;
                case "meh":
                    $x = 248.7;
                    $y = 216.4;
                break;
                case "sfa":
                    $x = 227.4;
                    $y = 272.8;
                break;
                case "sib":
                    $x = 162.8;
                    $y = 261.8;
                break;
                case "gaf":
                    $x = 98.4;
                    $y = 304.5;
                break;
                case "toz":
                    $x = 39.3;
                    $y = 346.8;
                break;
                case "keb":
                    $x = 114.8;
                    $y = 402.7;
                break;
                case "tat":
                    $x = 201.9;
                    $y = 500.8;
                break;
                case "med":
                    $x = 268.3;
                    $y = 192.6;
                break;
                case "kai":
                    $x = 186.9;
                    $y = 192.6;
                break;
                case "gab":
                    $x = 183.3;
                    $y = 359.8;
                break;
                case "kas":
                    $x = 104.3;
                    $y = 233.6;
                break;
                case "bej":
                    $x = 150.8;
                    $y = 99.1;
                break;
                case "jen":
                    $x = 96.4;
                    $y = 96.3;
                break;
                case "biz":
                    $x = 164.2;
                    $y = 52.8;
                break;
                case "man":
                    $x = 172.5;
                    $y = 83.8;
                break;
                default:
                    $x = 197.4;
                    $y = 92.7;
            } ?>
            <rect data-toggle="" data-target="" id="<?= empty($data->state)?"tun":$data->state ?>" x="<?= $x ?>" y="<?= $y ?>" class="st1 rect" style="transform-origin: <?= $x + 5?>px <?= $y + 5?>px; cursor: pointer" onclick="diplayCard(event)" onmouseover="hoverdiv(event,'<?= empty($data->state)?"tun":$data->state ?>')" onmouseout="hoverdiv(event,'<?= empty($data->state)?"tun":$data->state ?>')">
            
            </rect>
            <?php
        }
        ?>
           
        </svg>
        <a href="<?= pll_current_language() == "ar"?get_page_link(157):get_page_link(471) ?>">
            <button class="default-btn"><?= __('رؤية المزيد','jt') ?></button>
        </a>

                   
    </div>
    </div>


    </div>
</div>
</section>


    
<?php get_footer(); ?>

