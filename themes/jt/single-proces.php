<?php
/*
Template Name: proces
*/
get_header();
if ( pll_current_language() == "ar") {
$ar_months = ["جانفي", "فيفري", "مارس", "آفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
} else {
    $ar_months = ["Janvier", "février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"];
    include 'states.php';
    }
?>

<section >
        <div class="page-header-doc" style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/bg-3.png' ?>');" >
        <div class="page-header-doc" style="background-image: url('<?= get_the_post_thumbnail_url() ?>');" >
        <div class="black-layer-2 ">

            <a href="<?= pll_current_language() == "ar"?the_permalink(157):the_permalink(471) ?>">
                
                <h5 class="mx-5 px-5 white float-left" style="padding-top: 8rem; direction: rtl;"> <?= __('جميع القضايا','jt') ?> <img style="width: 1.5rem; height: auto;" src="<?= get_template_directory_uri() . '/assets/icons/arrowWhite.svg' ?>"/></h5>
            </a>
                <div class="container page-header-content single">
                    <h1 class="title-ar-1 white mb-4 text-center"><?php the_title() ?></h1>
                </div>
                    <div class="share-btn my-4 float-right">
                    <span><?= __('مشاركة','jt') ?></span><br>
                    <div style="border-top: 1px solid white;" class="mt-2 pt-3 px-1"><?= get_template_part('template', 'sharing-box'); ?></div>
                    </div>
        </div>
        </div>
    </section>
<?php 
$field = get_field('category');
if ($field == 'Highlighted') {
?>
<section>
    <div class="container position-relative" style="height: 16rem;">
        <div class="row mt-3">
        <div class="carousel-nav w-100">
                    <button class="carousel-nav-btn --prev leftLst left"><img src="<?= get_template_directory_uri() . '/assets/icons/arrow-right-red.png' ?>"/></button>
                    <button class="carousel-nav-btn --next rightLst right"><img src="<?= get_template_directory_uri() . '/assets/icons/arrow-left-red.png' ?>"/></button>
        </div>
        </div>
    <div class="row">
    <?php 
    $post_ID = get_the_ID();
    $sessions = new WP_Query( array(
        'post_type' => 'session',
        'posts_per_page'   => -1,
    ));
    ?>
    <div class="responsiveCarousel w-100 mt-3">
            <?php 
        while ($sessions->have_posts()): $sessions->the_post();
        $proces = get_field("proces");
                if ($proces->ID == $post_ID){
         ?>
                <div>
                    <h6 class="mx-3 date mb-3">
                        <?php 
                        $val = get_field_object('date');
                        $time = strtotime($val['value']);
                        $day = date('d',$time);
                        $year = date('Y',$time);
                        $month = $ar_months[ date('n',$time) - 1 ];
                        echo $day.' '.$month.' '.$year ?>
                    </h6>
                    <div class="px-3 border-top mobile-card" style="border-top-color: black !important;">
                    <svg width="15" height="15">
                        <rect width="12" height="12" style="fill:#be0a26;stroke:white" />
                    </svg>
                    
                        <h5 style="font-family: Cairo-Bold;">
                            <?php echo get_field('sub_title'); ?>
                        </h5>
                        <?php 
                        
                        if(date("Y/m/d") < date('Y/m/d',$time)){ ?>
                            <div class='desc mx-0'> <?= __('لم تتم هذه الجلسة بعد','jt') ; ?> </div>
                            
                        <?php } elseif (get_the_excerpt() == '') { ?>
                           <div class='desc mx-0'> <?= __('ليس هناك معلومات','jt');  ?></div>  
                        <?php } else { ?>
                            <p>
                            <?php echo wp_trim_words( get_the_excerpt(), 25, ".." );?>
                            </p>
                            <a href="<?= the_permalink() ?>" class="d-block" style="font-family: Cairo-Bold; font-size: 12px; color: #be0a26">
                            <?= __('رؤية المزيد','jt');  ?>
                                    <img style="width: 1.2rem; height: auto; transform: rotate(90deg);" class="d-inline-block" src="<?= get_template_directory_uri() . '/assets/icons/arrowPink.svg' ?>"/>
                             </a>
                            
                       <?php }; ?>
                            
                             
                    </div>
                </div>

        <?php } endwhile; ?>
    </div>
    </div>
    </div>
</section>

<section>
    <div class="container">
    <div class="row mt-5">
        <?php
        if (!empty(get_field('accused_data', $post_ID))):              
        ?>
        <div class="col-md-6 col-sm-12">
            <button class="default-btn black py-1 px-3 mt-5" style="background-image: unset;"><?= __('المتهمين وصفتهم أثناء وقوع الحدث','jt') ?></button>
            <div class="box mx-4 px-0 bg-gray" id="more-scroll" data-index="4">
                <div class="d-block scrollable p-4" id="data-1" >
                    <ul>
                        <?php
                            $accused = get_field('accused_data', $post_ID);
                            $accused = $accused?json_decode($accused, true):[];
                            foreach( $accused as $data ): ?>
                                <?php
                                $person = get_term_by('id', $data['person'], 'person');
                                ?>
                                <?php if (!$person) continue; ?>
                                <li>
                                <?php if (empty(get_field('public', $person))) { ?>
                                    <?= $person->name.' '.$data['charge'] ; ?>
                                <?php } else { ?>
                                    <a href="<?= get_term_link((int)$data['person'], 'person') ?>">
                                        <?= $person->name.' '.$data['charge'] ; ?>
                                    </a>
                                <?php } ?>
                                </li>
                                <?php 
                            endforeach; ?>
                            
                    </ul> 
                </div>
               
            
            </div>
            
        </div>
        <?php
        endif               
        ?>
        <?php
        if (!empty(get_field('in_charge_of_personal_right', $post_ID))):              
        ?>
        <div class="col-md-6 col-sm-12">
                <?php
                 $classHide = (get_field('in_charge_of_personal_right', $post_ID)) ? "" : "d-none" ;
                 $classShow = (get_field('in_charge_of_personal_right', $post_ID)) ? "d-none" : "d-block" ;
                 ?>
            <button class="default-btn black px-3 py-1 mt-5" style="background-image: unset;"><?= __('القائمين بالحق الشخصي','jt') ?></button>
            <div class="box mx-4 px-0 bg-gray <?= $classHide ?>" id="more-scroll-2" data-index="4">
                
                <div class="d-block scrollable p-4" id="data-2">
            <ul>
                <?php 
                 $p_rights = get_field('in_charge_of_personal_right', $post_ID);
                if ($p_rights) 
                foreach( $p_rights as $p ): ?>
                 <li><?= $p->name;  ?></li>
                 <?php 
                 endforeach;  
                 ?>
            </ul>
                </div>
                
            </div>
            <div class="box mx-4 px-3 bg-gray <?= $classShow ?>">
            <?= __('ليس هناك معلومات','jt') ?>
            </div>
        </div>
        <?php
        endif               
        ?>
    </div>
        <div class="col-md-12">
            <button class="default-btn black px-3 py-1 mt-5" style="background-image: unset;"><?= __('ملخص الوقائع','jt') ?></button>
            <p class="mt-3">
                <?= get_post_field('post_content', $post_ID)  ?>
            </p>
        </div>
        <div class="col-md-12">
            <button class="default-btn black px-3 py-1 mt-5" style="background-image: unset;"><?= __('التهم الموجهة','jt') ?></button>
            <div class="box">
            <ul>
                <?php
                    $terms = get_the_terms($post_ID,'accusation');
                    if(!empty ($terms)) {
                        foreach ($terms as $term) { ?>
                            <a href="<?= get_term_link($term->name, 'accusation') ?>"><li><?= $term->name  ?></li></a>
                       <?php }
                     

                    } ?>
            </ul>
            </div>
        </div>
        
</section>
<?php 
} else {
?>
<!-- The Vertical Timeline -->
<section> 
    <div class="container">
            <?php 
                $post_ID = get_the_ID();
                $sessions = new WP_Query( array(
                    'post_type' => 'session',
                    'posts_per_page'   => -1,
                ));
                $sum = 0;
                while ($sessions->have_posts()): $sessions->the_post();
                $proces = get_field("proces");
                if ($proces->ID == $post_ID) {
                    $sum += 1;
                }
                endwhile;
            ?>
            <div class="row px-4">
             <p class="my-4"> <?=
             get_post_field('post_content', $post_ID)
             ?></p>
            </div>
        <div class="bg-gray info px-5 mt-4">
            <div class="box">

                <div class="mt-5">
                    
                    <?php
                    $terms = get_the_terms($post_ID,'location');
                    $state = get_field('state', $post_ID);
                    $val= empty( $state['value'])?null:$state['value'];
                    $label = empty( $state['label'])?"تونس":$state['label'];
                    $label = pll_current_language() == "fr"?$array[$val]:$label;

                    if(!empty($terms[0]->name)){
                    ?>
                    <h5 class="d-inline-block" id="location" style="font-family: Cairo-Regular;"><img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                    class="proces-icon mx-1" style="width: 1.1rem; vertical-align: baseline;"/><?= $terms[0]->name.' , '.$label; ?></h5>
                    <?php
                    } else { 
                    ?>
                    <h5 class="d-inline-block" id="location" style="font-family: Cairo-Regular;"><img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                    class="proces-icon mx-1" style="width: 1.1rem; vertical-align: baseline;"/><?= __('لم يحدد بعد','jt') ?></h5>
                    <?php
                    } 
                    ?>
                </div>
                <h5 class="mb-0" style="font-family: Cairo-Regular;">
                <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>" class="proces-icon mx-1" style="width: 1.3rem; vertical-align: baseline;"/>
                <?= __('عدد الجلسات','jt') ?>: <?= $sum; ?>
                </h5>
                <?php
                if (!empty(get_field('victims_number', $post_ID))) { ?>
                   <h4 class="my-3"><span class="pink-bold"><?= __('الضحايا','jt') ?> : </span><?= get_field('victims_number', $post_ID); ?></h4>
               <?php }
               if (!empty(get_field('accused_number', $post_ID))) { ?>
                   <h4 class="my-3"><span class="pink-bold"><?= __('عدد المتهمين','jt') ?> : </span><?= get_field('accused_number', $post_ID); ?></h4>
                <?php
               }
                
                
                        $terms = get_the_terms($post_ID,'accusation');
                    if(!empty ($terms)) { ?>
                    <button class="default-btn black no-click px-3 py-1 mt-2 mb-2" style="background-image: unset;"><?= __('التهم الموجهة','jt') ?></button>
                <ul>
                    <?php
                        foreach ($terms as $term) { ?>
                            <a href="<?= get_term_link($term->name, 'accusation') ?>"><li><?= $term->name  ?></li></a>
                       <?php }
                     

                         ?>
                        <?php 
                         ?>
                </ul>
                <?php 
                    }  
                ?>
            </div>

        </div>


            <ul class="Vtimeline my-5">
                
                <?php
                
                    while ($sessions->have_posts()): $sessions->the_post();
                    $proces = get_field("proces");
                    if ($proces->ID == $post_ID) {
                ?>  

                        <?php 
                                if($sessions->current_post % 2 == 0){ 
                        ?>
                <!-- Item 1 -->
                <li>
                    <svg width="37" height="12">
                    <rect width="12" height="12" style="fill:#be0a26;stroke:white" />
                    </svg>
                        <div class="direction-r">
                            <div class="flag-wrapper">
                                    
                                <span class="flag"><?php 
                                $val = get_field_object('date');
                                $time = strtotime($val['value']);
                                $day = date('d',$time);
                                $year = date('Y',$time);
                                $month = $ar_months[ date('n',$time) - 1 ];
                                echo $day.' '.$month.' '.$year;
                                ?></span>
                                <span class="label-wrapper"><span class="label"><?= get_field('sub_title') ?></span></span>
                            <?php if (get_field("pdf_file") == ""){ 
                                    if ( pll_current_language() == "ar") {
                                        $msg = date("Y-n-d") < date('Y-n-d',$time)?"لم تتم هذه الجلسة بعد":"ليس هناك معلومات";
                                    } else 
                                        $msg = date("Y-n-d") < date('Y-n-d',$time)?"Cette session n'a pas encore eu lieu":"Aucune information"; ?>
                                    <div class="desc"><?= $msg; ?></div>
                            <?php } else{ ?>
                                <a href="<?= get_field("pdf_file") ?>" download="<?= get_field("date") ?>" class="btn-download px-2 py-1"><img src="<?= get_template_directory_uri() . '/assets/icons/002-pdfWhite.svg'?>">تحميل الملف</a>
                            <?php } ?>
                            </div>
                        </div>
                </li>
                                <?php } else { ?>
                <!-- Item 2 -->
                <li>
                    <svg width="22.3rem" height="12">
                    <rect width="12" height="12" style="fill:#be0a26;stroke:white" />
                    </svg>
                        <div class="direction-l">
                            <div class="flag-wrapper">
                                <span class="flag"><?php 
                                $val = get_field_object('date');
                                $time = strtotime($val['value']);
                                $day = date('d',$time);
                                $year = date('Y',$time);
                                $month = $ar_months[ date('n',$time) - 1 ];
                                echo $day.' '.$month.' '.$year 
                                ?></span>
                                <span class="label-wrapper"><span class="label"><?= get_field('sub_title') ?></span></span>
                            <?php if (get_field("pdf_file") == ""){
                                if ( pll_current_language() == "ar") {
                                    $msg = date("Y-n-d") < date('Y-n-d',$time)?"لم تتم هذه الجلسة بعد":"ليس هناك معلومات";
                                } else 
                                    $msg = date("Y-n-d") < date('Y-n-d',$time)?"Cette session n'a pas encore eu lieu":"Aucune information";
                                ?>
                                <div class="desc"><?= $msg; ?></div>
                            <?php } else{ ?>
                                <a href="<?= get_field("pdf_file") ?>" download="<?= get_field("date") ?>" class="btn-download px-2 py-1"><img src="<?= get_template_directory_uri() . '/assets/icons/002-pdfWhite.svg'?>"><?= __('تحميل الملف','jt') ?></a>
                            <?php } ?>
                            </div>
                        </div>
                </li>
                <?php } }?>
                
                <?php endwhile; ?>
            </ul>

    </div>
</section>
<?php 
}
?>
        <?php get_footer(); ?>
