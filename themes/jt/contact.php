<?php
/*
Template Name: Contact Page
*/
get_header();
the_post();
?>



<section >
    <div class="page-header-sondage"  style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/bg-2.jpg' ?>');" >
        <div class="black-layer-2 ">
            <div class="container page-header-content">
                <h1 class="title-ar-1 white mb-4 text-center"><?= __('اتصل بنا','jt') ?></h1>
                <div class="row address-info ">
                    <div class="col-6">
                        <img src="<?= get_template_directory_uri() . '/assets/icons/mail.svg ' ?>" />
                        <p class="white text-ar-2">contact@laroujou3.com</p>
                    </div>
                    <div class="col-6">
                        <img src="<?= get_template_directory_uri() . '/assets/icons/phone-call.svg' ?>"  />
                        <p class="white dir-ltr text-ar-2"><a href="tel:+21671840424" >216-71-840-424</a></p>
                        <p class="white  dir-ltr text-ar-2"><a href="tel:+21671894002" >216-71-894-002</a></p>
                        <p class="white  dir-ltr text-ar-2"><a href="tel:+21671257664" >216-71-257-664</a></p>
                    </div>
                </div>/
            </div>
        </div>
    </div>
</section>


<section>

    <div class="container contact-form">
        <h3 class=""><?= __('استمارة الاتصال','jt') ?></h3>
        <!--
        <form method="POST" action="" name="contactform">
            <div class="col-10" style="margin: 0 8.5%">
                <div class="row mt-5 pt-5 ">
                    <div class="col-md-6 ">
                        <label for="name"><?= __('الإسم','jt') ?></label>
                        <input type="text" class="form-input" id="name" name="name" />
                    </div>
                    <div class="col-md-6 ">
                        <label for="name"><?= __('اللقب','jt') ?></label>
                        <input type="text" class="form-input" id="last-name" name="last-name" />
                    </div>
                </div>

                <div class="col-md-12 px-0">
                    <label for="name"><?= __('البريد الالكتروني','jt') ?></label>
                    <input type="email" class="form-input" id="email" name="email" />
                </div>

                <div class="col-md-12 px-0">
                    <label for="name"><?= __('الرسالة','jt') ?></label>
                    <textarea class="form-input-area" id="msg" name="msg" ></textarea>
                </div>
            </div>
            <div class="text-center">
                <input class="submit-form" type="submit" value="<?= __('إرسال','jt') ?>" name="submit"  />
            </div>
        </form> -->

        <form method="POST" action=""  >
               <div class="col-10" style="margin: 0 8.5%">
                <div class="row mt-5 pt-5 ">
                    <div class="col-md-6 ">
                        <label for="first-name"><?= __('الإسم','jt') ?></label>
                        <input type="text" class="form-input" id="first-name" name="first-name" />
                    </div>
                    <div class="col-md-6 ">
                        <label for="last-name"><?= __('اللقب','jt') ?></label>
                        <input type="text" class="form-input" id="last-name" name="last-name" />
                    </div>
                </div>

                <div class="col-md-12 px-0">
                    <label for="email"><?= __('البريد الالكتروني','jt') ?></label>
                    <input type="email" class="form-input" id="email" name="email" />
                </div>

                <div class="col-md-12 px-0">
                    <label for="msg"><?= __('الرسالة','jt') ?></label>
                    <textarea class="form-input-area" id="msg" name="msg" ></textarea>
                </div>
            </div>
            <div class="text-center">
                <input class="submit-form" type="submit" value="<?= __('إرسال','jt') ?>" name="submit"  />
            </div>
        </form>
    </div>
   
<?php
if (isset($_POST["submit"])) {
    require_once("mail_function.php");

if(!$mail->send()) {
  echo 'Email is not sent.';
  echo 'Email error: ' . $mail->ErrorInfo;
} else { ?>
  <div class="w-100">
      <h3 class="text-center"><?= __('لقد تم وصول رسالتك','jt') ?></h3>;
  </div>
<?php }
}

?>

</section>

<?php get_footer(); ?>
