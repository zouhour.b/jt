
<?php get_header(); 
if ( pll_current_language() == "fr") {
    include 'states.php';
    $liaison = ' à ';
} else
    $liaison = ' ب';
    ?>


<section>
    <?php
        if(empty(get_search_query())){ ?>
            <div class="container">
                <div class="row justify-content-center pb-4" style="padding-top: 10rem;"> 
                <h3 class='text-center mb-4 mt-1 w-100'><?= __('أدخل كلمة مفتاحية','jt'); ?></h3>
                </div>
            </div>
        <?php } else {
    ?>
    <div class="container">
        <div class="row" style="padding-top: 10rem;"> 
        <?php
        $cat_args = array(
            'orderby'       => 'term_id', 
            'order'         => 'ASC',
            'hide_empty'    => true,
            'taxonomy' => 'person', 
            'field' => 'name',
            'meta_key' => 'public',

        );
        $a_args = array(
            'orderby'       => 'term_id', 
            'order'         => 'ASC',
            'hide_empty'    => true,
            'taxonomy' => 'accusation', 
            'field' => 'name',

        );
        $terms = get_terms($cat_args);
        $termsA = get_terms($a_args);
        $existP = false;
        $existA = false;
        foreach ($terms as $term):
            if(strpos($term->name, get_search_query(false)) !== false){ 
                $existP = true;
            break;
            }
        endforeach;
        foreach ($termsA as $term):
            if(strpos($term->name, get_search_query()) !== false){ 
                $existA = true;
            break;
            }
        endforeach;

        query_posts( array( 's' => esc_html( get_search_query( false ) ) ));
            if ((have_posts() == false) && (!$existP) && (!$existA)) { ?>
                <h3 class='text-center mb-5 mt-1 w-100'> <?= __('ليس هناك نتائج', 'jt') ?></h3>
           <?php }
        ?>
        </div>
    </div>

<?php
    
            foreach ($termsA as $term):
            if(strpos($term->name, get_search_query()) !== false){ ?>
        <div class="container">
            <hr class="seperator">
            <button class="default-btn black py-1 px-3" style="background-image: unset;"><?= __('التهم','jt'); ?> </button>
            <?php 
            break; 
        }
        endforeach; ?>
        <div class="row">
        <?php 
        foreach ($termsA as $term):
            if(strpos($term->name, get_search_query()) !== false){ ?>

                <div class="col-md-4 px-4 my-3 py-5">

                <svg width="35" height="5">
                    <rect width="35" height="5" style="fill:#be0a26" />
                </svg>
                <h1 class="card-title my-4"><a href="<?= get_term_link($term) ?>"><?= $term->name ?></a></h1>
                <p class="card-text" id="the_excerpt"><?=  wp_trim_words( $term->description, 20, ".." ); ?></p>
                
                </div>

            <?php }
            
            endforeach;
        ?>
        </div>
        </div>
           
        <?php
       
    
        $terms = get_terms($cat_args);
        foreach ($terms as $term):
            if(strpos($term->name, get_search_query()) !== false){ ?>
        <div class="container">
         
            <hr class="seperator">
            <button class="default-btn black py-1 px-3" style="background-image: unset;"><?= __('أشخاص','jt'); ?></button>
            <?php 
            break; 
        }
        endforeach; ?>
            <div class="row">
            <?php 
            foreach ($terms as $term):
                if(strpos($term->name, get_search_query()) !== false){ ?>

                    <div class="col-md-4 px-4 my-3 py-5">

                    <svg width="35" height="5">
                        <rect width="35" height="5" style="fill:#be0a26" />
                    </svg>
                    <h1 class="card-title my-4"><a href="<?= get_term_link($term) ?>"><?= $term->name ?></a></h1>
                    <p class="card-text" id="the_excerpt"><?=  wp_trim_words( $term->description, 20, ".." ); ?></p>
                    
                    </div>

                <?php }
                
            endforeach;
            ?>
            </div>
        </div>
        







    <div class="container">
        
            <?php
            
             $args = array(
                'numberposts' => -1,
                'post_type'   => 'session',
            );
            $all_sessions = get_posts( $args );
            $query_args = array( 's' => esc_html( get_search_query( false ) ) , 'post_type' => 'proces', 'meta_key' => 'category','meta_value' => 'Highlighted');
            query_posts( $query_args );
            
            while (have_posts()): the_post(); 
             $nSessions = 0;
              foreach ($all_sessions as $session) {
                $proces_Attached = get_field("proces", $session->ID);
                if ( $proces_Attached->ID == get_the_ID() ){
                    $nSessions += 1;
                }
                
              } ?>
              <div class="row my-4">
                <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                        <?php if (get_the_post_thumbnail_url() == false) { ?>
                            <a href="<?= the_permalink() ?>">
                            <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                            </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                            <img class="card-img-top" src="<?= get_the_post_thumbnail_url() ?>" alt="Card image cap">
                            </a><?php } ?>
                        </div>
                        <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    <?php
                                    $terms = get_the_terms($post,'location');

                                    $state = get_field('state');
                                    $val= empty( $state['value'])?null:$state['value'];
                                    $label = $state['label'];

                                        if(!empty($terms[0]->name)){
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= $terms[0]->name.$liaison.$label; ?></h5>
                                    <?php
                                    } else { 
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= __('لم يحدد بعد','jt'); ?></h5>
                                    <?php
                                    } 
                                    ?>
                                </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    
                                    <h5 class="d-inline-block" id="num"><?= __('عدد الجلسات','jt'); ?> : <?= $nSessions ?> </h5>
                                </div>
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt'); ?></button>
                                </a>
                            </div>
                        </div> 
                    </div> 
            <?php
            endwhile;
         ?> </div> <?php


         $query_args = array( 's' => esc_html( get_search_query( false ) ) , 'post_type' => 'session');
         query_posts( $query_args );
             if (have_posts() == true) { ?>
             <hr class="seperator">
             <button class="default-btn black py-1 px-3" style="background-image: unset;"><?= __('جلسات','jt'); ?> </button>
             <div class="row">
                <?php while (have_posts()): the_post(); ?>
                    
                    <div class="col-md-4 px-4 my-3">

                    <svg width="35" height="5">
                        <rect width="35" height="5" style="fill:#be0a26" />
                    </svg>
                    <h1 class="card-title my-4"><a ><?= the_title() ?></a></h1>
                    <p class="card-text" id="the_excerpt"><?=  wp_trim_words( get_the_content(), 20, ".." ); ?></p>
                    <?php   $val = get_field_object('date');
                            $time = strtotime($val['value']); 
                        if(date("Y/m/d") < date('Y/m/d',$time)){ ?>
                                <div class='desc mx-0'> <?= __('لم تتم هذه الجلسة بعد','jt') ; ?> </div>
                                
                            <?php } elseif (get_the_excerpt() == '') { ?>
                            <div class='desc mx-0'> <?= __('ليس هناك معلومات','jt');  ?></div>  
                            <?php } else { ?>
                                <p>
                                <?php echo wp_trim_words( get_the_excerpt(), 25, ".." );?>
                                </p>
                                <a href="<?= the_permalink() ?>" class="d-block" style="font-family: Cairo-Bold; font-size: 0.8rem; color: #be0a26">
                                <?= __('رؤية المزيد','jt');  ?>
                                        <img style="width: 1.2rem; height: auto; transform: rotate(90deg);" class="d-inline-block" src="<?= get_template_directory_uri() . '/assets/icons/arrowPink.svg' ?>"/>
                                </a>
                                
                        <?php }; ?>
                    </div>
                    <?php 
                    endwhile; 

                } ?>
        
    </div>
        <?php

        $query_args = array( 's' => esc_html( get_search_query( false ) ) , 'post_type' => 'proces', 'meta_key' => 'category','meta_value' => 'Not highlighted');
        query_posts( $query_args );
            if (have_posts() == true) {  ?>
        <div class="container">
            <hr class="seperator">
            <button class="default-btn black py-1 px-3" style="background-image: unset;"><?= __('قضايا أخرى','jt'); ?></button> 
                <div class="row">      
                    <?php while (have_posts()): the_post(); ?>
                    
                    <div class="col-md-4 px-4 my-3">

                    <svg width="35" height="5">
                        <rect width="35" height="5" style="fill:#be0a26" />
                    </svg>
                    <h1 class="card-title my-4"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                    <p class="card-text" id="the_excerpt"><?=  wp_trim_words( get_the_content(), 20, ".." ); ?></p>
                    <a href="<?= the_permalink() ?>">
                        <button class="default-btn black float-left py-1 mt-5"><?= __('رؤية المزيد','jt'); ?></button>
                    </a>
                    </div>
                    <?php 
                    endwhile; 
                    ?>
                </div>
        </div>
        <?php }
      
       

        $query_args = array( 's' => esc_html( get_search_query( false ) ) , 'post_type' => 'documents');
        query_posts( $query_args );
            if (have_posts() == true) { ?>
        <div class="container">
            <hr class="seperator">
            <button class="default-btn black py-1 px-3" style="background-image: unset;"><?= __('وثائق','jt'); ?> </button>
        <div class="page-of-title">

            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th class="text-center"><?= __('العناوين','jt'); ?></th>
                        <th></th>
                        <th class="text-center"><?= __('السنة','jt'); ?></th>
                    </tr>
                </thead>
                <tbody>

                <?php while (have_posts()): the_post(); 
                $category = get_the_category(); 
                if ( pll_current_language() == "ar") {
                    $link = $category[0]->slug;
                } else $link = substr($category[0]->slug, 0, -3) ;
                ?>
                <tr>
                    <td class="i-title"><a href="<?= the_permalink() ?>"><img src=" <?= get_template_directory_uri() . '/assets/icons/'. $link ?>.svg"/></a></td>
                    <td class="right"><a href="<?= the_permalink() ?>"><?=the_title()?>   </a></td>
                    <td class="center"><a href="<?= the_permalink() ?>"><?= get_the_date( 'd.m.Y') ?></a></td>
                </tr>
                         

                <?php endwhile; ?>
                </tbody>
                </table>
        </div>
            <?php } ?>
        
        </div>



<?php

$query_args = array( 's' => esc_html( get_search_query( false ) ) , 'post_type' => 'post');
        query_posts( $query_args );
            if (have_posts() == true) { ?>
        <div class="container">
            <hr class="seperator">
            <button class="default-btn black py-1 px-3" style="background-image: unset;"><?= __('منشورات','jt'); ?> </button>
            <?php 
        
         ?>
        <div class="row">
        <?php 
        while (have_posts()): the_post();  ?>

                <div class="col-md-4 px-4 my-3 py-5">
                        <?php if (get_the_post_thumbnail_url() == false) { ?>
                            <a href="<?= the_permalink() ?>">
                                <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                            </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                                <div class="card-img-top" style="background-image: url(<?= get_the_post_thumbnail_url() ?>); height: 20rem;"></div>
                            </a><?php } ?>
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                
                                
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt'); ?></button>
                                </a>
                            </div>
                
                </div>

            <?php 
            
        endwhile;
        ?>
        </div>
        </div>
        <?php } ?>

<?php } ?>
</section>
<?php get_footer(); ?>
