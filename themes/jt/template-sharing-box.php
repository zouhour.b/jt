<?php

$postUrl = 'http' . ( isset( $_SERVER['HTTPS'] ) ? 's' : '' ) . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; 
if (is_singular( 'post' )) {
	$img_url_twitter = get_template_directory_uri().'/assets/icons/twitter-black.svg';
	$img_url_fb = get_template_directory_uri().'/assets/icons/fb-black.svg';
	$img_url_email = get_template_directory_uri().'/assets/icons/envelope-black.svg';
} else{
	$img_url_twitter = get_template_directory_uri().'/assets/icons/twitter.svg';
	$img_url_fb = get_template_directory_uri().'/assets/icons/fb.svg';
	$img_url_email = get_template_directory_uri().'/assets/icons/envelope.svg';
}
?>
<section class="sharing-box content-margin content-background clearfix">
  <div class="share-button-wrapper">
  <a target="_blank" class="list-icons twitter-icon" href="https://twitter.com/intent/tweet?url=<?php echo $postUrl; ?>&text=<?php echo the_title(); ?>&via=<?php the_author_meta( 'twitter' ); ?>" title="Tweet this" style="background: url(<?= $img_url_twitter ?>);"></a>
  <a target="_blank" class="list-icons facebook-icon mx-4" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $postUrl; ?>&picture=<?php echo get_field('image'); ?>" title="Share on Facebook" style="background: url(<?= $img_url_fb ?>);"></a>
  <a target="_blank" class="list-icons share-email-icon" href="mailto:?subject=<?php echo the_title(); ?>&body=<?php echo $postUrl; ?>" title="envoyer un email" style="background: url(<?= $img_url_email ?>);"></a>
  </div>
</section>