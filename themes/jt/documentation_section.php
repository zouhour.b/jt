<section>

    <div class="documents-section">
        <div class="container py-5 ">
            <h1 class="title-ar-2 text-center mb-5 mt-4"><?= __('توثيق','jt') ?></h1>
            <p class="my-5 text-center"><?= __('تجدون في هذه الصفحة جردا لمحتلف الوثائق المتعلقة بالعدالة الانتقالية من بداية
                المسار إلى غاية اليوم','jt') ?> </p>
                <?php if ( pll_current_language() == "ar") {
                    $link = "documentation/";
                } else $link = "documentation-2/" ;
                ?>
            <div class="row mx-0 pt-4">
                <div class="col documents-icon">
                <form class="form-inline   justify-content-center" method="get" id="searchform" action=<?= $link ?>>
                <input type="hidden" name="cat" value="96" />
                <input type="hidden" name="site_section" value="documents" />
                <input type="hidden" name="year" />
                    <button type="submit" class="cat-btn" type="submit" id="searchsubmit" name="keyword" id="keyword">
                        <img src="<?= get_template_directory_uri() . '/assets/icons/category1.svg' ?>"/>
                        <p><?= __('وثائق المجلس الوطني التأسيسي','jt') ?></p>
                    </button>
                </form>
                </div>
                <div class="col documents-icon">
                <form class="form-inline   justify-content-center" method="get" id="searchform" action=<?= $link ?>>
                <input type="hidden" name="cat" value="98" />
                <input type="hidden" name="year" value="" />
                <input type="hidden" name="site_section" value="documents" />
                <input type="hidden" name="year" />
                    <button type="submit" class="cat-btn" type="submit" id="searchsubmit" name="keyword" id="keyword">
                        <img src="<?= get_template_directory_uri() . '/assets/icons/category2.svg' ?>"/>
                        <p><?= __('وثائق متعلقة بهيئة الحقيقة و الكرامة','jt') ?></p>
                    </button>
                </form>
                </div>
                <div class="col documents-icon">
                <form class="form-inline   justify-content-center" method="get" id="searchform" action=<?= $link ?>>
                <input type="hidden" name="cat" value="100" />
                <input type="hidden" name="site_section" value="documents" />
                <input type="hidden" name="year" />
                    <button type="submit" class="cat-btn" type="submit" id="searchsubmit" name="keyword" id="keyword">
                    <img src="<?= get_template_directory_uri() . '/assets/icons/category3' ?>.svg"/>
                    <p><?= __('حصيلة الإجراءات الحكومية','jt') ?> </p>
                    </button>
                </form>
                </div>
                <div class="col documents-icon">
                <form class="form-inline   justify-content-center" method="get" id="searchform" action=<?= $link ?>>
                <input type="hidden" name="cat" value="102" />
                <input type="hidden" name="site_section" value="documents" />
                <input type="hidden" name="year" />
                    <button type="submit" class="cat-btn" type="submit" id="searchsubmit" name="keyword" id="keyword">
                        <img src="<?= get_template_directory_uri() . '/assets/icons/category4.svg' ?>"/>
                        <p><?= __('رصد أعمال لجنة البرلمانية','jt') ?></p>
                    </button>
                </div>
                </form>

            </div>
            <a href="<?= pll_current_language() == "ar"?get_page_link(30):get_page_link(477) ?>">
                <button class="default-btn mt-5"><?= __('رؤية المزيد','jt') ?></button>
            </a>
        </div>
    </div>
</section>