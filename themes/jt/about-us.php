<?php
/*
Template Name: About us Page
*/
get_header();
?>



<section >
    <div class="page-header-sondage"  style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/archives-cover.jpg' ?>');" >
        <div class="black-layer-2 ">
            <div class="container page-header-content">
                <h1 class="title-ar-1 white mb-4 text-center"><?= __('من نحن','jt'); ?></h1>
            </div>
        </div>
    </div>
</section>

<section >
    <div class="container py-5" >
        <h1 class="title-ar-3 mb-4"><?= __('لا رجوع','jt'); ?></h1>
        <p class=" px-0 bigger text-right"><?= __('هو مشروع ثلاثي يضم كل من منظمة البوصلة، محامون بلا حدود، والمنتدى التونسي للحقوق الاقتصادية والاجتماعية يقوم على متابعة مسار العدالة الإنتقالية ودعم جهود الفاعلين فيه بغاية القطع مع أنّات الماضي وضمان عدم تكرار الانتهاكات الجسيمة لحقوق الإنسان والجرائم الاقتصادية التي وقعت في عهد الدكتاتورية في تونس
            وضعت المنظمات الرائدة لهذا المشروع نهجا شاملا يستند إلى ملاحظاتها المشتركة فيما يتعلق بمسار العدالة الانتقالية في تونس ورسمت ثلاثة أهداف مترابطة مفصليا على النحو التالي','jt'); ?>
        </p>

        <div class="row mx-0 justice-goals mb-md-5 mt-5">
            <div class="col-md-4 px-4 mt-md-0 mt-3">
                <div class="image-holder">
                    <img src="<?= get_template_directory_uri() . '/assets/icons/icon1.svg' ?>" >
                </div>
                <p class="bigger"><?= __('دعم مجهودات المجتمع المدني لمواصلة مسار العدالة الانتقالية','jt'); ?></p>
            </div>
            <div class="col-md-4 px-4 mt-md-0 mt-3">
                <div class="image-holder">
                    <img src="<?= get_template_directory_uri() . '/assets/icons/icon2.svg' ?>" >
                </div>
                <p class="bigger"><?= __('إدراج العدالة الإنتقالية صلب الحياة السياسية','jt'); ?></p>

            </div>
            <div class="col-md-4 px-4 mt-md-0 mt-3">
                <div class="image-holder">
                    <img src="<?= get_template_directory_uri() . '/assets/icons/icon3.svg' ?>" >
                </div>
                <p class="text-center px-0 bigger"><?= __('رصد قضايا العدالة الانتقالية أمام الدوائر الجنائية المختصة','jt'); ?></p>
            </div>
        </div>

    </div>
</section>
<hr class="large">

<section>
    <div class="container about-us-content">
        <h3><?= __('شركاؤنا','jt'); ?></h3>
        <p><?= __('بمبادرة مشتركة، ركز الشركاء الثلاث خبراتهم وتصوراتهم لبعث هذ المشروع الهادف إلى إستكمال مسار العدالة الانتقالية من طرف المجتمع المدني','jt'); ?></p>
        <p><strong><?= __('البوصلة','jt'); ?> :</strong><?= __('البوصلة هي منظمة غير حكومية وفقا للقانون التونسي و غير هادفة للربح و هي مستقلة عن جميع التيارات السياسية','jt'); ?>  .</p>
        <p><strong><?= __('محامون بلاحدود','jt'); ?> :</strong><?= __('منظمة دولية مقرها الرئيسي بلجيكا ولديها فروع في مختلف بلدان العالم بما فيها تونس تعمل على توعية المواطنين والمواطنات بحقوقهم الأساسية، ودعم المجتمع المدني والمحامين لضمان حقوق المتقاضين بشكل أفضل','jt'); ?> </p>
        <p><strong><?= __('المنتدى التونسي للحقوق الاقتصادية والاجتماعية','jt'); ?> :</strong>
        <?= __('هي منظمة حقوقية غير حكومية، محايدة ومستقلة عن أي حزب سياسي أو مؤسسة دينية. تم إنشاؤها في عام 2011 بهدف الدفاع
            على المواضيع التالية الحقّ في العمل، حقوق المرأة، الحقوق البيئيّة، حقوق المهاجرين','jt'); ?> 
        </p>
        <div class="row partners">
            <div class="col"><a href="http://www.albawsala.com/" target="_blank"><img src="<?= get_template_directory_uri() . '/assets/logos/logo_albawsala.png' ?>"> </a></div>
            <div class="col"><a href="https://www.asf.be/action/field-offices/asf-in-tunisia/" target="_blank"><img src="<?= get_template_directory_uri() . '/assets/logos/logo_asf.png' ?>"> </a></div>
            <div class="col"><a href="https://ftdes.net/" target="_blank"> <img src="<?= get_template_directory_uri() . '/assets/logos/logo_ftdes.jpg' ?>"> </a></div>
        </div>
    </div>
</section>

<?php get_footer(); ?>