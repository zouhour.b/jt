<?php
/**
 * Created by PhpStorm.
 * User: 09
 * Date: 17/09/2018
 * Time: 14:15
 */


class Calendar_events
{

    public function Calendar_events()
    {

        add_action('rest_api_init', array($this, 'register_api_hooks'));
    }


    function register_api_hooks()
    {
        register_rest_route(
            'custom-plugin', '/calendar-events/',
            array(
                'methods'  => 'GET',
                'callback' => array($this, 'events'),
            )
        );
        register_rest_route(
            'custom-plugin', '/calendar-events-ar/',
            array(
                'methods'  => 'GET',
                'callback' => array($this, 'events_ar'),
            )
        );
        register_rest_route(
            'custom-plugin', '/calendar-events-en/',
            array(
                'methods'  => 'GET',
                'callback' => array($this, 'events_en'),
            )
        );
        register_rest_route(
            'custom-plugin', '/calendar-lang-fr/',
            array(
                'methods'  => 'GET',
                'callback' => array($this, 'lang_fr'),
            )
        );
        register_rest_route(
            'custom-plugin', '/calendar-lang-en/',
            array(
                'methods'  => 'GET',
                'callback' => array($this, 'lang_en'),
            )
        );
        register_rest_route(
            'custom-plugin', '/calendar-lang-ar/',
            array(
                'methods'  => 'GET',
                'callback' => array($this, 'lang_ar'),
            )
        );

    }

    function events()
    {
        $widget = get_option('Widget_events');

        $result = '';

        $result    = $result . '[';
        $separator = "";

        $i      = 1;
        $result = $result . $separator;


        $args = array(
            'category_name'  => 'evenements',
            'posts_per_page' => $widget[2]['nbrpost'],
            'meta_key'       => 'date',
            'orderby'        => 'meta_value',
            'order'          => 'ASC'
        );

        $the_query    = new WP_Query($args);
        $numberEvents = $the_query->post_count;
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()): $the_query->the_post();

                if ($numberEvents == $i) {
                    $result = $result . '  { "date": "' . date("Y-m-d H:i:00",
                            strtotime(get_field('date'))) . '", "type": "test", "title": "' . get_the_title() . '", "description": "'.get_the_excerpt().'", "url": "' . get_permalink() . '" , "time": "' . get_field('heure') . '", "location": "' . get_field('lieux') . '"}';

                } else {

                    $result = $result . '  { "date": "' . date("Y-m-d H:i:00",
                            strtotime(get_field('date'))) . '", "type": "test", "title": "' . get_the_title() . '", "description": "'.get_the_excerpt().'", "url": "' . get_permalink() . '" , "time": "' . get_field('heure') . '", "location": "' . get_field('lieux') . '" },';

                }

                $i++;

            endwhile;
        }

        $result = $result . ']';

        return json_decode($result);
    }
    function events_ar()
    {
        $widget = get_option('Widget_events');

        $result = '';

        $result    = $result . '[';
        $separator = "";

        $i      = 1;
        $result = $result . $separator;


        $args = array(
            'cat'  => 96,
            'posts_per_page' => $widget[2]['nbrpost'],
            'meta_key'       => 'date',
            'orderby'        => 'meta_value',
            'order'          => 'ASC'
        );

        $the_query    = new WP_Query($args);
        $numberEvents = $the_query->post_count;
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()): $the_query->the_post();

                if ($numberEvents == $i) {
                    $result = $result . '  { "date": "' . date("Y-m-d H:i:00",
                            strtotime(get_field('date'))) . '", "type": "test", "title": "' . get_the_title() . '", "description": "'.get_the_excerpt().'", "url": "' . get_permalink() . '" , "time": "' . get_field('heure') . '", "location": "' . get_field('lieux') . '"}';

                } else {

                    $result = $result . '  { "date": "' . date("Y-m-d H:i:00",
                            strtotime(get_field('date'))) . '", "type": "test", "title": "' . get_the_title() . '", "description": "'.get_the_excerpt().'", "url": "' . get_permalink() . '" , "time": "' . get_field('heure') . '", "location": "' . get_field('lieux') . '" },';

                }

                $i++;

            endwhile;
        }

        $result = $result . ']';

        return json_decode($result);
    }
    function events_en()
    {
        $widget = get_option('Widget_events');

        $result = '';

        $result    = $result . '[';
        $separator = "";

        $i      = 1;
        $result = $result . $separator;


        $args = array(
            'cat'  => 57,
            'posts_per_page' => $widget[2]['nbrpost'],
            'meta_key'       => 'date',
            'orderby'        => 'meta_value',
            'order'          => 'ASC'
        );

        $the_query    = new WP_Query($args);
        $numberEvents = $the_query->post_count;
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()): $the_query->the_post();

                if ($numberEvents == $i) {
                    $result = $result . '  { "date": "' . date("Y-m-d H:i:00",
                            strtotime(get_field('date'))) . '", "type": "test", "title": "' . get_the_title() . '", "description": "'.get_the_excerpt().'", "url": "' . get_permalink() . '" , "time": "' . get_field('heure') . '", "location": "' . get_field('lieux') . '"}';

                } else {

                    $result = $result . '  { "date": "' . date("Y-m-d H:i:00",
                            strtotime(get_field('date'))) . '", "type": "test", "title": "' . get_the_title() . '", "description": "'.get_the_excerpt().'", "url": "' . get_permalink() . '" , "time": "' . get_field('heure') . '", "location": "' . get_field('lieux') . '" },';

                }

                $i++;

            endwhile;
        }

        $result = $result . ']';

        return json_decode($result);
    }

    function lang_ar () {
             $resp = '{
                  "locale": "ar",
                  "txt_noEvents": "لا توجد مستجدات لهذه الفترة",
                  "txt_SpecificEvents_prev": "مستجدات ",
                  "txt_SpecificEvents_after": ":",
                  "txt_next": "التالي",
                  "txt_prev": "سابق",
                  "txt_NextEvents": "الأحداث القادمة:",
                  "txt_GoToEventUrl": "الذهاب إلى الحدث",
                  "txt_NumAbbrevTh": "",
                  "txt_NumAbbrevSt": "",
                  "txt_NumAbbrevNd": "",
                  "txt_NumAbbrevRd": "",
                  "txt_loading": " ",
                  "moment": {
                    "months" : [ "يناير", "فبراير", "مارس" , "أبريل" ,"مايو" ,"يونيو",
                        "يوليو" ,"أغسطس" ,"سبتمبر" ,"أكتوبر" ,"نوفمبر" ,"ديسمبر" ],
                    "weekdaysShort" : [ "الأحد","الإثنين","الثلاثاء","الأربعاء",
                        "الخميس","الجمعة","السبت" ],
                    "longDateFormat" : {
                      "LT" : "H:mm",
                      "LTS" : "LT:ss",
                      "L" : "DD/MM/YYYY",
                      "LL" : "D [de] MMMM [de] YYYY",
                      "LLL" : "D [de] MMMM [de] YYYY LT",
                      "LLLL" : "dddd, D [de] MMMM [de] YYYY LT"
                    },
                    "week" : {
                        "dow" : 1,
                      "doy" : 4
                    }
                  }
                }';

        return  json_decode($resp);
    }
    function lang_fr () {
        $resp = '{
                  "locale": "fr",
                  "txt_noEvents": "Il n\'y a pas d\'événements pour cette période",
                  "txt_SpecificEvents_prev": "Événements de ",
                  "txt_SpecificEvents_after": ":",
                  "txt_next": "suivant",
                  "txt_prev": "précédent",
                  "txt_NextEvents": "Prochains événements:",
                  "txt_GoToEventUrl": "Aller à l\'événement",
                  "txt_NumAbbrevTh": "",
                  "txt_NumAbbrevSt": "",
                  "txt_NumAbbrevNd": "",
                  "txt_NumAbbrevRd": "",
                  "txt_loading": " ",
                  "moment": {
                    "months" : [ "Janvier", "Février", "Mars" , "Avril" ,"Mai" ,"Juin",
                        "Juillet" ,"aout" ,"Septembre" ,"Octobre" ,"Novembre" ,"Décembre" ],
                    "weekdaysShort" : [ "Dim","Lun","Mar","Mer",
                        "Jue","Ven","Sam" ],
                    "longDateFormat" : {
                      "LT" : "H:mm",
                      "LTS" : "LT:ss",
                      "L" : "DD/MM/YYYY",
                      "LL" : "D [de] MMMM [de] YYYY",
                      "LLL" : "D [de] MMMM [de] YYYY LT",
                      "LLLL" : "dddd, D [de] MMMM [de] YYYY LT"
                    },
                    "week" : {
                        "dow" : 1,
                      "doy" : 4
                    }
                  }
                }';

        return  json_decode($resp);
    }
    function lang_en () {

     $resp = '{
                  "locale": "en",
                  "txt_noEvents": "There are no events for this period",
                  "txt_SpecificEvents_prev": "Events from ",
                  "txt_SpecificEvents_after": ":",
                  "txt_next": "Next",
                  "txt_prev": "Previous",
                  "txt_NextEvents": "Next Events:",
                  "txt_GoToEventUrl": "Go to the Event",
                  "txt_NumAbbrevTh": "",
                  "txt_NumAbbrevSt": "",
                  "txt_NumAbbrevNd": "",
                  "txt_NumAbbrevRd": "",
                  "txt_loading": " ",
                  "moment": {
                    "months" : [ "January", "February" ,"March" ,"April" ,"May" ,"June",
                         "July", "August", "September", "October", "November", "December" ],
                    "weekdaysShort" : [ "Sun","Mon","Tue","Wed",
                        "Thu","Fri","Sat" ],
                    "longDateFormat" : {
                      "LT" : "H:mm",
                      "LTS" : "LT:ss",
                      "L" : "DD/MM/YYYY",
                      "LL" : "D [de] MMMM [de] YYYY",
                      "LLL" : "D [de] MMMM [de] YYYY LT",
                      "LLLL" : "dddd, D [de] MMMM [de] YYYY LT"
                    },
                    "week" : {
                        "dow" : 1,
                      "doy" : 4
                    }
                  }
                }';

        return json_decode($resp);
    }

}

new Calendar_events();

