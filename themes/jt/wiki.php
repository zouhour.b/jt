<?php
/*
Template Name: wiki Page
*/
get_header();
the_post();
?>

<div class="container pt-5 px-5">
	<div class="pt-4"></div>
	<h1 class="title-ar-2 py-5"><?= __('تعريف المفاهيم','jt') ?></h1>
	<div class="row">
		<?= get_the_content() ?>
	</div>
</div>


<?php get_footer(); ?>