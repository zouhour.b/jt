<!doctype html>
<html dir="rtl">

<head>
    <meta charset="utf-8">
    <title><?= get_bloginfo('name') ?>  <?php if (!(is_front_page())) { echo " - ".get_the_title(); }   ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri() . '/favicon.ico'?>">
<?php 
if (empty(get_the_post_thumbnail_url())) {
$img_url = get_field('image');
} else {
$img_url = get_the_post_thumbnail_url();
}
?>
<?php if (is_single()) { ?>
    <meta property="fb:app_id"          content="479924239467772" />
    <meta property="og:type"            content="article" />
    <meta property="og:url"             content="<?= get_permalink() ?>" />
    <meta property="og:title"           content="<?php the_title(); ?>" />
    <meta property="og:image"           content="<?= $img_url ?>" />
    <!-- Twitter Sharer -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="La Roujou3">
    <meta name="twitter:title" content="<?php the_title(); ?>">
    <meta name="twitter:image" content="<?= $img_url ?>">
<?php } ?>
    
    <?php   wp_head(); ?>
    
</head>


<body style="text-align: right;" class="px-0">
    <?php do_action('body_start') ?>
    
    <div class="">
        <div style="
                position: sticky;
                z-index: 99;
                top: 20%;
            ">
        <!-- Button trigger modal -->
        <?php 
        $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
        if (!(strpos($escaped_url, 'archives-proces'))) { ?>
            <button type="button" class="btn btn-calendar" data-toggle="modal" data-target="#myModal">
            <img src="<?= get_template_directory_uri() . '/assets/icons/calendar-outline.svg' ?>" class="my-2"/>
            <?= __('الجلسات','jt') ?> <br>
            <?= __('القادمة','jt') ?>
            
            </button>
            <?php  } ?>
        
        </div>
<section class="text-left">
    <nav class="main-nav">
    <div style="color: white">
    </div>
        <div class="row mx-0">
            <div class="col-1 frLogo text-center">
            <?php $home = pll_current_language() == "ar"?get_site_url():get_site_url().'/fr/' ?>
                <a href="<?= $home ?>"> <img src="<?= get_template_directory_uri() . '/assets/icons/logo-ar.svg' ?>"  class="site-logo"/></a>
            </div>

            <img src="<?= get_template_directory_uri() . '/assets/icons/menu-icon.svg' ?>" class="mobile-only menu-icon"  onclick="toggleMenu()"/>
            <div class="mobile-menu" id="mobile-menu">
                <img src="<?= get_template_directory_uri() . '/assets/icons/menu-icon.svg' ?>" class="mobile-only menu-icon"  onclick="toggleMenu()" />
                <a href="<?= $home ?>" class="logoMobile"> <img src="<?= get_template_directory_uri() . '/assets/icons/logo-ar.svg' ?>" class="site-logo"></a>
                <ul class="searchFormMobile">
                <form class="form-inline justify-content-center pb-2" method="get" id="searchform" action="<?php bloginfo( 'url' ); ?>/">
                        <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" class="form-control w-50" placeholder="<?= __('كلمة مفتاحية','jt') ?>...">
                        <input type="hidden" name="site_section" value="site-search" />
                        <button type="submit" class="list-icons search-icon mx-2" id="searchsubmit" value="Search">
                        </button>                
                </form>
                </ul>
            <?php $mobile= array(
                    'theme_location'=> 'arabic mobile menu',

                    'menu_class'=> 'main-menu'

                );
                wp_nav_menu($mobile);
                $postUrl = 'http' . ( isset( $_SERVER['HTTPS'] ) ? 's' : '' ) . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                ?>
 
                <ul id="shareMobile">
                    <a href="https://www.facebook.com/laroujou3/" target="_blank"><li class="list-icons email-icon"></li></a>
                    <a href="https://www.facebook.com/laroujou3/" target="_blank"><li class="list-icons facebook-icon mx-2"></li></a>
                    <a href="https://twitter.com/LaRoujou3Tn" target="_blank">  <li class="list-icons twitter-icon"></li></a>
                </ul>
            </div>

            <div class="col desktop-only">
                <?php $arabic_menu= array(
                    'theme_location'=> 'arabic menu',

                    'menu_class'=> 'main-menu px-0'

                );
                 wp_nav_menu($arabic_menu);?>
            </div>
            <div class="col-2 px-0 desktop-only">
                <ul class="pt-4 px-4 d-inline-block">
                <a target="_blank" class="list-icons email-icon" href="mailto:?subject:Email to webmaster &body:Write your letter title="Envoyer un email"></a>
                <a href="https://www.facebook.com/laroujou3/" target="_blank">  <li class="list-icons facebook-icon"></li></a>
                <a href="https://twitter.com/LaRoujou3Tn" target="_blank">  <li class="list-icons twitter-icon"></li></a>
              

                </ul>
                <ul class="pt-4 d-inline-block px-0 mx-2 text-center">
                  <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><li class="list-icons search-icon"></li></a>
                </ul>
            </div>

                <ul class="lang-icon pt-3"><?php pll_the_languages(array( 'display_names_as' => 'slug',  'hide_current' => 1));?></ul>
        </div>
        <div class="collapse p-0" id="collapseExample">
                    <form class="form-inline justify-content-center pb-2" method="get" id="searchform" action="<?php bloginfo( 'url' ); ?>/">
                        <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" class="form-control w-50" placeholder="<?= __('كلمة مفتاحية','jt') ?>...">
                        <input type="hidden" name="site_section" value="site-search" />
                        <button type="submit" class="d-none form-control mr-lg-5 mb-2 i-search " id="searchsubmit" value="Search">
                        </button>
                    </form>
    </div>
    </nav>
</section>

  
        <!-- Modal -->
        <div class="modal" id="myModal" style="">
            <div class="modal-dialog" style="max-width: 80%;">
            <div class="modal-content" style="border-radius: 0.8rem;">
            
                <!-- Modal Header -->
                <div class="modal-header" style="border-bottom: none">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                <?php 
                 if ( pll_current_language() == "ar") {
                    $dir = 'rtl';
                }
                else $dir = 'ltr';
                $calendar = new PostTypeCalendar([
                'baseurl'=>get_the_permalink( get_the_ID() ),
                'post_type'=>'session', 'dir'=>$dir]); ?>
                    <?= $calendar->renderMonthFromRequest($_GET) ?>
                <div id="eventCalendarHumanDate" class="row"></div>
                </div>
                
                
                
            </div>
            </div>
        </div>
