<?php
/*Template Name: documents*/
get_header();
$category = get_the_category();
if ( pll_current_language() == "ar") {
    $link = $category[0]->slug;
} else $link = substr($category[0]->slug, 0, -3) ;
?>


<?php while (have_posts()):the_post() ?>            
    <section data-single=true class="single">
        <div class="page-header-doc" style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/bg-3.png' ?>');" >
        <div class="black-layer-2 ">
            <div class="container page-header-content">

                <h2 class="title-ar-1 white mb-4 text-center w-100" style="background: none;"><?= the_title() ?></h2>
                <div class="px-5 pt-3">
                <div class="bigger px-4 doc-subtitle mt-5">

                <span><img src="<?= get_template_directory_uri() . '/assets/icons/'.$link?>'-white.svg'" class="proces-icon mx-1">
                   <?= $category[0]->name ?><br>
                    <img src="<?= get_template_directory_uri() . '/assets/icons/clock-white.svg'?>'" class="proces-icon mx-1">
                    <?= get_the_date('d F Y') ?>                    
                 </span>
                </div>
                </div>
            </div>
             <div class="share-btn my-4 float-right">
                    <span><?= __('مشاركة','jt') ?></span><br>
                    <div style="border-top: 1px solid white;" class="mt-2 pt-3 px-1"><?= get_template_part('template', 'sharing-box'); ?></div>
            </div>
        </div>
        </div>
    </section>

    <section>
        <div class="container mb-4">
            <div class="py-5">

                <?php the_content() ?>
                <div class="d-none">
                <a class="prev"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
                                          class="slide-video-icon"/></a>
                <a class="next"><img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>"
                                          class="slide-video-icon"/></a>
                </div>
            </div>
        </div>

    </section>
    <section id="vids"> 
    </section>
<?php endwhile ?>
<style>
    .wp-block-file a{
        text-align:left;
        margin-top: 1rem;
        margin-bottom: 1rem;
        color: white;
        float: left;
        clear: both;
        margin-bottom: 1rem;
        background: #be0a26 url("<?= get_template_directory_uri() . '/assets/icons/002-pdfWhite.svg' ?>") no-repeat right;
        padding: .5rem 2.5rem .5rem 1rem;
        background-size: 1.5rem;
        background-position: 93%;

    }
    .wp-block-file a:hover{
        color: white;
    }
    .wp-block-file__button{
        display: none;
    }

    h2{
        background: black;
        color: white;
        float:right;
        padding: 0.5rem 1rem;
        clear:both;
        margin-bottom: 3rem;
    }
    p{
        clear: both;
    }
    .wp-block-embed-youtube {
    width: 100vw;
    background-color: black;
    position: relative;
    }
    

</style>


<?php
get_footer();
