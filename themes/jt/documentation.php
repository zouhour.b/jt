<?php
/*
Template Name: documentation template
*/
get_header();
$url = get_post_permalink();
$number = 10;
$years = new WP_Query( array(
    'post_type' => 'documents',
));
$year = array();
while ($years->have_posts()): $years->the_post();
$d = get_the_date( 'Y');
    if(!in_array($d, $year)) {
        array_push($year, $d);
    }
endwhile;
?>

<section>
    <div class="page-header-doc"  style="background-image: url('<?= get_template_directory_uri() . '/assets/backgrounds/bg-3.png' ?>');">
        <div class="black-layer-2 " >
            <div class="container page-header-content">
                <h1 class="title-ar-1 white mb-4 text-center"><?= __('أرشيف','jt') ?></h1>
                <div class="px-5 pt-3 ">
                    <p><?= __('هو مشروع ثلاثي يضم كل من منظمة البوصلة، محامون بلا حدود، والمنتدى التونسي للحقوق الاقتصادية
                        والاجتماعية يقوم على متابعة مسار العدالة الإنتقالية ودعم جهود الفاعلين فيه بغاية القطع مع أنّات
                        الماضي وضمان عدم تكرار الانتهاكات الجسيمة لحقوق الإنسان والجرائم الاقتصادية التي وقعت في عهد
                    الدكتاتورية في تونس','jt') ?></p>
                    <p><?= __('وضعت المنظمات الرائدة لهذا المشروع نهجا شاملا يستند إلى ملاحظاتها المشتركة فيما يتعلق بمسار
                    العدالة الانتقالية في تونس ورسمت ثلاثة أهداف مترابطة مفصليا على النحو التالي :','jt') ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="form-container py-2">
        <!--<div class="bg-warning p-5 d-flex align-items-center  ">
            icon('lawProject', 'icon-sm icon-red') 
        </div>-->
       
        <div class="container doc-form">

<?php $categories = get_categories();
$catlist = '';
foreach ($categories as $cat) {
$catlist.= $cat->cat_ID.',';
}
$catlist.'4';
?>
            <form class="form-inline   justify-content-center" method="get" id="searchform" >
                <input type="hidden" name="current-lang" id="current-lang" value="<?= pll_current_language() ?>" />
                <input type="text" name="keyword" id="keyword" class="form-control mb-2 mr-lg-3 i-pwd" placeholder="<?= __('كلمة مفتاحية','jt') ?>" onkeyup="fetchDocs()">
                <input type="hidden" name="site_section" value="documents" />
                <select name="cat" id="cat" class="form-control mr-lg-3 mb-2 i-code" onchange="fetchDocs()";>
                <option value=" " selected="selected"><?= __('فئة الوثيقة','jt') ?></option>
                <?php 
                foreach ($categories as $category) { 
                    if (($category->term_id != "409") && ($category->term_id != "407") && ($category->term_id != "191") && ($category->term_id != "217")){
                 ?>
                    <option value="<?= $category->term_id ?> "><?= $category->name ?></option>
                <?php }  }?>
                </select>
                <select name="format" id="format" class="form-control mr-lg-3 mb-2 i-code" onchange="fetchDocs()";>
                <option value=" " selected="selected"><?= __('نوع الوثيقة','jt') ?></option>
                <?php 
                   if (pll_current_language() == "ar"){
                        $val1 = 191;
                        $val2 = 407;
                    } else{
                        $val1 = 217;
                        $val2 = 409;
                    }
                 ?>
                    <option value="<?= $val1 ?>"><?= __('فيديو','jt') ?></option>
                    <option value="<?= $val2 ?>"><?= __('ملفات','jt') ?></option>
                <?php  ?>
                
                </select>
                <select name="year" id="year" class="form-control mr-lg-3 mb-2 i-year" onchange="fetchDocs()">
                    <option value="AW" selected="selected"><?= __('السنة','jt') ?></option>
                    <?php for ($i = 0; $i < count($year); $i++) { ?>
                    <option value="<?= $year[$i] ?> "><?= $year[$i] ?></option>
                    <?php } ?>
                </select>
                

                <button class="form-control mr-lg-5 mb-2 i-search " type="submit" id="searchsubmit" value="Search"><?= __('بحث','jt') ?> </button>

            </form>
        </div>
    </div>
</section>

<section>
    <div class="page-of-title">
        <div class="container t-container table-content text-left">
<?php
                    $page = get_query_var('page', 1);
                    if($_GET){
                        $wp_query = new WP_Query(
                            array(
                                'post_type' => 'documents',
                                'posts_per_page' => $number+1,
                                'paged' => $page,  
                                's' => $_GET['keyword'],
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'category',
                                        'field' => 'id',
                                        'terms' => array( esc_attr($_GET['cat']), esc_attr($_GET['format']) ),
                                        'operator' => 'AND'
                                    )
                                ),
                                //'cat' => $_GET['cat'],
                                'date_query' => array(
                                    'year' =>  intval($_GET['year']),
                                )
                            ));}
                            else {
                                $wp_query = new WP_Query(
                                    array(
                                        'post_type' => 'documents',
                                        'posts_per_page' => $number+1,
                                        'paged' => $page,
                                        
                                    ));
                            }
                    ?>

            <table class="table table-condensed" id="liveSearch">
            <?php if ($wp_query->have_posts() == true){
                    ?>
                <thead>
                    <tr>
                        <th class="text-center"><?= __('العناوين','jt') ?></th>
                        <th></th>
                        <th class="text-center"><?= __('السنة','jt') ?></th>
                    </tr>
                </thead>


                <tbody id="infinite-scroll">
                    
                        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                        
                            <?php /*$category = empty(wp_get_post_terms( get_the_ID(), 'doc-category', array( 'fields' => 'slugs' ) ))?$category = array (0 => 'category1'):wp_get_post_terms( get_the_ID(), 'doc-category', array( 'fields' => 'slugs' ) );*/
                            //$category = get_the_category();
                            $cat = wp_get_post_categories($post->ID);
                            $a = get_category($cat[0]);
                            $b = count($cat) < 2 ?get_category($cat[0]):get_category($cat[1]);
                            $link = null;

                            if ( (pll_current_language() == "ar") && (($a->slug == "category5") || ($a->slug == "category7"))) {
                                $link = $b->slug ;
                            } else if ((pll_current_language() == "fr") && (($a->slug == "category5-fr") || ($a->slug == "category7-fr"))) {
                                $link = $b->slug == null?substr($a->slug, 0, -3):substr($b->slug, 0, -3) ;
                            } else {
                                if (pll_current_language() == "ar") {
                                    $link = $a->slug;
                                } else {
                                    $link = substr($a->slug, 0, -3) ;
                                }
                            }
                            ?>
                            <tr>
                                <td class="i-title"><a href="<?= the_permalink() ?>"><img src=" <?= get_template_directory_uri() . '/assets/icons/'. $link ?>.svg"/></a></td>
                                <td class="right"><a href="<?= the_permalink() ?>"><?=the_title()?>   </a></td>
                                <td class="center"><a href="<?= the_permalink() ?>"><?= get_the_date( 'd.m.Y') ?></a></td>
                            </tr>
                         


                        <?php endwhile; ?>

                <!--  <tr>
                        <td  class="text-center i-title"><img src="../wp-content/themes/jt/assets/icons/009-legal-paper.svg"></td>
                        <td class="text-right">المشتركة فيما يتعلق بمسار العدالة الانتقالية في تونس ورسمت ثلاثة أهداف مترابطة</td>
                        <td class="text-center">18.06.2019</td>
                    </tr>-->
                </tbody>
                <?php 
                }  else { ?>
                   <h3 class='my-4 text-center'> <?= __('ليس هناك وثائق','jt') ?> </h3> 
               <?php } ?>
            </table>
            <?php if ($wp_query->max_num_pages -1 != get_query_var('paged') and ($wp_query->have_posts() == true)) { ?>
                <button class="default-btn --js-load-more" data-per-page="2" data-target="#infinite-scroll"
                data-page="<?= $page ?>" data-url="<?= $url ?>">
                <i class="d-none loader fa fa-spin fa-spinner"></i>
                <?= __('رؤية المزيد','jt') ?>
            </button>
                    <?php  }?>

    </div>
</div>

</section>
<style>
    .icon-sm{width: 2rem;height:2rem;}
    .icon-red use {fill: red;}
</style>

<?php
get_footer();
?>


