<section>
    <footer>
        <div class="container">
            <a href="<?= get_site_url() ?>"><img src="<?= get_template_directory_uri() . '/assets/icons/logo-ar.svg' ?>"  class="site-logo"/></a>
            <div class="row mr-5 mt-5">
                <div class="col-4 footer-list-group">
                    <p class="list-title"><?= __('الروابط','jt') ?></p>
                    <?php $footer_links= array(
                        'theme_location'=> 'footer arabic links menu',
                    );
                    wp_nav_menu($footer_links);?>
                </div>
                <div class="col-4 footer-list-group">
                    <p class="list-title"><?= __('شركاؤنا','jt') ?></p>
                    <?php $footer_partners= array(
                        'theme_location'=> 'footer arabic partners menu',
                    );
                    wp_nav_menu($footer_partners);?>
                </div>
                <div class="col-4 footer-list-group">
                    <p class="list-title"><?= __('اتصل بنا','jt') ?></p>
                    <ul class="contact-us">
                        <li class="phone"> <a href="tel:+21671840424" >216-71-840-424</a></li>
                        <li class="phone"> <a href="tel:+21671894002" >216-71-894-002</a></li>
                        <li class="phone"> <a href="tel:+21671257664" >216-71-257-664</a></li>
                        <li class="email"><a href="mailto:contact@company.com"> contact@laroujou3.com</a></li>
                    </ul>
                </div>

                <!--<div class="col footer-list-group ">
                    <p class="list-title">أخبارنا</p>
                    <p class="news ">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي</p>
                    <input type="email" placeholder="بريدك الالكتروني" class="newsletter-input" />
                </div>
                    -->
            </div>
            <hr>
            <div class="pb-5 row">
            <div class="col-6">
                <p class="poweredby" dir="rtl"> <img src="<?= get_template_directory_uri() . '/assets/icons/inkylab.svg"' ?>" /> Powered by </p>
            </div>
            <div class="col-6">
                <p class="backtotop"><a href="#top" style="font-size: 2rem"><?= __('الرجوع للأعلى','jt') ?> <img src="<?= get_template_directory_uri() . '/assets/icons/down-arrow.svg' ?>" /></a></p>
            </div>
                
            </div>
        </div>
    </footer>
</section>
 <?php wp_footer(); ?>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150654593-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-150654593-1');
</script>

</div>
</body>
</html>