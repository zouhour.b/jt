<?php
function script_enqueue()
{
    wp_enqueue_style('MainStyle', get_template_directory_uri() . '/css/main-rtl.css', array(), '1.0.0', 'all');
    wp_enqueue_script('jQuery', get_template_directory_uri() . '/js/jquery-3.3.1.min.js', array(), '3.3.1', false);
    
    wp_enqueue_script('loadmore', get_template_directory_uri() . '/js/loadMorePosts.js', array(), '1.0.0', false);
    wp_localize_script( 'loadmore', 'base_url', get_site_url());
    wp_enqueue_script('btnmore', get_template_directory_uri() . '/js/btnMore.js', array(), '1.0.0', false);


    wp_enqueue_script('amchartsCore', get_template_directory_uri() . '/js/core.js', array(), '1.0.0',false);
    //wp_enqueue_script('amchartmaps', get_template_directory_uri() . '/js/maps.js', array(), '1.0.0',false);
    //wp_enqueue_script('tunisiaMap', get_template_directory_uri() . '/js/tunisiaHigh.js', array(), '1.0.0',false);
    wp_enqueue_script('language', get_template_directory_uri() . '/js/lang_AR.js', array(), '1.0.0',false);
    
    wp_enqueue_script('slick-carousel', get_template_directory_uri() . '/js/slick-carousel.min.js', array(), '1.0.0',false);
    wp_enqueue_script('carousel', get_template_directory_uri() . '/js/carousel.js', array(), '1.0.0',false);

    if ( is_home() ) {
     wp_enqueue_script('map', get_template_directory_uri() . '/js/map.js', array(), '1.0.0',false);
     wp_localize_script( 'map', 'base_url', get_site_url());
     }

 wp_enqueue_script('bootstrapJS', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0',false);
 wp_enqueue_script('themomentlib', get_template_directory_uri() . '/js/moment.min.js', array(), '1.0.0',false);

 wp_enqueue_script('playscript', get_template_directory_uri() . '/js/scripts.js', array(), '1.0.0',true);
 wp_localize_script( 'playscript', 'base_url', get_site_url());
 $vars = array(
    'baseUrl'  => get_site_url(),  
) ;

 wp_localize_script( 'calendar', 'php_vars', $vars);

 wp_enqueue_style('fa', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

 wp_enqueue_script('postsApiCustom', get_template_directory_uri() . '/js/searchProces.js', array(), '1.0.0',true);
}

add_action('wp_enqueue_scripts', 'script_enqueue');

add_action('admin_enqueue_scripts', function(){
    wp_enqueue_script('admin-js', get_template_directory_uri().'/js/admin.js', ['jquery', 'wp-media']);
});

include_once ("inc/custom-types/Documents.php");
include_once ("inc/custom-types/Videos.php");
include_once ("inc/custom-types/Proces.php");
include_once ("inc/custom-types/Session.php");

/* Custom Post Type End */
add_theme_support( 'post-thumbnails' ); 

function wpc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'wpc_mime_types');

remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );

register_nav_menus (array(
    'arabic menu'=>('Arabic Menu'),
    'arabic mobile menu'=>('Arabic Mobile Menu'),
    'footer arabic links menu'=>('Footer  Arabic Links Menu'),
    'footer arabic partners menu'=>('Footer  Arabic Partners Menu'),
    )
);

add_filter('cazlendar_item_date', function($date, $post){
    $args['post_type'] = 'newposttype';
    return $args;
},2,2);



add_action('admin_enqueue_scripts', function(){
    wp_enqueue_script('accused-ui', get_template_directory_uri() . '/js/accused-ui.js', array('jquery', 'wp-api'));
    wp_localize_script( 'accused-ui', 'api_url', get_site_url());
    wp_enqueue_script('fields-proces', get_template_directory_uri() . '/js/fields_proces.js', array('jquery', 'wp-api'));
    wp_localize_script( 'fields-proces', 'api_url', get_site_url());
});


function icon($icon, $class='svg-icon'){
    $url = get_template_directory_uri().'/assets/icons.svg';
    return sprintf('<svg class="%s" viewBox="0 0 320 320" xml:space="preserve">
    <use xlink:href="%s#%s" ></use>
    </svg>', $class, $url, $icon);
}


add_action('wp_ajax_data_fetch' , 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch','data_fetch');
add_action('wp_ajax_proces_fetch' , 'proces_fetch');
add_action('wp_ajax_nopriv_proces_fetch','proces_fetch');

include_once('inc/postsapi.php');

include_once('inc/shortcodes.php');


include_once('inc/Api.php');


