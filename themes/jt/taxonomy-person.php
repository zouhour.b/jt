<?php
/*
Template Name: Person
*/
get_header();
?>



<section>
<?php 

 $index_query = new WP_Query( array(
    'post_type' => 'proces',
    'posts_per_page'   => -1,
    'meta_key' => 'category',              
    'meta_value' => 'Highlighted',
));
$taxonomy_ID = get_queried_object()->term_id;
        $proces_sum = 0;
        while ($index_query->have_posts()): $index_query->the_post();
        $accused = get_field('accused_data', get_the_ID());
        $accused = $accused?json_decode($accused, true):[];
        if ($accused) {
            $p='';
            foreach( $accused as $data ):
                $person = get_term_by('id', $data['person'], 'person');
                if (!$person) continue;
                $p .= strval($person->term_id) .',';
            endforeach;
            if(strpos($p, strval($taxonomy_ID)) !== false) { 
           
                $proces_sum++;
        $accusation = get_the_terms(get_the_ID(),'accusation');
        if ($accusation) {
            $a='';
            foreach( $accusation as $data ):
                if(strpos($a, strval($data->name)) === false) { 
                    $a .= strval($data->name) .' - ';          
                }
            endforeach;
            }
                    
        }}
        
        endwhile;
?>
    <div class="container" style="padding-top: 6.5rem">
        <div style="background: url(<?= get_template_directory_uri() . '/assets/backgrounds/salesla.svg' ?>) no-repeat top center; margin: 0 17%">
            <h1 class="tax-title mt-5 " >
            <svg width="35" height="5" style="position: absolute; bottom: 109%;">
                <rect width="35" height="5" style="fill:#be0a26" />
            </svg><?= get_queried_object()->name ?>
            </h1>
        
            <p class="tax-desc"><?= empty(get_queried_object()->description)?null: get_queried_object()->description . '<br/>' ?>
            </p>
            <h4 class= " pink-bold d-inline-block mb-0"><?= __('التهم الموجهة','jt'); ?> : </h4>
            <h5 class="d-inline-block mb-0"><?= substr($a, 0, -2) ?> </h5>
            <h5 class=" mb-4"><img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>" class="tax-icon mx-1 mt-2"/>
            <?= __('عدد القضايا','jt'); ?>: <?= $proces_sum ?>
            </h5>
        </div>
    <hr class="seperator">
    <button class="default-btn black py-1 px-2" style="background-image: unset;"><?= __('القضايا المذكور فيها','jt'); ?> </button>
    <?php 
        $args = array(
            'numberposts' => -1,
            'post_type'   => 'session'
        );
        $all_sessions = get_posts( $args );
        $post_index = 0;
         while ($index_query->have_posts()): $index_query->the_post();
        
         $accused = get_field('accused_data', get_the_ID());
        $accused = $accused?json_decode($accused, true):[];
        if ($accused) {
            $p='';
            foreach( $accused as $data ):
                $person = get_term_by('id', $data['person'], 'person');
                if (!$person) continue;
                $p .= strval($person->term_id) .',';
            endforeach;
            if((!empty($person->term_id)) and (strpos($p, strval($taxonomy_ID)) !== false)) { 
           
                $nSessions = 0;
                foreach ($all_sessions as $session) {
                    $proces_Attached = get_field("proces", $session->ID);
                    if ( $proces_Attached->ID == get_the_ID() ){
                        $nSessions += 1;
                    }
                    
                }
              
    ?>
            <div class="row desktop-display my-5 py-5">
                <?php 
                $post_index += 1;
                        if($post_index % 2 == 0){ 
                ?>
                            <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                            <?php if (get_the_post_thumbnail_url() == false) { ?>
                                <a href="<?= the_permalink() ?>"><img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                                </a>
                                <?php } else { ?>
                                
                                <a href="<?= the_permalink() ?>"><div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"> </div>
                                </a><?php } ?>
                            </div>
                            <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                                <div class="card-body">
                                    <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                                
                                    <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                    <div>
                                        <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                            class="proces-icon mx-1"/>
                                        <?php
                                        $terms = get_the_terms($post,'location');

                                        $state = get_field('state');
                                        $val= empty( $state['value'])?null:$state['value'];
                                        $label = $state['label'];

                                            if(!empty($terms[0]->name)){
                                        ?>
                                        <h5 class="card-text mb-0 d-inline-block" id="location"><?= $terms[0]->name.' , '.$label; ?></h5>
                                        <?php
                                        } else { 
                                        ?>
                                        <h5 class="card-text mb-0 d-inline-block" id="location"><?= __('لم يحدد بعد','jt'); ?></h5>
                                        <?php
                                        } 
                                        ?>
                                    </div>
                                    <div>
                                        <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                            class="proces-icon mx-1"/>
                                        
                                        <h5 class="card-text d-inline-block" id="num"><?= __('عدد الجلسات','jt'); ?> : <?= $nSessions ?> </h5>
                                    </div>
                                    <a href="<?= the_permalink() ?>">
                                            <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt'); ?></button>
                                    </a>
                                </div>
                            </div> 
                <?php 
                        } 
                        else { 
                ?>      <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                                <div class="card-body">
                                    <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                                
                                    <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                    <div>
                                        <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                            class="proces-icon mx-1"/>
                                        <?php
                                        $terms = get_the_terms($post,'location');
                                        
                                        $state = get_field('state');
                                        $val= empty( $state['value'])?null:$state['value'];
                                        $label = $state['label'];

                                        if(!empty($terms[0]->name)){
                                        ?>
                                        <h5 class="card-text mb-0 d-inline-block" id="location"><?= $terms[0]->name.' , '.$label; ?></h5>
                                        <?php
                                        } else {
                                        ?>
                                        <h5 class="card-text mb-0 d-inline-block" id="location"><?= __('لم يحدد بعد','jt'); ?></h5>
                                        <?php
                                        } 
                                        ?>
                                    </div>
                                    <div>
                                        <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                            class="proces-icon mx-1"/>
                                        
                                        <h5 class="card-text d-inline-block" id="num"><?= __('عدد الجلسات','jt'); ?> : <?= $nSessions ?> </h5>
                                    </div>
                                    <a href="<?= the_permalink() ?>">
                                            <button class="default-btn black float-left py-1 mt-3"><?= __('رؤية المزيد','jt'); ?></button>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                                <?php if (get_the_post_thumbnail_url() == false) { ?>
                                <a href="<?= the_permalink() ?>"><img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                                </a>
                                <?php } else { ?>
                                <a href="<?= the_permalink() ?>">
                                <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"> </div>                                </a><?php } ?>
                            </div> 
                <?php 
                        } 
                    
                ?>
            

                
            </div>
            <div class="row py-4 mobile-display">
                    <div class="card d-block col-md-6" style="background-color: unset; border: none;">
                        <?php if (get_the_post_thumbnail_url() == false) { ?>
                            <a href="<?= the_permalink() ?>">
                            <img class="card-img-top" src="https://picsum.photos/300/200" alt="Card image cap">
                            </a>
                            <?php } else { ?>
                            <a href="<?= the_permalink() ?>">
                            <div class="card-img-top" style="background-image:url('<?= get_the_post_thumbnail_url() ?>')" alt="Card image cap"></div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="col-md-6 d-block card" style="background-color: unset; border: none;">
                            <div class="card-body">
                                <h1 class="card-title" id="the_title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h1>
                            
                                <p class="card-text" id="the_excerpt"><?= wp_trim_words( get_the_content(), 50, ".." ) ?></p>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/place.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    <?php
                                    $terms = get_the_terms($post,'location');

                                    $state = get_field('state');
                                    $val= empty( $state['value'])?null:$state['value'];
                                    $label = $state['label'];

                                        if(!empty($terms[0]->name)){
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= $terms[0]->name.' ب'.$label; ?></h5>
                                    <?php
                                    } else { 
                                    ?>
                                    <h5 class="d-inline-block" id="location"><?= __('لم يحدد بعد','jt'); ?></h5>
                                    <?php
                                    } 
                                    ?>
                                </div>
                                <div>
                                    <img src="<?= get_template_directory_uri() . '/assets/icons/law.svg' ?>"
                                        class="proces-icon mx-1"/>
                                    
                                    <h5 class="d-inline-block" id="num"><?= __('عدد الجلسات','jt'); ?> : <?= $nSessions ?> </h5>
                                </div>
                                <a href="<?= the_permalink() ?>">
                                        <button class="default-btn black float-left py-1 mb-5"><?= __('رؤية المزيد','jt'); ?></button>
                                </a>
                            </div>
                        </div> 
           
            </div>
    <?php  }
     }
    endwhile; 
    ?>
       
    </div>
</section>


<?php get_footer(); ?>

