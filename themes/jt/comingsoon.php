<?php
/*
Template Name: Coming Soon Page
*/
get_header();
?>

<section>
    <div class="coming-soon">
        <div class="container">
            <h1 >قريبا</h1>
            <img src="<?= get_template_directory_uri() . '/assets/icons/comingsoon.svg' ?>" />
        </div>
    </div>
</section>

<?php get_footer(); ?>