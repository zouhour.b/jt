<?php

function data_fetch(){

    $the_query = new WP_Query( array( 'posts_per_page' => -1, 's' => esc_attr( $_POST['keyword'] ) , 'post_type' => 'documents', 'cat' => esc_attr($_POST['cat']),'date_query' => array('year' =>  intval($_POST['year'])) ) );
    if( $the_query->have_posts() ) { ?>
                <thead>
                    <tr>
                        <th class="text-center"><?= __('العناوين','jt') ?></th>
                        <th></th>
                        <th class="text-center"><?= __('السنة','jt') ?></th>
                    </tr>
                </thead>
        <?php
        while( $the_query->have_posts() ): $the_query->the_post(); 
        $category = get_the_category();
        if ( pll_current_language() == "ar") {
            $link = $category[0]->slug;
        } else $link = substr($category[0]->slug, 0, -3) ;
        ?>
            <tr>
                <td class="text-center i-title"><a href="<?= esc_url( post_permalink()) ?>"><img src=" <?= get_template_directory_uri() . '/assets/icons/'. $link ?>.svg"/></a></td>
                <td class="right"><a href="<?= esc_url( post_permalink()) ?>"><?=the_title()?>   </a></td>
                <td class="text-center"><a href="<?= esc_url( post_permalink()) ?>"><?= get_the_date( 'd.m.Y') ?></a></td>
            </tr>

        <?php endwhile;
        wp_reset_postdata();  
    } else {
         ?>
            <h3 class='my-4 text-center'> <?= __('ليس هناك وثائق','jt') ?> </h3> 
        <?php 
    }
    return true;


}


function proces_fetch(){

    $the_query = new WP_Query( array
        ( 
            'posts_per_page' => -1,
            'post_type' => 'proces',
            
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key' => 'state',
                    'value' => $_GET['court'],
                    'compare' => '='
                )
            ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'location',
                    'field' => 'name',
                    'terms' => $_GET['tpi']
                    ),
                array(
                    'taxonomy' => 'accusation',
                    'field' => 'name',
                    'terms' => $_GET['accusation']
                    )
                )

            ));
       
        
    if( $the_query->have_posts() ) { ?>
        <?php
        	$path = set_include_path('../proces_archives_template.php');
            include ($path); 
            ?>
        
        <?php 
        wp_reset_postdata(); 
    } else {
         ?>
            <h3 class='my-4 text-center'> <?= __('ليس هناك وثائق','jt') ?> </h3> 
        <?php 
    }

  return true;
} 