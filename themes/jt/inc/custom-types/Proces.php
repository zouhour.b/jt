<?php


class Proces {
    public function __construct() {

        add_action('init', [$this, 'register']);
        add_filter( 'enter_title_here', array( $this, 'my_title_place_holder' ), 2, 10 );
        add_action( 'init', array( $this,'register_taxonomy' ));
        add_action('save_post', [$this, 'save_proces'],0,2);
        add_action('add_meta_boxes', [$this, 'meta_boxes']);
    }

    public function save_proces($post_id, $post){
        if ($post->post_type !== 'proces') return;
        if (!isset($_POST['acf'])) return;
        $fields = array_keys($_POST['acf']);
        $persons = [];
        foreach ($_POST['acf'] as $key=>$value){

            $field = get_posts(['post_type'=>'acf-field', 'name'=>$key]);
            if (count($field) == 1 && $field[0]->post_excerpt=='accused_data'){
                $value = stripslashes($value);
                $value = $value?json_decode($value, true):[];
                foreach ($value as $v){
                    $persons[] = (int)$v['person'];
                }
            }
            
            if ($field && count($field) == 1 && $field[0]->post_excerpt=='in_charge_of_personal_right'){
                foreach ($value as $v){
                    $persons[] = (int)$v;
                }


            }
        }
        wp_set_object_terms($post->ID, $persons, 'person', true);
        return;
    }

    public function register() {
        $post_type = 'proces';

        $labels = array(
            'name'           => $post_type,
            'singular_name'  => $post_type,
            'menu_name'      => 'Proces',
            'name_admin_bar' => $post_type,
            'add_new_item'   => $post_type,
            'edit_item'      => $post_type,
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'supports'           => array( 'title','editor','thumbnail' ),
            'show_in_admin_bar' =>  true,


        );

        if ( ! in_array( $post_type, get_post_types() ) ) {
            register_post_type( $post_type, $args );

        }

    }
    function register_taxonomy() {
     register_taxonomy( 'person', ['proces'], array(
        'hierarchical' => false,
        'show_in_rest'    =>true,
            'labels'=>[
            'name'=>'person', 
            'singular_name'=>'Person',
            'menu_name'=>'Person'
        ]

    ) );
     register_taxonomy( 'location', ['proces'], array(
        'hierarchical' => false,
        'show_in_rest'    =>true,
        'labels'=>[
            'name'=>'location', 
            'singular_name'=>'Location',
            'menu_name'=>'Location'
        ]

    ) );
     register_taxonomy( 'accusation', ['proces'], array(
        'hierarchical' => false,
        'show_in_rest'    =>true,
        'labels'=>[
            'name'=>'accusation', 
            'singular_name'=>'Accusation',
            'menu_name'=>'Accusation'
        ]

    ) );

        // Add new taxonomy, NOT hierarchical (like tags)




 } 
 function my_title_place_holder( $title, $post ) {
    $post_type = 'proces';


    if ( $post->post_type == $post_type ) {
        $my_title = "Nom du Proces";

        return $my_title;
    }

    return $title;

}
public function meta_boxes(){
   /* add_meta_box(

        'wpt_field_location',
        'location',
         array($this, 'render_meta_field'),
         'proces',
         'normal',
         'default'

    );
     add_meta_box(

        'wpt_field_accusation',
        'accusation',
         array($this, 'render_meta_field_accusation'),
         'proces',
         'normal',
         'default'

    );*/

}

public function render_meta_field ($post){
    $locations = get_terms( array(
    'taxonomy' => 'location',
    'hide_empty' => false,
    ));
      $locationid =  get_the_terms($post, 'location')[0]->term_id;


    ?>
<ul class="acf-checkbox-list acf-bl">

    <?php foreach ($locations as $term)  { 
     ?>

    <li data-id="<?= $term->ID ?>"> 
        <label> 
     
            <input type="radio" name="location-radio"  value="<?= $term->name ?>" <?= ($locationid == intval($term->term_id) ) ? 'checked' : '' ?> > 
            <span><?= $term->name ?></span>
        </label>
    </li>


  <?php
   } 

}

public function render_meta_field_accusation ($post){
    $accusations = get_terms( array(
    'taxonomy' => 'accusation',
    'hide_empty' => false,
    ));
      $accu =  !empty(get_the_terms($post, 'accusation'))?get_the_terms($post, 'accusation'): []; 
    ?>
   
    <ul class="acf-checkbox-list acf-bl">
          <?php foreach ($accusations as $term)  { 
           // var_dump(array_search($term->term_id,  array_column($accu, 'term_id') ) );
     ?>

    <li data-id="<?= $term->ID ?>">
        <label>
            <input type="checkbox" name="accusation-checkbox" value="<?= $term->name ?>" <?= (array_search($term->term_id,  array_column($accu, 'term_id') ) != false ) ? 'checked' : '' ?>> <?= $term->name ?>
        </label>
    </li>
    
<?php
   } ?>

 
    <script>
         $ = jQuery
         function recursiveClick(){
            if (('#tagsdiv-accusation .tagchecklist li button.ntdelbutton').length != 0) {
                 $(this).click();
                 $(this).click(recursiveClick);

            }
         }
        $("input[name='location-radio']").on('change',function () {
            $('#tagsdiv-location .tagchecklist button.ntdelbutton').click()
            $('#tagsdiv-location #new-tag-location').val($(this).val())
            $('#tagsdiv-location .tagadd').click()
        });
        $("input[name='accusation-checkbox']").on('change',function () {
            $('#tagsdiv-accusation #new-tag-accusation').val($(this).val())
            $('#tagsdiv-accusation .tagadd').click()
            
        })
        $('input#publish').on('click', function(){

            console.log($('#tagsdiv-accusation .tagchecklist button.ntdelbutton').length)
            //$('#tagsdiv-accusation .tagchecklist li').remove();
            //$('#tagsdiv-accusation .tagchecklist button.ntdelbutton').click()
        })
    </script>       
    <?php

}

}

new Proces();