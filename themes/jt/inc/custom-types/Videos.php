<?php


class Videos {
    public function __construct() {

        $this->register();
        add_filter( 'enter_title_here', array( $this, 'my_title_place_holder' ), 2, 10 );
        /* add_action( 'init', array( $this,'register_taxonomy' )); */

    }

    public function register() {
        $post_type = 'videos';

        $labels = array(
            'name'           => $post_type,
            'singular_name'  => $post_type,
            'menu_name'      => 'videos',
            'name_admin_bar' => $post_type,
            'add_new_item'   => $post_type,
            'edit_item'      => $post_type,
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'supports'           => array( 'title','editor','thumbnail' ),
            'show_in_admin_bar' =>  true,


        );

        if ( ! in_array( $post_type, get_post_types() ) ) {
            register_post_type( $post_type, $args );

        }

    }

    function my_title_place_holder( $title, $post ) {
        $post_type = 'videos';


        if ( $post->post_type == $post_type ) {
            $my_title = "Nom de la video";

            return $my_title;
        }

        return $title;

    }
}

new Videos();