<?php


class Documents {
    public function __construct() {

        $this->register();
        add_filter( 'enter_title_here', array( $this, 'my_title_place_holder' ), 2, 10 );
        add_action( 'init', array( $this,'register_taxonomy' )); 

    }

    public function register() {
        $post_type = 'documents';

        $labels = array(
            'name'           => $post_type,
            'singular_name'  => $post_type,
            'menu_name'      => 'Documents',
            'name_admin_bar' => $post_type,
            'add_new_item'   => $post_type,
            'edit_item'      => $post_type,
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'show_in_rest'=>true,
            'supports'           => array( 'excerpt','title','editor','thumbnail' ),
            'show_in_admin_bar' =>  true,
            'taxonomy' => array(
                'category'
            )


        );

        if ( ! in_array( $post_type, get_post_types() ) ) {
            register_post_type( $post_type, $args );

        }

    }
    /* function register_taxonomy() {
        //  register_taxonomy( 'post_tag', ['partner','post','page','revision','customize_changeset'] );
        // Add new taxonomy, NOT hierarchical (like tags)
        $labels = array(
            'name' => _x( 'Tags', 'taxonomy general name' ),
            'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Tags' ),
            'popular_items' => __( 'Popular Tags' ),
            'all_items' => __( 'All Tags' ),
            'parent_item' => null,
            'parent_item_colon' => null,
            'edit_item' => __( 'Edit Tag' ),
            'update_item' => __( 'Update Tag' ),
            'add_new_item' => __( 'Add New Tag' ),
            'new_item_name' => __( 'New Tag Name' ),
            'separate_items_with_commas' => __( 'Separate tags with commas' ),
            'add_or_remove_items' => __( 'Add or remove tags' ),
            'choose_from_most_used' => __( 'Choose from the most used tags' ),
            'menu_name' => __( 'Tags' ),
        );

        register_taxonomy('tag','partner',array(
            'hierarchical' => false,
            'labels' => $labels,
            'show_ui' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array( 'slug' => 'tag' ),
        ));

    } */

    function register_taxonomy () {
        register_taxonomy('category','documents',array(
            'hierarchical' => true,
            'show_in_rest'=>true,
        'labels'=>[
            'name'=>'category', 
            'singular_name'=>'Category',
            'menu_name'=>'Category'
        ],
            'show_ui' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array( 'slug' => 'category' ),
        ));
    }
    function my_title_place_holder( $title, $post ) {
        $post_type = 'documents';


        if ( $post->post_type == $post_type ) {
            $my_title = "Nom du Documents";

            return $my_title;
        }

        return $title;

    }
}

new Documents();