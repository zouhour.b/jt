<?php


class Session {
    public function __construct() {

        $this->register();
        add_filter( 'enter_title_here', array( $this, 'my_title_place_holder' ), 2, 10 );
       /* add_action( 'init', array( $this,'register_taxonomy' )); */

    }

    public function register() {
        $post_type = 'session';

        $labels = array(
            'name'           => $post_type,
            'singular_name'  => $post_type,
            'menu_name'      => 'Session',
            'name_admin_bar' => $post_type,
            'add_new_item'   => $post_type,
            'edit_item'      => $post_type,
        );

        $args = array(
            'labels'             => $labels,
            'show_in_rest'    =>true,
            'public'             => true,
            'supports'           => array( 'title','editor','thumbnail', 'excerpt' ),
            'show_in_admin_bar' =>  true,


        );

        if ( ! in_array( $post_type, get_post_types() ) ) {
            register_post_type( $post_type, $args );

        }

    }
    /*function register_taxonomy() {
       register_taxonomy( 'location', ['proces'], array(
           'hierarchical' => false,
           'labels'=>[
               'name'=>'location', 
               'singular_name'=>'Location',
               'menu_name'=>'Location'
               ]
           
       ) );
   } */
    function my_title_place_holder( $title, $post ) {
        $post_type = 'proces';


        if ( $post->post_type == $post_type ) {
            $my_title = "Nom du Session";

            return $my_title;
        }

        return $title;

    }
}

new Session();