<?php

function  intersect_posts ( $array1 , $array2 ) {

	$rslt_posts = [] ; 

	foreach ($array1 as $post ) {
		 foreach ($array2  as $post2) {
		 		if ( $post == $post2 ) {
		 			array_push($rslt_posts,$post) ; 
		 		} 
		 }
	} 
 
	return $rslt_posts ;
}


class Api {
	public function __construct() { 
	
        add_action('rest_api_init', [$this, 'register_hooks']);
    }


    public function register_hooks () {
    	register_rest_route (
    			'apirest' , '/getposts', 
    			['methods' => 'POST', 'callback' => [$this,'getposts']]
    	);
    	register_rest_route (
    			'apirest' , '/getproces', 
    			['methods' => 'POST', 'callback' => [$this,'getProces']]
    	);
    }

    public function getposts () {
    	$lang = $_POST['lang'];

    	$filter_relation = null;

    	if ( empty($_POST['format']) || empty($_POST['cat']) ) 
    	{
    		$relation = 'OR'; 
    	} else { 
    		$relation = 'AND';
    	}


	    $the_query = new WP_Query( 
	    	array( 
	    		'posts_per_page' => -1, 
	    		's' => esc_attr( $_POST['keyword'] ) , 
	    		'post_type' => 'documents', 
	    		'lang' => $lang,
	    		'tax_query' => array(
	    			 'relation' => $filter_relation,
			            array(
			               'taxonomy' => 'category',
			                'field'    => 'id',
			                'terms'    => array(esc_attr($_POST['cat'])),
			                ),
			            array(
			                'taxonomy' => 'category',
			                'field'    => 'id',
			                'terms'    => array(esc_attr($_POST['format'])),
			                )
				  
				    ),
					    		//'cat' => esc_attr($_POST['cat']),
	    		'date_query' => array(
	    			'year' =>  intval($_POST['year'])
	    			) 
	    	) 
	    );
	    if( $the_query->have_posts() ) { ?>
	                <thead>
	                    <tr>
	                        <th class="text-center"><?= __('العناوين','jt') ?></th>
	                        <th></th>
	                        <th class="text-center"><?= __('السنة','jt') ?></th>
	                    </tr>
	                </thead>
	        <?php
	        while( $the_query->have_posts() ): $the_query->the_post(); 
	        $category = get_the_category();
	        if ( $lang == "ar") {
	            $link = $category[0]->slug;
	        } else $link = substr($category[0]->slug, 0, -3) ;
	        ?>
	            <tr>
	                <td class="text-center i-title"><a href="<?= esc_url( post_permalink()) ?>"><img src=" <?= get_template_directory_uri() . '/assets/icons/'. $link ?>.svg"/></a></td>
	                <td class="right"><a href="<?= esc_url( post_permalink()) ?>"><?=the_title()?>   </a></td>
	                <td class="text-center"><a href="<?= esc_url( post_permalink()) ?>"><?= get_the_date( 'd.m.Y') ?></a></td>
	            </tr>

	        <?php endwhile;
	        wp_reset_postdata();  
	    } else {
	         ?>
	            <h3 class='my-4 text-center'> <?= __('ليس هناك وثائق','jt') ?> </h3> 
	        <?php 
	    }
	    die();
    }

    


    public function getProces () {
    	$lang = $_POST['lang'];
    	
    	$relation = null ;

    	if ( empty($_POST['tpi']) || empty($_POST['accusation']) ) 
    	{
    		$relation = 'OR'; 
    	} else { 
    		$relation = 'AND';
    	}

 
    	$the_metaQuery = new WP_Query ( array
	        ( 
	            'posts_per_page' => -1,
	            'post_type' => 'proces', 
	            'suppress_filters' => false,
	            'lang' => $lang,
	            'meta_query' => array(
	                'relation' => 'OR',
	                array(
	                    'key' => 'state',
	                    'value' => $_POST['court'],
	                    'compare' => '='
	                )
	            )
            )
        );



		$the_taxQuery = new WP_Query ( array
	        ( 
	            'posts_per_page' => -1,
	            'post_type' => 'proces', 
	            'suppress_filters' => false, 
	            'tax_query' => array(
	            	'relation' => $relation ,
	            	array(
	            		'taxonomy' => 'location',
	            		'field' => 'term_id',
	            		'terms' => $_POST['tpi']
	            	),
	            	array(
	            		'taxonomy' => 'accusation',
	            		'field' => 'term_id',
	            		'terms' => intval($_POST['accusation'])
	            	),
	            ) 
	        ) 
	    );
 
	

	    $the_query = new WP_Query();

 

	    if (    !empty($_POST['court']) && ( ! empty($_POST['accusation'] )   ||  ! empty($_POST['tpi']) ) ) 
	    { 
	  
	    	$the_query->posts = intersect_posts ($the_taxQuery->posts , $the_metaQuery->posts) ;

	    } elseif ( $the_metaQuery->have_posts() ) {
 
			$the_query->posts = $the_metaQuery->posts ;
	    } else {
 	 
	    	$the_query->posts = $the_taxQuery->posts ;
	    }
 				
	    $the_query->post_count =  count($the_query->posts);
	    

	    if( $the_query->have_posts() ) { ?>
	        	<?php
	        	include __DIR__ . '/../proces_archives_template.php'; 
	            ?> 
	        <?php 
	        wp_reset_postdata(); 
	    } else {
	         ?>
	            <h3 class='my-4 text-center'> <?= __('ليس هناك وثائق','jt') ?> </h3> 
	        <?php 
	    }
	    	die();
 	}


}



new Api();