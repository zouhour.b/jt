<?php

function victims($attrs, $content){
    $victims = get_field('hearing_victims');
    ob_start();
    if (($victims != '') and ($victims != '[]')) { 
      ?>
        <h2><?= __('سماع الضحايا','jt') ?> </h2>
        <?php
        }
       
    $victims = $victims?json_decode($victims, true):[];
    foreach( $victims as $data ): 
        if((!empty($data['person'])) and (!empty($data['charge']))) { ?>
        <div class="bg-gray p-5 mb-4" style="clear: both;">
            <h4 class="name-attr"><?= $data['person']; ?></h4>                
            <p><?= $data['charge'] ?></p>
        </div>
        <?php 
        } else ?>
        <div style="height: 2rem"> </div>
   <?php endforeach;
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_shortcode( 'victims', 'victims' );