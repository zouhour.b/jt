<?php 

/**
 * Plugin name: calendar by alkhatt
 * 
 */

add_action('wp_enqueue_scripts', function(){
 wp_enqueue_script('calendarjs', plugins_url('assets/calendar.js', __FILE__), ['jquery']);
 wp_enqueue_style('calendarcss', plugins_url('assets/calendar.css', __FILE__));
});

class PostTypeCalendar {
   
    public function __construct($args){
        $this->args = apply_filters('calendar_args', wp_parse_args($args, [
            'post_type'=>'post',
            'calendar_id'=>'calendar'.uniqid(),
            "dir"=>'ltr',
            "container_class"=>'calendar-container',
            'calendar_data_class'=>'calendar-data col-md-6 px-5',
            'calendar_class'=>'calendar col-md-6 align-self-start'
        ]));
    }

    public function getBaseUrl(){
        if (is_home()){
            return home_url()."/".pll_current_language();
        }
        return get_the_permalink(get_the_ID());
    }

    public function renderMonthFromRequest($var){
        if (isset($var['month'])){
            $month = new Datetime($var['month']);
        }
        else {
            $month = new Datetime();
            $month = $month->format('Y-m');
            $month = new Datetime($month);
        }
        return $this->render($month);
    }

    public function render($month){
        
        $postsByDate = [];
        $date1 = $month->format('Ymd');
        $date2 = new \Datetime($month->format('Ymd'));
        $date2 = $date2->modify('+1 month')->format('Ymd');
        $filterParams = [
            'post_type'=>$this->args['post_type'],
            'numberposts'=>-1,
            'meta_query'=>[
                [
                    'key'=>'date',
                    'value'=>[$date1, $date2],
                    'compare'=>'BETWEEN'
                ]
            ]
            

        ];
        $data = get_posts($filterParams);
        foreach ($data as $post){
            $val = get_post_meta($post->ID, 'date', true);
            if (!$val) continue;
            $date = $this->sanitizeTime($val);
            if (!isset($postsByDate[$date->format('Y-m-d')])){
                $postsByDate[$date->format('Y-m-d')] = [];
            }
            $postsByDate[$date->format('Y-m-d')][] = $post;
        }
        $args= $this->args;
        return sprintf('<div dir="%s" class="%s">
            <div class="row mt-2 calendar-root"  id="calendar">
            <div class="%s">%s%s</div>
            <div class="%s">%s</div>
            </div></div>', 
            $args['dir'],
            $args['container_class'],
            $args['calendar_class'], 
            $this->renderCalendarHead($month, $data), 
            $this->renderCalendarBody($month, $data),
            $args['calendar_data_class'],
            $this->renderCalendarData($month,$postsByDate)  

        );
    }

    public function renderCalendarHead($month){
        $prevM = new DateTimeImmutable($month->format('F Y'));
        $nextM = new DateTimeImmutable($month->format('F Y'));
        $cells = [];
        global $days;
        if (pll_current_language() == "ar") {
            $ar_months = ["جانفي", "فيفري", "مارس", "آفريل", "ماي", "جوان", "جويلية", "أوت", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
            $days = ['الاثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة', 'السبت', 'الأحد'];
        } else  {
            $ar_months = ["Janvier", "février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"];
            $days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
        } 
        foreach ( $days as $day){
            $cells[] = '<div class="calendar-cell calendar-head-cell"><div class="calendar-cell-inner"><span class="calendar-day">'.$day.'</span></div></div>';
            
        }
        return sprintf('<div class="d-flex justify-content-between mb-3">
            <a class="arrow-month left mt-2" data-month="%s"><img src="%s"/></a>
            <h1 style="font-size: 2rem;" class="text-center w-100">%s</h1>
            <a class="flip arrow-month right mt-2" data-month="%s"><img src="%s"/></a>
            </div>
            <div class="calendar-head">%s</div>',
            $this->getBaseUrl().'?month='.$prevM->modify('-1 month')->format('Y-m'),
            plugins_url() . '/calendar/assets/img/arrow.png',
            $ar_months[$month->format('n') -1 ] .' '. $month->format('Y') ,
            $this->getBaseUrl().'?month='.$nextM->modify('+1 month')->format('Y-m'),
            plugins_url() . '/calendar/assets/img/arrow.png',
            implode('', $cells));

    }

    public function sanitizeDate($date){
        return new Datetime($date);
    }

    public function renderEvent($event){
        $date = apply_filters('calendar_item_date', $event->post_date, $event);
        $val = get_post_meta( $event->ID, 'date', true);

        $proces_attached = get_field('proces', $event->ID);
        $session_title = get_post_meta( $event->ID, 'sub_title', true);
        
        $address = get_the_terms($proces_attached,'location'); //get_field('service_municipal', $proces_attached->ID);
        $t = empty(get_field('time', $event->ID))?" ":get_field('time', $event->ID);
        $class = empty(get_field('time', $event->ID))?"d-none":" ";
        
        $state = get_field('state', $proces_attached->ID);
        $select_state= empty( $state['value'])?null:$state['value'];
        if ( pll_current_language() == "ar") {
            $place = empty($address[0]->name)?"لم يحدد بعد":$address[0]->name.','.$state['label'];
        } else {
             $place = empty($address[0]->name)?"ce n'est pas encore déterminé":$address[0]->name.','.$state['label'];
        }
        global $days;
        return sprintf('        
            <div class="calendar-event mb-2"><h1 class="mt-0" style="font-size: 2rem;"><a href="%s" target="_blank">%s</a></h1>
            <h4 class="mb-3 d-inline-block" style="color: #be0a26">%s</h4>
            <div>
            <img src="%s" class="proces-icon mx-1" style="vertical-align: middle;"/>
            <h5 class="card-text mb-0 d-inline-block" id="location">%s</h5>
            </div>
            <div>
            <img src="%s" class="proces-icon mx-1 %s" style="vertical-align: middle;"/>
            <h5 class="card-text mb-0 d-inline-block" id="time">%s</h5>
            </div>
            </div>'
            , get_post_permalink($event->ID)
            , $proces_attached->post_title
            , $session_title
            , get_template_directory_uri() . '/assets/icons/place.svg'
            , $place

            , get_template_directory_uri() . '/assets/icons/clock.svg'
            , $class
            , $t
        );
        
    }

    public function renderCalendarData($month,$data){
        $tabs = $eventsMarkup = [];
        global $days;
        $i = 0;
        $date = new Datetime($month->format('Y-m-d'));
        $datelimit = new Datetime($date->format('Y-m-d'));
        $datelimit->modify('+1 month');
        while ($date < $datelimit && $i < 32){
            $d = $date->format('Y-m-d');
            if (!array_key_exists($d, $data)){
                $data[$d] = [];
            }
            $i++;
            $date->modify('+1 day');
        }
        foreach ($data as $date=>$events){
            $eventsMarkup = [];
            if ($events){

                foreach ($events as $event){
                    $val = get_post_meta( $event->ID, 'date', true);
                    if (!$val) continue;
                    $eventDate = $this->sanitizeTime($val);
                    $eventsMarkup[] = $this->renderEvent($event);
                }
            }
            else{
                $eventDate =new Datetime($date);
                if (pll_current_language() == "ar"){        
                $eventsMarkup = ['<h4 class="mt-3">لا يوجد أحداث</h4>'];
                } else $eventsMarkup = ["<h4 class='mt-3'>Il n'y a aucun événement</h4>"];
            }
            $active = count($events)>0?'active':'';
            $tabs[] = sprintf('<div class="calendar-tab '.$active.'" id="%s-tab-%s">
                <svg width="100" height="110">
                <rect width="100" height="110" style="fill:#be0c27;" />
                <text  text-anchor="middle" font-family="Cairo-Regular" font-size="25" x="50" y="40" fill="white">%s
                </text>
                <text text-anchor="middle" font-family="Cairo-Regular" font-size="50" x="50" y="95" fill="white">%s
                </text>
                </svg>
                %s</div>
                '
                , $this->args['calendar_id']
                , $date
                , $days[ $eventDate->modify('-1 day')->format('w') ]     
                , $eventDate->modify('+1 day')->format('d')
                , implode('',$eventsMarkup));
            
        }

        return implode('', $tabs);
    }

    public function sanitizeTime($string){
        if (!$string) return null;
        if (strpos($string, '-')){
            return new \Datetime($string);
        }
        $y = substr($string, 0,4);
        $m = substr($string, 4,2);
        $d = substr($string, 6,2);
        //\Datetime($y.'-'.$m.'-'.$d)
        return new \Datetime($y.'-'.$m.'-'.$d);
    }


    public function renderCalendarBody($month, $data){
        $i = 1;
        $date = new Datetime($month->format('Y-m-d'));
        $cells = [];
        $byDate = [];
        foreach ($data as $e){
            $val = get_field_object('date', $e->ID);
            if (!$val) continue;
            $eventDate = $this->sanitizeTime($val['value']);
            $eventDateFormatted = $eventDate->format('Y-m-d');
            if (!isset($byDate[$eventDateFormatted])){
                $byDate[$eventDateFormatted] = [];

            }
            $byDate[$eventDateFormatted][] = $e;
        }

        $datelimit = new Datetime($date->format('Y-m-d'));
        $datelimit->modify('+1 month');
        while ($date < $datelimit && $i < 32){
            $dayOfWeek = $date->format('w');
            $hasDate = isset($byDate[$date->format('Y-m-d')]);
            if ($hasDate) {
                $hasDateClass = 'hasDate';
            } else $hasDateClass = '';
            $cells[] =  sprintf('<a href="#%s-tab-%s"  class="calendar-cell" data-day="%s"><div class="calendar-cell-inner"><div class="calendar-day %s">%s</div></div></a>',
               $this->args['calendar_id'], 
               $date->format('Y-m-d'),
               $dayOfWeek,
               $hasDateClass ,
               $i
           );
            $date->modify('+1 day');  
            $i++;
        }
        return sprintf('<div class="%s">%s</div>', 'calendar-body d-flex flex-wrap', implode('', $cells));
    }

}