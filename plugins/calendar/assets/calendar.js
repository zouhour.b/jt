(function($){



$(document).on('click', '.calendar-cell[href]', function(e){
    e.preventDefault()
    let target = $(this).attr('href');
    $(this).parent().children().removeClass('active');
    $(this).addClass('active');

    $('.calendar-tab').removeClass('active');
    $(target).addClass('active');
    if ($( window ).width()< 500) {
        $.each($('.calendar-tab').find('svg') , function(){
            $(this).attr('width', '50').attr('height', '55');
            $(this).find('rect').attr('width', '50').attr('height', '55');
            $(this).find('text').attr('font-size', '12').attr('x', '25');
            $(this).find('text:first').attr('y', '20');
            $(this).find('text:last').attr('y', '45');
    
})
}
    
})
$('.calendar-cell:first-child').click();
$(document).on('click', 'a[data-month]', function(e){
    e.preventDefault();
    let $calendar = $(this).closest('.calendar-root');
    let wrapper = $('<div>');
    $.get($(this).data('month'), function(response){
        $calendar.html(wrapper.html(response).find('#calendar').html());
    })
   
})
$(function() {
    var date = new Date();
    
    $('.calendar-cell[href]').each(function() {
        if ($(this).text() == date.getDate()){
            //$(this).click();
            $(this).addClass('active');
        }
    })

});
$( document ).ready(function(){
    if ($( window ).width()< 500) {
        $.each($('.calendar-tab').find('svg') , function(){
            $(this).attr('width', '50').attr('height', '55');
            $(this).find('rect').attr('width', '50').attr('height', '55');
            $(this).find('text').attr('font-size', '12').attr('x', '25');
            $(this).find('text:first').attr('y', '20');
            $(this).find('text:last').attr('y', '45');
    
})
}
})
})(jQuery)